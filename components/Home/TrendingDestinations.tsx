import { Carousel, Icon } from 'antd';
import React, { useContext } from 'react'
import { getContent } from 'web-component-antd';
import { CMSContext } from '../../pages';
import TourCard from '../Card/TourCard';
import { CarouselArrow } from '../Utilities/carousel-arrow';

const TrendingDestinations = () => {
  const content = useContext(CMSContext);
  const trendingDestinations = getContent(content).tag("trending-destinations").getValue();
  console.log({trendingDestinations});

  const japanTours = [
    {
      photo_url: "/img/japan1.jpg",
      title: "7D5N Honshu",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
    {
      photo_url: "/img/japan2.jpg",
      title: "4D3N Hokkaido",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
    {
      photo_url: "/img/japan3.jpg",
      title: "4D3N Hokkaido",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
    {
      photo_url: "/img/japan1.jpg",
      title: "7D5N Honshu",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
  ];

  const koreaTours = [
    {
      photo_url: "/img/korea1.jpg",
      title: "7D5N Honshu",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
    {
      photo_url: "/img/korea2.jpg",
      title: "7D5N Honshu",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
    {
      photo_url: "/img/korea3.jpg",
      title: "7D5N Honshu",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
    {
      photo_url: "/img/korea1.jpg",
      title: "7D5N Honshu",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
  ];

  const busanTours = [
    {
      photo_url: "/img/busan1.jpg",
      title: "7D5N Honshu",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
    {
      photo_url: "/img/busan2.jpg",
      title: "7D5N Honshu",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
    {
      photo_url: "/img/busan3.jpg",
      title: "7D5N Honshu",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
    {
      photo_url: "/img/busan1.jpg",
      title: "7D5N Honshu",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      price: "S$1,688.00",
    },
  ];
  
  trendingDestinations.photos[0].tours = japanTours;
  trendingDestinations.photos[1].tours = koreaTours;
  trendingDestinations.photos[2].tours = busanTours;

  const LeftArrowIcon = () => <Icon style={{ fontSize: "60px" }} type="left" />;
  const RightArrowIcon = () => <Icon style={{ fontSize: "60px" }} type="right" />;

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: (
      <CarouselArrow
        iconElement={
          <span className="carousel-arrow">
            <LeftArrowIcon />
          </span>
        }
      />
    ),
    nextArrow: (
      <CarouselArrow
        iconElement={
          <span className="carousel-arrow">
            <RightArrowIcon />
          </span>
        }
      />
    ),
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 1210,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 838,
        settings: {
          arrows: false,
          slidesToShow: 1,
        },
      },
    ],
  };

  return (
    <div id="trending-destinations">
      <div className="wrap">
        <div className="title">{trendingDestinations.title}</div>
        {
          trendingDestinations.photos.map((destination, index) => {
            return (
              <div className="tours" key={`Tours-${index}`}>
                <div className="title">{destination.title}</div>
                <Carousel {...settings}>
                  {destination.tours.map((tour, index) => {
                    return (
                      <TourCard tour={tour} key={`TourCard-${index}`} />
                    )
                  })}
                </Carousel>
            </div>
            )
        })}
      </div>
    </div>
  )
}

export default TrendingDestinations
