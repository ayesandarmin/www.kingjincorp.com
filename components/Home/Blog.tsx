import { Col, Row } from "antd";
import { useContext } from "react";
import { getContent } from "web-component-antd";
import { splitNameAndUrl } from "../../helpers/common-function";
import { CMSContext } from "../../pages";

const Blog = () => {
  const content = useContext(CMSContext);
  const blogs = getContent(content).tag("news-and-happenings").getValue();
  return (
    <div className="wrap news-and-happenings" id="blog">
      <div className="center">
        <h3>{blogs.title}</h3>
        <h5>{blogs.description}</h5>
      </div>
      <div className="center">
        <div>
          <Row>
            {blogs.photos.map((blog, index) => {
              const [date, title] = splitNameAndUrl(blog.title);
              return (
                <Col md={12} lg={6} key={`blog-${index}`}>
                  <div className="blog-card">
                    <a href={blog.desc} target="_blank">
                      <div className="border">
                        <div
                          className="blog-img"
                          style={{
                            backgroundImage: `url(${blog.photo.regular})`,
                            backgroundPosition: "center",
                            backgroundSize: "cover",
                            backgroundRepeat: "no-repeat",
                            height: 285,
                          }}
                        />
                      </div>

                      <div className="center blog-title">
                        <h6>{date}</h6>
                        <h4>{title}</h4>
                      </div>
                    </a>
                  </div>
                </Col>
              );
            })}
          </Row>
        </div>
        <div className="view-all-articles">
          <a
            href="https://local.google.com/place?id=15750477537378982974&amp;use=posts&amp;lpsid=3114256293406097984"
            target="_blank"
          >
            View All Articles
          </a>
        </div>
      </div>
    </div>
  );
};
export default Blog;
