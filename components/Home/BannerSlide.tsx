import { Button, Col, Icon, Row } from "antd";
import Router from "next/router";
import qs from "qs";
import React, { useEffect, useState } from "react";
import { Element } from "react-scroll";
import { isEmptyObject, TravelCloudClient, validateHotelbedsParams } from "travelcloud-antd";
import { HotelbedsFormValue } from "travelcloud-antd/order-product-compute";
import config from "../../customize/config";
import { HotelBedsCheckInDatePicker, HotelBedsCheckOutDatePicker, HotelbedsSearchForm, HotelbedsSourceMarket, HotelsAndDestinationsName } from "../hotelbeds-search-form";

const BannerSlide = ({ banner }) => {
  const client: TravelCloudClient = new TravelCloudClient(config);
  const [formValue, setFormValue]: [HotelbedsFormValue, any] = useState<any>({});

  useEffect(() => {
    if (!isEmptyObject(formValue) && validateHotelbedsParams(formValue)) {
      Router.push("/hotels?" + qs.stringify(formValue));
    }
  }, [formValue]);

  return (
    <div
      className="banner-slider"
      key={`home-banner-photo-${banner.id}`}
    >
      <div
        className="banner-img"
        style={{
          backgroundImage: `url(${banner.photo.regular})`,
          backgroundPosition: "center",
          backgroundSize: "cover",
        }}
      >
        <div className="banner-text">
          <div className="banner-desc" dangerouslySetInnerHTML={{ __html: banner.desc }} />
          <div className="banner-title">{banner.title}</div>
        </div>

        <section className="hotels-search wrap">
          <Element name="search">
            <HotelbedsSearchForm formValue={formValue} client={client}>
              {({ params, controller }) => (
                <React.Fragment>
                  <div className="upper-hotel-search">
                    <Row type="flex" align="middle">
                      <Col>
                        <span className="label">
                          {/* Nationality */}
                          <Icon type="flag" />
                        </span>
                      </Col>
                      <Col>
                        <HotelbedsSourceMarket
                          style={{ minWidth: 110, marginRight: 10 }}
                          params={params}
                          onSourceMarketChange={controller.onSourceMarketChange}
                        />
                      </Col>
                    </Row>
                  </div>
                  <div className="lower-hotel-search">
                    <Row type="flex" justify="center" align="middle" gutter={20}>
                      <Col span={7}>
                        <div className="custom-auto-complete">
                          <span className="label">Where you go?</span>
                          <HotelsAndDestinationsName
                            placeholder="Select Destination"
                            className="destination-autocomplete"
                            client={client}
                            params={params}
                            onHotelsOrDestinationsChange={
                              controller.onHotelsOrDestinationsChange
                            }
                          />
                        </div>
                      </Col>
                      <Col span={6}>
                        <div className="custom-date-picker custom-divider">
                          <span className="label">Check-In</span>
                          <HotelBedsCheckInDatePicker
                            placeholder="Add a Date"
                            params={params}
                            onStayChange={controller.onStayChange}
                          />
                        </div>
                      </Col>
                      <Col span={6}>
                        <div className="custom-date-picker">
                          <span className="label">Check-Out</span>
                          <HotelBedsCheckOutDatePicker
                            placeholder="Add a Date"
                            params={params}
                            onStayChange={controller.onStayChange}
                          />
                        </div>
                      </Col>
                      {/* <Col>
                        <HotelBedsOccupancyPopover
                          className="bg-transparent"
                          params={params}
                          onOccupanciesChange={controller.onOccupanciesChange}
                        />
                      </Col> */}
                      <Col span={5}>
                        <Button
                          size="default"
                          type="primary"
                          disabled={!validateHotelbedsParams(params)}
                          onClick={(_) => {
                            setFormValue(Object.assign({}, params));
                            if (!isEmptyObject(params)) {
                              // search(params);
                              Router.push("/hotels?" + qs.stringify(formValue));
                            }
                          }}
                        >
                          Search
                        </Button>
                      </Col>
                    </Row>
                  </div>
                </React.Fragment>
              )}
            </HotelbedsSearchForm>
          </Element>
        </section>

      </div>
    </div>
  )
}

export default BannerSlide
