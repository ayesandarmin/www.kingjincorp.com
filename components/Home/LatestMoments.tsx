import { Carousel, Icon } from "antd";
import { useContext } from "react";
import { getContent } from "web-component-antd";
import { splitNameAndUrl } from "../../helpers/common-function";
import { CMSContext } from "../../pages";

const LatestMoments = () => {
  const content = useContext(CMSContext);
  const latest = getContent(content).tag("latest-moments").getValue();

  function SampleNextArrow(props) {
    const { className, onClick } = props;
    return (
      <div className={className} onClick={onClick}>
        <span className="carousel-arrow">
          <Icon
            style={{ fontSize: "60px" }}
            type="right-circle"
            theme="filled"
          />
        </span>
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { className, onClick } = props;
    return (
      <div className={className} onClick={onClick}>
        <span className="carousel-arrow">
          <Icon
            style={{ fontSize: "60px" }}
            type="left-circle"
            theme="filled"
          />
        </span>
      </div>
    );
  }

  const settings = {
    autoplay: true,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: <SamplePrevArrow/>,
    nextArrow: <SampleNextArrow />,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          dots: false,
          infinite: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          infinite: true,
        },
      },
    ],
  };

  return (
    <div className="wrap latest-moments">
      <div className="center">
        <h3>{latest.title}</h3>
        <h5>{latest.description}</h5>
      </div>
      <Carousel {...settings}>
        {latest.photos.map((slide, index) => {
          const [desc, link] = splitNameAndUrl(slide.desc);
          return (
            <a href={link} target="_blank">
              <div key={`latest-slide-${index}`} className="latest-slide">
                <div
                  className="latest-slide-img"
                  style={{
                    backgroundImage: `url(${slide.photo.regular})`,
                    backgroundPosition: "center",
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                    height: 265,
                  }}
                />
                <div className="center">
                  <h4>{slide.title}</h4>
                  {desc.length <= 80 ? (
                    <h6>{desc}</h6>
                  ) : (
                    <h6>{desc.substring(0, 80).concat(" ...")}</h6>
                  )}
                </div>
              </div>
            </a>
          );
        })}
      </Carousel>
    </div>
  );
};
export default LatestMoments;
