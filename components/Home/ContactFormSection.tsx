import { Col, Row } from "antd";
import React, { useContext } from "react";
import { getContent } from "web-component-antd";
import { CMSContext } from "../../pages";
import { HomeContactForm } from "../TravelCloud/HomeContactForm";
import TitleContent from "../Utilities/TitleContent";

const ContactFormSection = () => {
  const content = useContext(CMSContext);
  const contact = getContent(content).tag("contact").getValue();

  return (
    <div id="home-contact">
      <Row>
        <div
          style={{
            backgroundImage: `url(${contact.photo.regular})`,
            backgroundPosition: "center",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            height: 615,
          }}
        >
          <div>
            <Row type="flex" justify="start">
              <Col>
                <div className="contact-form m-y-55">
                  <TitleContent
                    title={contact.title}
                    position="start"
                    description={contact.description}
                    className="m-b-15"
                  />
                  <HomeContactForm />
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </Row>
    </div>
  );
};

export default ContactFormSection;
