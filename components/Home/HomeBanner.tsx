import { Carousel } from "antd";
import { useContext } from "react";
import { getContent } from "web-component-antd";
import { CMSContext } from "../../pages";

const HomeBanner = () => {
  const content = useContext(CMSContext);
  const banner = getContent(content).tag("banner-section").getValue();

  const settings = {
    autoplay: true,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      }
    ]
  };

  return (
    <div className="home-banner">
      <Carousel {...settings}>
        {banner.photos.map((banner, index) => {
          return (
            <div key={`banner-${index}`}>
              <div
                style={{
                  backgroundImage: `url(${banner.photo.regular})`,
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                  height: 640,
                }}
              ></div>
            </div>
          );
        })}
      </Carousel>
      <div>
        <div className="banner-title-section">
          <div style={{ width: 401 }}>
            <div style={{ padding: "31px 54px" }}>
              <h2>Korea & Japan Tour TOP Specialist</h2>
              <h6>韩国和日本旅游专家</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default HomeBanner;
