import { Row, Col, Carousel, Icon } from "antd";
import moment from "moment";
import { CarouselArrow } from "../Utilities/carousel-arrow";

const GoogleReviews = ({ reviewTitle }) => {

  const reviews = [
    {
      flagUrl:
        "https://www.google.com/local/review/rap/report?postId=ChZDSUhNMG9nS0VJQ0FnSUNPenJ6SElREAE&entityid=ChZDSUhNMG9nS0VJQ0FnSUNPenJ6SElRGi4KF0NJSE0wb2dLRUlDQWdJQ096cnpIb1FFEhNDZ3dJNzYtTGxRWVE4SkMya2dNIhIJcXYTZcEZ2jERPlw-4t7vlNoqE0Nnd0k3Ni1MbFFZUThKQzJrZ00&wv=1&d=286732320",
      plusId: "111587606444616022074",
      reviewAuthor: "LING LING GOH",
      reviewAuthorImage: "/img/reviews/review-img1.png",
      reviewDate: "3 weeks ago",
      reviewRating: 1,
      reviewText: "",
      reviewUrl:
        "https://www.google.com/maps/reviews/data=!4m8!14m7!1m6!2m5!1sChZDSUhNMG9nS0VJQ0FnSUNPenJ6SElREAE!2m1!1s0x0:0xda94efdee23e5c3e!3m1!1s2@1:CIHM0ogKEICAgICOzrzHIQ%7CCgwI76-LlQYQ8JC2kgM%7C?hl=en-US",
      rawDate: "06-10-2022",
    },
    {
      flagUrl:
        "https://www.google.com/local/review/rap/report?postId=ChdDSUhNMG9nS0VJQ0FnSUQyeThtWjJnRRAB&entityid=ChdDSUhNMG9nS0VJQ0FnSUQyeThtWjJnRRItChZDSUhNMG9nS0VJQ0FnSUQyeThtWk9nEhNDZ3dJZ29lM2xBWVE4SmIxckFFGi4KF0NJSE0wb2dLRUlDQWdJRDJ5OG1adWdFEhNDZ3dJZ29lM2xBWVE4SmIxckFFIhIJcXYTZcEZ2jERPlw-4t7vlNoqE0Nnd0lnb2UzbEFZUThKYjFyQUU&wv=1&d=286732320",
      plusId: "107227920409719371001",
      reviewAuthor: "hanyu li",
      reviewAuthorImage: "/img/reviews/review-img2.png",
      reviewDate: "a month ago",
      reviewRating: 5,
      reviewText:
        "\u611f\u89c9\u975e\u5e38\u4e13\u4e1a \u670d\u52a1\u5f88\u597d king\u54e5\u8fd8\u8bf7\u559d\u4e86\u5976\u8336\ud83e\udd2b",
      reviewUrl:
        "https://www.google.com/maps/reviews/data=!4m8!14m7!1m6!2m5!1sChdDSUhNMG9nS0VJQ0FnSUQyeThtWjJnRRAB!2m1!1s0x0:0xda94efdee23e5c3e!3m1!1s2@1:CIHM0ogKEICAgID2y8mZ2gE%7CCgwIgoe3lAYQ8Jb1rAE%7C?hl=en-US",
      rawDate: "05-25-2022",
    },
    {
      flagUrl:
        "https://www.google.com/local/review/rap/report?postId=ChdDSUhNMG9nS0VJQ0FnSUNxc2FHNndBRRAB&entityid=ChdDSUhNMG9nS0VJQ0FnSUNxc2FHNndBRRItChZDSUhNMG9nS0VJQ0FnSUNxc2FHNklBEhNDZ3dJcWJ1SGhnWVEyTzNkeEFJGi4KF0NJSE0wb2dLRUlDQWdJQ3FzYUc2b0FFEhNDZ3dJXzdxSGhnWVF3UHZJOXdFIhIJcXYTZcEZ2jERPlw-4t7vlNoqE0Nnd0lxYnVIaGdZUTJPM2R4QUk&wv=1&d=286732320",
      plusId: "100350054347966278599",
      reviewAuthor: "elvis tan",
      reviewAuthorImage: "/img/reviews/review-img3.png",
      reviewDate: "a year ago",
      reviewRating: 5,
      reviewText:
        "\n\u8d85\u68d2\u7684\u670d\u52a1\u6001\u5ea6\u548c\u901f\u5ea6 \u8ba9\u6211\u5b8c\u5168\u4e0d\u62c5\u5fc3\u75ab\u60c5\u671f\u95f4\u590d\u6742\u7684\u5b89\u6392\uff0c\u4ee5\u540e\u65c5\u884c\u80af\u5b9a\u662f\u6211\u9996\u9009\u7684\u65c5\u884c\u793e\uff01",
      reviewUrl:
        "https://www.google.com/maps/reviews/data=!4m8!14m7!1m6!2m5!1sChdDSUhNMG9nS0VJQ0FnSUNxc2FHNndBRRAB!2m1!1s0x0:0xda94efdee23e5c3e!3m1!1s2@1:CIHM0ogKEICAgICqsaG6wAE%7CCgwIqbuHhgYQ2O3dxAI%7C?hl=en-US",
      rawDate: "06-10-2021",
    },
    {
      flagUrl:
        "https://www.google.com/local/review/rap/report?postId=ChZDSUhNMG9nS0VJQ0FnSUNDai1hS0hBEAE&entityid=ChZDSUhNMG9nS0VJQ0FnSUNDai1hS0hBGi4KF0NJSE0wb2dLRUlDQWdJQ0NqLWFLbkFFEhNDZ3dJNXNMYy1nVVFnTVh4blFNIhIJcXYTZcEZ2jERPlw-4t7vlNoqE0Nnd0k1c0xjLWdVUWdNWHhuUU0&wv=1&d=286732320",
      plusId: "113675748259321371134",
      reviewAuthor: "Jesse Efron",
      reviewAuthorImage: "/img/reviews/review-img4.png",
      reviewDate: "a year ago",
      reviewRating: 5,
      reviewText: "",
      reviewUrl:
        "https://www.google.com/maps/reviews/data=!4m8!14m7!1m6!2m5!1sChZDSUhNMG9nS0VJQ0FnSUNDai1hS0hBEAE!2m1!1s0x0:0xda94efdee23e5c3e!3m1!1s2@1:CIHM0ogKEICAgICCj-aKHA%7CCgwI5sLc-gUQgMXxnQM%7C?hl=en-US",
      rawDate: "09-08-2020",
    },
    {
      flagUrl:
        "https://www.google.com/local/review/rap/report?postId=ChdDSUhNMG9nS0VJQ0FnSUNzei03MDB3RRAB&entityid=ChdDSUhNMG9nS0VJQ0FnSUNzei03MDB3RRIsChZDSUhNMG9nS0VJQ0FnSUNzei03ME13EhJDZ3NJcE1hdThnVVF1UEsyVXcaLQoXQ0lITTBvZ0tFSUNBZ0lDc3otN2NvZ0USEkNnc0lwTWF1OGdVUXVQSzJVdyISCXF2E2XBGdoxET5cPuLe75TaKhJDZ3NJcE1hdThnVVF1UEsyVXc&wv=1&d=286732320",
      plusId: "112745911739479680608",
      reviewAuthor: "Grace Lum",
      reviewAuthorImage: "/img/reviews/review-img5.png",
      reviewDate: "2 years ago",
      reviewRating: 5,
      reviewText:
        "Our 10D9N trip in Hokkaido and Tokyo has been truly amazing and memorable! From the enquiry stage until the week prior the trip King Jin has been really helpful and informative! He is extremely knowledgeable about Japan! There is nothing that we had to be worried about, we were in good hands.\n\nSpecial thanks to our tour guide Xiao Feng (Hokkaido) and Xiao Cui (Tokyo) as well who\u2019s been very accommodative and helped us with our heavy luggage each time (12-14 of them for a family of 6)! And especially when we had 2 elderly parents travelling together.\n\nHighly recommended!! Will definitely recommend them to our friends and families and we look forward to our next trip!",
      reviewUrl:
        "https://www.google.com/maps/reviews/data=!4m8!14m7!1m6!2m5!1sChdDSUhNMG9nS0VJQ0FnSUNzei03MDB3RRAB!2m1!1s0x0:0xda94efdee23e5c3e!3m1!1s2@1:CIHM0ogKEICAgICsz-700wE%7CCgsIq8au8gUQwO2VVQ%7C?hl=en-US",
      rawDate: "02-18-2020",
    },
    {
      flagUrl:
        "https://www.google.com/local/review/rap/report?postId=ChdDSUhNMG9nS0VJQ0FnSUNzM3V5ZG93RRAB&entityid=ChdDSUhNMG9nS0VJQ0FnSUNzM3V5ZG93RRItChZDSUhNMG9nS0VJQ0FnSUNzM3V5ZFl3EhNDZ3dJbU1qZDhRVVFzT1doM3dJGi4KF0NJSE0wb2dLRUlDQWdJQ3MzdXlkNHdFEhNDZ3dJbU1qZDhRVVFzT1doM3dJIhIJcXYTZcEZ2jERPlw-4t7vlNoqE0Nnd0ltTWpkOFFVUXNPV2gzd0k&wv=1&d=286732320",
      plusId: "102870355084635841925",
      reviewAuthor: "QiaoJun Lin",
      reviewAuthorImage: "/img/reviews/review-img6.png",
      reviewDate: "2 years ago",
      reviewRating: 5,
      reviewText:
        "Staff are friendly and very helpful in providing recommendations! It was a good experience. Will definitely come back for future travels.",
      reviewUrl:
        "https://www.google.com/maps/reviews/data=!4m8!14m7!1m6!2m5!1sChdDSUhNMG9nS0VJQ0FnSUNzM3V5ZG93RRAB!2m1!1s0x0:0xda94efdee23e5c3e!3m1!1s2@1:CIHM0ogKEICAgICs3uydowE%7CCgwImMjd8QUQsOWh3wI%7C?hl=en-US",
      rawDate: "02-03-2020",
    },
    {
      flagUrl:
        "https://www.google.com/local/review/rap/report?postId=ChZDSUhNMG9nS0VJQ0FnSUNzcnR5eUJREAE&entityid=ChZDSUhNMG9nS0VJQ0FnSUNzcnR5eUJRGi4KF0NJSE0wb2dLRUlDQWdJQ3NydHl5aFFFEhNDZ3dJeF9UWjhRVVEtTEdScFFFIhIJcXYTZcEZ2jERPlw-4t7vlNoqE0Nnd0l4X1RaOFFVUS1MR1JwUUU&wv=1&d=286732320",
      plusId: "110794210828342249203",
      reviewAuthor: "\u9673\u70b3\u548c",
      reviewAuthorImage: "/img/reviews/review-img7.png",
      reviewDate: "2 years ago",
      reviewRating: 5,
      reviewText: "",
      reviewUrl:
        "https://www.google.com/maps/reviews/data=!4m8!14m7!1m6!2m5!1sChZDSUhNMG9nS0VJQ0FnSUNzcnR5eUJREAE!2m1!1s0x0:0xda94efdee23e5c3e!3m1!1s2@1:CIHM0ogKEICAgICsrtyyBQ%7CCgwIx_TZ8QUQ-LGRpQE%7C?hl=en-US",
      rawDate: "02-02-2020",
    },
    {
      flagUrl:
        "https://www.google.com/local/review/rap/report?postId=ChdDSUhNMG9nS0VJQ0FnSUNzcnRTSnJ3RRAB&entityid=ChdDSUhNMG9nS0VJQ0FnSUNzcnRTSnJ3RRIsChZDSUhNMG9nS0VJQ0FnSUNzcnRTSmJ3EhJDZ3NJNGVqWjhRVVFvUFhIUUEaLQoXQ0lITTBvZ0tFSUNBZ0lDc3J0U0o3d0USEkNnc0k0ZWpaOFFVUW9QWEhRQSISCXF2E2XBGdoxET5cPuLe75TaKhJDZ3NJNGVqWjhRVVFvUFhIUUE&wv=1&d=286732320",
      plusId: "118170858591977367547",
      reviewAuthor: "Liangqun Zhou",
      reviewAuthorImage: "/img/reviews/review-img8.png",
      reviewDate: "2 years ago",
      reviewRating: 5,
      reviewText:
        "\n\u8001\u677f\u5f88\u8010\u5fc3\uff0c\u884c\u7a0b\u5b89\u6392\u5f88\u6ee1\u610f\uff0c\u660e\u5e74\u8fd8\u4f1a\u518d\u53bb\u4e00\u6b21",
      reviewUrl:
        "https://www.google.com/maps/reviews/data=!4m8!14m7!1m6!2m5!1sChdDSUhNMG9nS0VJQ0FnSUNzcnRTSnJ3RRAB!2m1!1s0x0:0xda94efdee23e5c3e!3m1!1s2@1:CIHM0ogKEICAgICsrtSJrwE%7CCgsI4ejZ8QUQoPXHQA%7C?hl=en-US",
      rawDate: "02-02-2020",
    },
    {
      flagUrl:
        "https://www.google.com/local/review/rap/report?postId=ChdDSUhNMG9nS0VJQ0FnSUNzcnVUcGpnRRAB&entityid=ChdDSUhNMG9nS0VJQ0FnSUNzcnVUcGpnRRItChZDSUhNMG9nS0VJQ0FnSUNzcnVUcFRnEhNDZ3dJME9YWjhRVVFxS2JHX3dJGi4KF0NJSE0wb2dLRUlDQWdJQ3NydVRwemdFEhNDZ3dJME9YWjhRVVFxS2JHX3dJIhIJcXYTZcEZ2jERPlw-4t7vlNoqE0Nnd0kwT1haOFFVUXFLYkdfd0k&wv=1&d=286732320",
      plusId: "104843147782678130082",
      reviewAuthor: "Christopher Zheng",
      reviewAuthorImage: "/img/reviews/review-img9.png",
      reviewDate: "2 years ago",
      reviewRating: 5,
      reviewText: "Informative staff and friendly service.",
      reviewUrl:
        "https://www.google.com/maps/reviews/data=!4m8!14m7!1m6!2m5!1sChdDSUhNMG9nS0VJQ0FnSUNzcnVUcGpnRRAB!2m1!1s0x0:0xda94efdee23e5c3e!3m1!1s2@1:CIHM0ogKEICAgICsruTpjgE%7CCgwI0OXZ8QUQqKbG_wI%7C?hl=en-US",
      rawDate: "02-02-2020",
    },
    {
      flagUrl:
        "https://www.google.com/local/review/rap/report?postId=ChdDSUhNMG9nS0VJQ0FnSUNzem8tQm93RRAB&entityid=ChdDSUhNMG9nS0VJQ0FnSUNzem8tQm93RRItChZDSUhNMG9nS0VJQ0FnSUNzem8tQll3EhNDZ3dJX0xuWjhRVVEySy1HM1FFGi4KF0NJSE0wb2dLRUlDQWdJQ3N6by1CNHdFEhNDZ3dJX0xuWjhRVVEySy1HM1FFIhIJcXYTZcEZ2jERPlw-4t7vlNoqE0Nnd0lfTG5aOFFVUTJLLUczUUU&wv=1&d=286732320",
      plusId: "105486874407656893396",
      reviewAuthor: "Teh Emily",
      reviewAuthorImage: "/img/reviews/review-img10.png",
      reviewDate: "2 years ago",
      reviewRating: 5,
      reviewText: "\n\u670d\u52a1\u6001\u5ea6\u5f88\u597d",
      reviewUrl:
        "https://www.google.com/maps/reviews/data=!4m8!14m7!1m6!2m5!1sChdDSUhNMG9nS0VJQ0FnSUNzem8tQm93RRAB!2m1!1s0x0:0xda94efdee23e5c3e!3m1!1s2@1:CIHM0ogKEICAgICszo-BowE%7CCgwI_LnZ8QUQ2K-G3QE%7C?hl=en-US",
      rawDate: "02-02-2020",
    },
  ];

  const settings = {
    autoplay: true,
    dots: false,
    arrows: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: (
      <CarouselArrow
        iconElement={
          <span className="carousel-arrow">
            <Icon
              style={{ fontSize: "60px" }}
              type="left-circle"
              theme="filled"
            />
          </span>
        }
      />
    ),
    nextArrow: (
      <CarouselArrow
        iconElement={
          <span className="carousel-arrow">
            <Icon
              style={{ fontSize: "60px" }}
              type="right-circle"
              theme="filled"
            />
          </span>
        }
      />
    ),
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <div id="reviews" className="wrap">
      <div className="wrap center">
        <h3>{reviewTitle.title}</h3>
        <h5>{reviewTitle.description}</h5>
      </div>
      <div>
        <Row>
          <Carousel {...settings}>
            {reviews.map((review, index) => {
              return (
                <Col xs={24} md={12} lg={8} key={`review-${index}`}>
                  <a href={review.reviewUrl} target="_blank">
                    <div className="review-card">
                      <Row type="flex" justify="start" align="middle">
                        <div className="review-author-img">
                          <img src={review.reviewAuthorImage} />
                        </div>
                        <div>
                          <h3 className="review-author-name">
                            {review.reviewAuthor}
                          </h3>
                          <h6 className="review-date">
                            {moment(review.rawDate).format("ll")}
                          </h6>
                        </div>
                      </Row>
                      <Row>
                        <div className="review-stars">
                          {Array.from(Array(review.reviewRating).keys()).map(
                            (_star, index) => (
                              <span key={`review-star-${index}`}>
                                <Icon type="star" theme="filled" />
                              </span>
                            )
                          )}
                        </div>
                        {review.reviewText.length > 50 ? (
                          <div className="review-text">
                            {review.reviewText.slice(0, 150).concat("...")}
                          </div>
                        ) : (
                          <div className="review-text">{review.reviewText}</div>
                        )}
                      </Row>
                    </div>
                  </a>
                </Col>
              );
            })}
          </Carousel>
        </Row>
      </div>
      <div className="view-all-reviews center">
        <a
          href="https://www.google.com/search?hl=en-MM&gl=mm&q=101+UPPER+CROSS+STREET,+PEOPLE%27S+PARK+CENTER,+B1+02+King+Travel+(@King+Jin+Corporation+Pte+Ltd),+Singapore+058357&ludocid=15750477537378982974&lsig=AB86z5V5mFpm1vn0ESqdUGexvewP#lrd=0x31da19c165137671:0xda94efdee23e5c3e,1"
          target="_blank"
        >
          View All Reviews
        </a>
      </div>
    </div>
  );
};
export default GoogleReviews;
