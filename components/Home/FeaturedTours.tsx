import { Carousel, Col, Row, Tabs } from "antd";
import { FeaturedTourCard } from "../Card/FeaturedTourCard";

const { TabPane } = Tabs;

const FeaturedTours = ({ tours }) => {

  const koreaTours = tours.filter((tour) =>
    tour.categories.find((cat) => cat.name === "ctry-korea")
  );
  const japanTours = tours.filter((tour) =>
    tour.categories.find((cat) => cat.name === "ctry-japan")
  );

  function callback(key) {
    console.log(key);
  }

  const settings = {
    autoplay: true,
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: false,
          dots: true
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          dots: true
        }
      }
    ]
  };

  return (
    <div className="featured-tours">
      <div className="wrap center p-t-60">
        <h3>FEATURED Tours</h3>
        <h5>推荐旅游套餐</h5>
      </div>
      <div className="wrap country-tabs">
        <Tabs defaultActiveKey="1" onChange={callback} type="card">
          <TabPane tab="Korea" key="1">
            <div>
              <Row>
                <Carousel {...settings}>
                  {koreaTours.map((item, index) => (
                    <Col key={`featured-card-item-${index}`}>
                      <FeaturedTourCard tour={item}/>
                    </Col>
                  ))}
                </Carousel>
              </Row>
            </div>
          </TabPane>
          <TabPane tab="Japan" key="2">
            <div>
              <Row>
                <Carousel {...settings}>
                  {japanTours.map((item, index) => (
                    <Col sm={24} md={12} lg={8} key={`featured-card-item-${index}`}>
                      <FeaturedTourCard tour={item}/>
                    </Col>
                  ))}
                </Carousel>
              </Row>
            </div>
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
};
export default FeaturedTours;
