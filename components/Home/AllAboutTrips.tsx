import { Col, Row } from "antd";
import { useContext } from "react";
import { getContent } from "web-component-antd";
import { CMSContext } from "../../pages";
import { LayoutContext } from "../Layouts/WrappedLayout";

const AllAboutTrips = () => {
  const { setShowForm, setInitialValue } = useContext(LayoutContext);
  const content = useContext(CMSContext);
  const titleSection = getContent(content).tag("all-about-trips").getValue();

  return (
    <div className="wrap all-about-trips">
      <Row type="flex" justify="space-between" gutter={60}>
        <Col xs={24} md={24} lg={12}>
          <div className="why-choose-us">
            <h3>{titleSection.title}</h3>
            <h5>{titleSection.code}</h5>
            <p>{titleSection.description}</p>
            <div className="why-choose-us-btn">
              <a href="/about">Why Choose Us?</a>
            </div>
          </div>
        </Col>
        <Col xs={24} md={24} lg={12}>
          <div>
            <Row>
              <Col xs={24} sm={12}>
                <div className="korea-and-japan" id="get-quote-korea">
                  <a
                    onClick={() => {
                      setShowForm(true);
                      setInitialValue({ destination: "korea-tour" });
                    }}
                  >
                    <div className="border">
                      <div
                        className="trip-img"
                        style={{
                          backgroundImage: `url("/img/korea-trip-img.png")`,
                          backgroundPosition: "center",
                          backgroundSize: "cover",
                          height: "312px",
                        }}
                      />
                    </div>
                    <div className="trip-label">
                      <span className="trip-label-quote">GET QUOTE</span>
                      <span className="trip-label-chinese"> (索取报价)</span>
                      <span className="trip-label-arrow"> {">>"}</span>
                    </div>
                  </a>
                </div>
              </Col>
              <Col xs={24} sm={12}>
                <div className="korea-and-japan" id="get-quote-japan">
                  <a
                    onClick={() => {
                      setShowForm(true);
                      setInitialValue({ destination: "japan-tour" });
                    }}
                  >
                    <div className="border">
                      <div
                        className="trip-img"
                        style={{
                          backgroundImage: `url("/img/japan-trip-img.png")`,
                          backgroundPosition: "center",
                          backgroundSize: "cover",
                          height: "312px",
                        }}
                      />
                    </div>
                    <div className="trip-label">
                      <span className="trip-label-quote">GET QUOTE</span>
                      <span className="trip-label-chinese"> (索取报价)</span>
                      <span className="trip-label-arrow"> {">>"}</span>
                    </div>
                  </a>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  );
};
export default AllAboutTrips;
