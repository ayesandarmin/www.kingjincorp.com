import { Col, Row } from "antd";

const TikTokBanner = () => {
  return (
    <div className="tiktok-banner" id="tiktok">
      <Row
        className="wrap tiktok-banner-content"
        type="flex"
        justify="space-around"
      >
        <Col xs={24} sm={24} md={24} lg={12}>
          <Row className="tiktok-banner-first-slide">
            <Col xs={12} sm={12} md={8}>
              <div
                style={{
                  backgroundImage: `url("/img/tiktok-qr1.png")`,
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                  height: "245px",
                  padding: "15px",
                  borderRadius: "8px 0 0 8px",
                }}
              />
            </Col>
            <Col xs={12} sm={12} md={16}>
              <div>
                <div
                  style={{
                    backgroundImage: `url("/img/tiktok-bg.png")`,
                    backgroundPosition: "center",
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                    height: "245px",
                    padding: "15px",
                    borderRadius: "0 8px 8px 0",
                  }}
                >
                  <div className="tiktok-title-section">
                    <h6>For International Users</h6>
                    <h1>king_jin_corp_sg</h1>
                    <h4>新加坡King哥</h4>
                    <a
                      href="https://www.tiktok.com/@king_jin_corp_sg?lang=en"
                      target="_blank"
                    >
                      Follow
                    </a>
                  </div>
                </div>
                <img
                  className="tiktok-label-icon"
                  src="/img/tiktok-label.png"
                />
              </div>
            </Col>
          </Row>
        </Col>
        <Col xs={24} sm={24} md={24} lg={12}>
          <Row className="tiktok-banner-sec-slide">
            <Col xs={12} sm={12} md={8}>
              <div
                style={{
                  backgroundImage: `url("/img/tiktok-qr2.png")`,
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                  height: "245px",
                  padding: "15px",
                  borderRadius: "8px 0 0 8px",
                }}
              />
            </Col>
            <Col xs={12} sm={12} md={16}>
              <div>
                <div
                  style={{
                    backgroundImage: `url("/img/tiktok-bg.png")`,
                    backgroundPosition: "center",
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                    height: "245px",
                    padding: "15px",
                    borderRadius: "0 8px 8px 0",
                  }}
                >
                  <div className="tiktok-title-section">
                    <h6>For Mainland China Users</h6>
                    <h1>king_jin_corp_sg</h1>
                    <h4>新加坡King哥</h4>
                    <a
                      href="https://www.douyin.com/user/MS4wLjABAAAA1E-K6sXIn47G8yMtuifFHPRRj-eajylctkB3vbRMtak"
                      target="_blank"
                    >
                      Follow
                    </a>
                  </div>
                </div>
                <img
                  className="tiktok-label-icon"
                  src="/img/tiktok-label-chinese.png"
                />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};
export default TikTokBanner;
