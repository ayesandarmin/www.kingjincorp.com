import { Carousel } from 'antd';
import React, { useContext } from 'react';
import { getContent } from "web-component-antd";
import { CMSContext } from '../../pages';
import TravelServiceCard from '../Card/TravelServiceCard';

const Services = () => {
  const content = useContext(CMSContext);
  const travelServices = getContent(content).tag("travel-services").getValue();

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 1314,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 1258,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 1014,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 682,
        settings: {
          arrows: false,
          slidesToShow: 1,
        },
      },
    ],
  };

  return (
    <div id="travel-services">
      <div className="wrap">
        <div className="title">{travelServices.title}</div>
        <Carousel {...settings}>
          {
            travelServices.photos.map((service, index) => {
              return (
                <TravelServiceCard service={service} key={`ServiceCard-${index}`} />
              )
          })}
        </Carousel>
      </div>
    </div>
  );
}

export default Services;