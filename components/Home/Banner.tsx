import { Carousel, Icon } from "antd";
import React, { useContext } from "react";
import { getContent } from "web-component-antd";
import { CMSContext } from "../../pages";
import { CarouselArrow } from "../Utilities/carousel-arrow";
import BannerSlide from "./BannerSlide";

const LeftArrowIcon = () => <Icon style={{ fontSize: "30px" }} type="left" />;
const RightArrowIcon = () => <Icon style={{ fontSize: "30px" }} type="right" />;

const setting = {
  arrows: true,
  autoplay: false,
  infinite: true,
  dots: true,
  speed: 500,
  prevArrow: (
    <CarouselArrow
      iconElement={
        <span className="carousel-arrow">
          <LeftArrowIcon />
        </span>
      }
    />
  ),
  nextArrow: (
    <CarouselArrow
      iconElement={
        <span className="carousel-arrow">
          <RightArrowIcon />
        </span>
      }
    />
  ),
};

const Banner = ({ allTours }) => {
  console.log(allTours);
  const content = useContext(CMSContext);
  const banner = getContent(content).tag("banner-section").getValue();

  return (
    <div className="home-slider">
      <Carousel {...setting}>
        {banner.photos.map((banner, index) => {
          return (
            <BannerSlide 
              banner={banner}
              key={`custom-banner-slide-${index}`}
            />  
          );
        })}
      </Carousel>
    </div>
  );
};

export default Banner;
