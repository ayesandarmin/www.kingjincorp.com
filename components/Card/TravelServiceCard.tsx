import { Card } from 'antd'
import { FixedAspectRatio } from '../Utilities/fixed-aspect-ratio'

const TravelServiceCard = ({ service }) => {
  
  return (
    <Card
      style={{ height: 543, width: 422 }}
      bordered={false}
      cover={
        <FixedAspectRatio
          ratio="77:100"
          imageUrl={service.photo.regular}
        />
      }
    >
      <div className="cover">
        <div className="title">{service.title}</div>
      </div>
    </Card>
  )
}

export default TravelServiceCard
