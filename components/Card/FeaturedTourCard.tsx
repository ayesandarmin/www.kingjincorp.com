import { Card } from "antd";
import Link from "next/link";
import { convertToUrl } from "../Hotels/functions";

export const FeaturedTourCard = ({ tour }) => {
  return (
    <div id="featured-card">
      <Link href={`/tour/${convertToUrl(tour.name)}/${tour.id}`} passHref>
          <Card
            cover={
              <div
                style={{
                  backgroundImage: `url(${ tour.photo_url ? tour.photo_url : "/img/no-photo.png" })`,
                  backgroundPosition: "center",
                  backgroundSize: "cover",
                  backgroundRepeat: "no-repeat",
                  height: 289,
                  borderRadius: '8px 8px 0 0'
                }}
              />
            }
          >
            <div>
                <div style={{ margin: 30 }}>
                  <div className="featured-card-section">
                    <div className="featured-card-title-section">
                      <h6>{tour.options[0].product_code}</h6>
                      <div className="featured-card-title">{tour.name}</div>
                      <div className="featured-card-title-chinese">
                        {tour.attributes["sub-tour-title"]}
                      </div>
                    </div>
                    <div className="featured-card-pricing-section">
                      <div style={{ paddingLeft: 20, paddingRight: 20 }}>
                        From <span>S$ {tour.options[0].TWN}</span> per pax /
                        twin-sharing
                      </div>
                      <div style={{ paddingLeft: 20, paddingRight: 20 }}>
                        Min. <span>{tour.attributes["min-pax"]} Pax</span> to
                        book
                      </div>
                    </div>
                    <div className="featured-card-btn">
                      <a href={`/tour/${convertToUrl(tour.name)}/${tour.id}`}>
                        View this itinerary
                      </a>
                    </div>
                  </div>
                </div>
            </div>
          </Card>
      </Link>
    </div>
  );
};
