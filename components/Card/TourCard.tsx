import { Card } from 'antd'
import React from 'react'
import { FixedAspectRatio } from '../Utilities/fixed-aspect-ratio'

const TourCard = ({ tour }) => {
  return (
    <div className="destination-card">
      <Card
        // style={{ height: "100%" }}
        bordered={false}
        hoverable={false}
        cover={
          <FixedAspectRatio
            ratio="1:1"
            imageUrl={tour.photo_url}
          />
        }
      >
        <div className="content">
          <div className="content-head">
            <div className="content-title">{tour.title}</div>
            <div className="price">{tour.price}</div>
          </div>
          <div className="desc">
            {tour.desc}
          </div>
        </div>
      </Card>
    </div>
  )
}

export default TourCard
