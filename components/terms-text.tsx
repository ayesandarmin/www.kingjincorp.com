import React from "react";
import { Collapse } from "antd";
import Head from "next/head";
import config from "../customize/config";

const Terms = () => {
  return (
    <section id="page" className="grey">
      <Head>
        <title>Terms and Conditions | {config.defaultTitle} </title>
      </Head>

      <div className="wrap">
        <h1>Terms & Conditions</h1>
        <p>
          <strong>
            PLEASE READ THE FOLLOWING TERMS &amp; CONDITIONS (the "Terms &amp;
            Conditions") CAREFULLY BEFORE USING THIS WEBSITE.
          </strong>
          <br /> Your use of this website (hereinafter referred to as the
          "Website") is expressly conditioned on your acceptance of the Terms
          &amp; Conditions. By using this Website, you signify that you
          unconditionally agree and accept to be legally bound by the Terms
          &amp; Conditions.&nbsp;
          <strong>
            If you do not agree with any part of the Terms &amp; Conditions, you
            must not use this Website.
          </strong>
          &nbsp;The Terms &amp; Conditions apply to the use of this Website.
          These Terms &amp; Conditions limit and exclude our liability. Your
          legal recourse for liability from using this Website will generally be
          limited to claims against third party service providers. If you are
          accessing our Website or initiating transactions on our Website, you
          will be deemed to have read, understood and agreed to these Terms
          &amp; Conditions.
        </p>
        <h3>HOTELS</h3>
        <Collapse bordered={false} defaultActiveKey={["1"]}>
          <Collapse.Panel header="1. HOTELS" key="1">
            <p>
              We sell hotel rooms on behalf of various hotels in Singapore &
              Overseas. Terms and conditions imposed by them therefore apply.
            </p>
            <ol type="a">
              <li>
                {" "}
                In case of No-Show of a booked room, a room night is chargeable.
              </li>
              <li>
                {" "}
                In case of a Cancellation/Amendment of a room already booked, a
                fee of $50.00* is payable.
              </li>
            </ol>
            <p>
              It should be noted that some hotels might not accept any
              cancellation/amendments of rooms booked (especially during peak
              periods) and in such cases passengers are not entitled to any
              refunds.
            </p>
          </Collapse.Panel>

          <Collapse.Panel header="2. TRAVEL INSURANCE" key="2">
            <p>
              We market specifically packaged travel insurance of AIG and Chubb.
              The insurance sold is subject to their respective terms and
              conditions stated in their respective Policies.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="3. MODE OF PAYMENT" key="3">
            <p>
              Payment may by made in Singapore Dollars by major credit cards or
              travel vouchers issued by the Company. In case of payment made via
              our Travel Vouchers, all the terms and condition contained therein
              are applicable.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="4. TRAVEL DOCUMENTS" key="4">
            <p>
              It is the customer's sole responsibility to ensure that his/her
              international passport has a validity of at least 6 months from
              the date of return to Singapore, and that relevant visas, ESTA,
              vaccinations and travel documents are gotten.
            </p>
            <p>
              The Company will wherever possible assist passengers to obtain the
              necessary visas but does not guarantee the approval from the
              Embassies/Consulates.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="5. CONFIDENTIALITY OF CUSTOMER DATA" key="5">
            <p>
              The company only collects the necessary customers' data to fulfill
              the purpose of completing every sale transaction and future
              in-house marketing. We will not disclose, share, transfer, sell or
              rent the date collected to third party.
            </p>
          </Collapse.Panel>
          <h3>USER AGREEMENT FOR HOTEL BOOKINGS</h3>
          <p>
            Your use of this website (hereinafter referred to as the "Website")
            is expressly conditioned on your acceptance of the User Agreement.
            By using this Website, you signify that you unconditionally agree
            and accept to be legally bound by the User Agreement. If you do not
            agree with any part of the User Agreement, you must not use this
            Website. The User Agreement applies to the use of this Website. This
            User Agreement limits and excludes our liability. Your legal
            recourse for liability from using this Website will generally be
            limited to claims against third party service providers or partner
            sites.
          </p>
          <p>
            If you are accessing our Website or initiating transactions on our
            Website, you will be deemed to have read, understood and agreed to
            this User Agreement. Royal Wings Travel Singapore Pte Ltd
            (hereinafter referred to as the "Company") is the operator of this
            Website. This Website, and each of its modules, together with the
            arrangement and compilation of the content found on this Website, is
            the copyrighted property of the Company and/or its various third
            party providers and distributors and which is licensed to the
            Company.
          </p>
          <Collapse.Panel header="1. WEBSITE CONTENT" key="h1">
            <p>
              The Company makes its best efforts to ensure that all the
              information appearing on our website is accurate and up to date.
              However, the Company does not accept liability for any errors,
              omissions and reserves the right to change this information at any
              time and without notice.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="2. HOTEL INFORMATION" key="h2">
            <p>
              The Company endeavors to ensure all information, description and
              photos provided by our hotel partners and/or suppliers are as
              accurate as possible. However, the Company cannot accept any
              liability for the accuracy of the hotels' information, which can
              sometimes be temporarily unavailable due to renovation or
              maintenance of the hotel, or inaccurate due to absence of update.
              The Company also cannot accept responsibility for any disturbance
              or inconvenience caused to you during your stay, nor for any
              accident in a hotel or loss caused by the hotel management or
              staff.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="3. CREDIT CARD INFORMATION" key="h3">
            <p>
              When making a booking on our website, you undertake to provide
              accurate, true and complete information to the Company, including
              valid credit card details. Failure to do so may result in an
              inaccurate booking or non-fulfilment of your booking. As such, the
              Company shall not be liable for any damage or loss incurred.
            </p>
            <p>
              The Company conducts random checks by calling and/or emailing you
              to verify your purchases made via our website. At times, we may
              request for photocopies of the credit card used (back and front)
              and you to fill up a credit card authorization payment form to
              minimize the possibility that the card may have been misused by an
              unauthorized third party. You agree to cooperate and provide
              necessary information to the Company without delay. In cases where
              the Company is carrying out random checks and is unable to contact
              you via email, or in urgent cases via phone, the Company reserves
              all rights to cancel the booking and you shall not hold the
              Company liable for any loss or damage incurred.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="4. ONLINE BOOKINGS" key="h4">
            <p>
              The Company does not guarantee the actual bedding configuration
              (e.g. single, double, twin, queen or king-size bed) of the room
              type. Your bookings are forwarded to the hotel and are subjected
              to availability upon check-in. All additional special requests,
              including but not limited to non-smoking room, high-floor,
              particular orientation etc., are also subjected to availability
              and cannot be guaranteed by the Company.
            </p>
            <p>
              In cases where the hotel is not able to honour your reservation
              due to overbooking, sudden increase of price, or any other
              circumstance, the Company endeavours to offer you an alternative
              hotel similar in standard. If this were to occur, the Company will
              contact you via email and/or phone. If you were to accept the
              alternative arrangement, the Company shall be deemed to have
              fulfilled its contractual obligations and you agree not to pursue
              any form of compensation from the Company, partner site and hotel.
              If you were to decline the alternative arrangement, a full refund
              of the amount paid by you to the Company will be made to you, and
              you agree to discharge the Company, partner site and hotel of all
              liabilities.
            </p>
            <p>
              The Company is not liable for any claim against non-fulfillment or
              unsatisfactory fulfillment of products and services purchased from
              our hotel partners, partner sites, tour operators, car hire
              companies, insurance companies, and other entities found on our
              website.
            </p>
            <p>
              Each product and service found on our website has their specific
              terms and conditions. Please refer to our website's and booking
              confirmation's terms and conditions, user agreement and privacy
              policy, and also the terms and conditions of the individual
              product and service before purchase.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="5. CANCELLATION POLICY" key="h5">
            <p>
              By making purchase on our website, you agree to the Company's
              Cancellation Policy.
            </p>
            <p>
              Hotels may implement cancellation policies that are different from
              the Company's. As such, additional cancellation charges may apply
              on top of the Company's cancellation charges, and you agree to
              bear these charges. In some cases, cancellation is prohibited and
              the booking becomes totally non-refundable.
            </p>
            <p>
              Cancellation needs to be confirmed by the Company's hotel
              reservation staff. If for any reason, you do not receive
              confirmation of your cancellation from our reservation staff after
              submission, you need to email to{" "}
              <a href="mailto:onlinebooking@royalwingstravel.com.sg">
                onlinebooking@royalwingstravel.com.sg
              </a>{" "}
              or call our Company at{" "}
              <a href={"tel:" + config.phone}>{config.phone}</a> for assistance.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="6. AMENDMENT POLICY" key="h6">
            <p>
              By making purchase on our website, you agree to the Company's
              Amendment Policy.
            </p>
            <p>
              Hotels may implement amendment policies that are different from
              the Company's. As such, additional amendment charges may apply on
              top of the Company's amendment charges, and you agree to bear
              these charges. In some cases, amendment is prohibited and the
              booking becomes totally non-refundable.
            </p>
            <p>
              Amendment needs to be confirmed by the Company's hotel reservation
              staff. If for any reason, you do not receive confirmation of your
              amendment from our reservation staff after submission, you need to
              email to{" "}
              <a href="mailto:onlinebooking@royalwingstravel.com.sg">
                onlinebooking@royalwingstravel.com.sg
              </a>{" "}
              or call our Company at{" "}
              <a href={"tel:" + config.phone}>{config.phone}</a> for assistance.
            </p>
            <p>
              Our Company levies an administrative fee of S$30 per booking on
              top of any charge the hotel may impose. These charges appear on
              our website during the booking process.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="7. MODE OF COMMUNICATION" key="h7">
            <p>
              You agree to accept email as the dominant mode of communication
              between you and the Company. Booking confirmation via email shall
              therefore satisfy the legal requirement for binding communications
              between you and the Company. You also agree to email to{" "}
              <a href="mailto:onlinebooking@royalwingstravel.com.sg">
                onlinebooking@royalwingstravel.com.sg
              </a>{" "}
              or call our Company at{" "}
              <a href={"tel:" + config.phone}>{config.phone}</a> for assistance
              your booking status in case you do not hear from us. In doing so,
              you agree not to hold the Company liable for any non-receipt of
              hotel voucher or email confirmation or booking confirmation. In
              cases where the Company cancels or amends your booking due to
              various reasons, including but not limited to incomplete booking
              information or payment details or incomprehensible instructions,
              and emails you the cancellation or amendment notice, the Company
              does not take responsibility for your failing to be notified by
              our email, and the Company shall not be liable for any damage or
              loss incurred. Please be reminded to check your contact email
              regularly.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="8. DISCLAIMER" key="h8">
            <p>
              Although the Company takes reasonable care to ensure that
              published descriptions are correct, the Company does not own or
              operate hotels or other accommodation or travel services. The
              Company accepts no liability for errors or omissions in the
              description of accommodation or other travel services on the
              website, and the Company reserves the right to change information
              without prior notice. In some circumstances, the Company may not
              be able to honour the booking confirmation for reasons beyond our
              control. These reasons may include, but not limited to, government
              regulations, environmental, system downtime or natural disasters.
            </p>
            <p>
              Payment or booking transactions over the Internet may be subjected
              to interruption, blackout or delays during transmission.
            </p>
            <p>
              In extreme circumstances, the Company may be forced to cancel your
              booking. You will be notified as soon as possible and a full
              refund will be made to you. In no circumstance shall the Company
              be liable for any consequential and direct and indirect loss or
              damage. Our liability shall be limited to the amount you paid to
              us for the booking.
            </p>
            <p>
              The Company shall not be liable for any failure or delay in
              performance of our obligations, which results directly or
              indirectly from any cause or circumstance, which is beyond our
              reasonable control. Without limiting the generality of the
              foregoing, the following shall be regarded as such circumstances:
              act of God, outbreak of hostilities, riot, civil disturbance, acts
              of terrorism, revolution, the act of any government or authority
              (including but not limited to refusal or revocation of any licence
              or consent), fire, flood, epidemic, lightning, explosion, fog or
              bed weather, interruption or failure of a utility service
              (including but not limited to electricity, gas, water or
              telecommunications), renovations undertaken by the accommodation
              providers, strikes, lockouts or boycotts, embargo, blockade.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="9. GOVERNING LAW & JURISDICTION" key="h9">
            <p>
              The User Agreement, its performance and all disputes arising out
              of or in connection with the User Agreement shall be governed by
              the laws of Singapore. You hereby consent and submit to the
              exclusive jurisdiction of the courts of the Republic of Singapore
              in all questions and controversies arising out of or in connection
              with the User Agreement, its performance and your use of this
              website.
            </p>
          </Collapse.Panel>
          <h3>TOUR PACKAGES</h3>
          <Collapse.Panel header="1. CANCELLATIONS / AMENDMENTS" key="t1">
            <p>
              Cancellation of confirmed booking from customers must be made in
              writing to avoid misunderstanding. The following charges will
              apply accordingly:
            </p>
            <p>For tours operated by the Company</p>
            <table className="info-table">
              <tbody>
                <tr>
                  <th rowSpan={2}>
                    {" "}
                    Period of Cancellation Notice <br />
                    (from departure date)
                  </th>
                  <th colSpan={2}> Cancellation Charges Per Person</th>
                </tr>
                <tr>
                  <th>Normal Tour Packages&nbsp;</th>
                  <th>Chartered Flights & Air / Cruise</th>
                </tr>
                <tr>
                  <td>30 days and more</td>
                  <td colSpan={2}>Deposit Non-Refundable </td>
                </tr>
                <tr>
                  <td>15 to 29 days</td>
                  <td>75% of Tour Fare </td>
                  <td rowSpan={2}>100% Tour Fare</td>
                </tr>
                <tr>
                  <td>14 days and below</td>
                  <td>100% of Tour Fare</td>
                </tr>
              </tbody>
            </table>
            <p>
              For tours or components supplied by third parties, e.g. cruises,
              Insight Vacations, Contiki, hotels, airlines, car rentals, trains
              etc., cancellation charges under the terms and conditions of the
              respective third parties shall apply, plus a handling charge of
              S$50 per service per person.
            </p>
            <p>
              <b>Cancellation of Tour by Company</b>
              <br />
              Tour members should be aware that the Company is acting as an
              agent for the service suppliers. After a deposit or full payment
              is made, all arrangements are still subject to final confirmation.
              When the arrangement cannot be confirmed, the Company will
              endeavor to notify customer at least 2 weeks (for group tour and
              tour of 7 days and above), or 1 week (for tour of 6 days and
              below) before departure, and a refund of payment of tour fare will
              be made if the tour is cancelled. The Company shall not be liable
              for any further responsibility. The Company reserves the rights to
              cancel any tour prior to departure for any reason, including but
              not limited to, an insufficient number of participants. The
              Company may, if it so decides, recommend alternative tour
              arrangements.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="2. AMENDMENTS" key="t2">
            <p>
              <b>General Amendments</b> - Any amendments (excluding departure
              date, tour type or air tickets) made by customers after
              reservations have been confirmed, an administrative fee of S$50*
              per person per amendment will be imposed by the Company. For
              amendments to departure date or tour type, cancellation charges
              stated above will apply.
            </p>
            <p>
              <b>Amendment to Itineraryby Company</b> - The company shall
              endeavor not to make any material alternation to the tour package.
              However, the Company reserves the right to make minor changes at
              any time due to unforeseen circumstances without compensation,
              especially during peak season.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="3. REFUND" key="t3">
            <p>
              <b>Refund on Tour Package (excluding air ticket component)</b>{" "}
              Payment by credit cards made by customers, refund will be made
              through the credit card companies and take approximately 4-6
              weeks, or approximately 2 months during peak periods.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="4. MODE OF PAYMENT" key="t4">
            <p>
              Payment must be in Singapore Dollars and may be made by major
              credit cards or travel vouchers issued by the Company. The Company
              reserves the rights not to accept cheque payments. If accepted,
              cheques must be presented to the Company at least 7 working days
              prior to departure date. For payment by travel vouchers, all terms
              & conditions stipulated on the travel vouchers have to be met
              before it can be accepted as a mode of payment.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="5.  TOUR FARE EXCLUDES" key="t5">
            <ul>
              <li>Visa fees</li>
              <li>
                All taxes and fuel surcharges imposed by relevant authorities
                and airlines.
              </li>
              <li>
                Meals, beverages, room service or any other item not listed in
                the itinerary.
              </li>
              <li>
                Excess baggage charges, laundry charges, travel insurance, and
                all items of a personal nature.
              </li>
              <li>
                Gratuities to driver, tour guide, tour leader, hotel porters
                etc.
              </li>
            </ul>
            <p>
              Tour prices are based on current airfares, service fees,
              government and airport taxes, fuel surcharges and exchange rates,
              applicable at the time of print and quotation, and are subject to
              changes without prior notice. Any difference in tour prices due to
              new regulations being effected or late instructions by the
              customer to issue travel documents will be borne by the customer
              even after full payment had been made. Any non-compliance may
              result in automatic cancellation of reservation and forfeiture of
              your payment.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="6. CHILD FARE" key="t6">
            <p>
              <b>Eligibility</b>: Below 12 years of age (on the date of return)
            </p>
            <p>
              Child fare is based on sharing a room with adults and no
              additional bed will be provided. There will be an additional cost
              for child sharing one room with an adult or when an extra bed is
              required.
            </p>
            <p>
              In some countries, due to fire regulation, it is compulsory for
              each person to have their own bed. Booking with four persons in a
              room will not be allowed unless the hotel has quad-sharing rooms
              available
            </p>
          </Collapse.Panel>

          <Collapse.Panel header="7. ACCOMODATION" key="t7">
            <p>
              In the event the accommodation booked or requested is not
              available, every effort will be made for an alternative in another
              accommodation of similar standard.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="8. TRAVEL REQUEST" key="t8">
            <p>
              If there are any requests regarding special meals, dietary
              requirements, adjoining rooms etc., please inform the Company when
              booking. Such requests are subject to confirmation and
              availability.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="9. TRAVEL INSURANCE" key="t9">
            <p>
              All customers are strongly recommended to purchase travel
              insurance to cover such areas as trip cancellation, personal
              baggage, personal accident, injury & illness. Under no
              circumstance is the Company to be construed as a carrier under
              contract for safe carriage of customer or his/her baggage &
              belonging. Our staff will be pleased to assist in the enquiries of
              any travel insurance.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="10. TRAVEL DOCUMENTS" key="t10">
            <p>
              It is the customer's sole responsibility to ensure that his/her
              international passport has a validity of at least 6 months from
              the date of return, and that relevant visas, ESTA, vaccinations
              and travel documents are gotten. For Singapore Permanent
              residents, please bring along your exit permits when travelling.
            </p>
            <p>
              Where possible, the Company will assist the customer to obtain the
              necessary visas. Service charges and visa fees will be borne by
              the customer. The Company does not guarantee the approval of the
              visa application. If, for any reason, application for visa or exit
              permit is rejected, a full refund of all monies paid (excluding
              visa application fees) will be made if the result of the rejection
              is made known to the Company at least 35 days prior to departure.
              If less than 35 days' notice is given, the relevant cancellation
              charges as stated, plus other charges if applicable, will apply.
              The Company cannot be held responsible for any expense,
              reimbursement or refund of tour or travel arrangement charges if
              any customer is refused entry or transit by any country at any
              point during the tour or travel for whatsoever reason, including
              but not limited to, lack of necessary visas.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="11. RESPONSIBILITIES" key="t11">
            <p>
              The Company and its associates act only as an agent for the
              transportation companies, hotels & other principals for the tour
              programs. They accept no responsibilities for injuries, damage,
              accident, loss, delay, theft, quarantine, customs regulation,
              strike, changes in itinerary, deportation or refusal of entry by
              Immigration Authorities resulting from improper travel documents,
              possession of unlawful items or irregularities that may be caused
              to person or property. Any loss & expense are the responsibility
              of the customer. All proper travel documentation are the sole
              responsibility of the customer. The Company reserves the right to
              alter itineraries, travel arrangements, hotels reservations etc.
              If it is necessary or in the case of force majeure, the Company
              reserves the right to cancel any tour prior to date of departure
              for any reason, including insufficient number of participants.
              Should this happen, the entire payment shall be refunded without
              further obligation on the part of the Company.
            </p>
            <p>
              The Company will recommend alternative tour, preferably to the
              same destination or other destinations. Should customer decide not
              to accept the alternative, all monies paid shall be refunded in
              full by the Company without further obligations. The Company also
              reserves the right to require any individual to withdraw from the
              tour if it is deemed that his/her act of conduct is detrimental to
              or incompatible with the interest, harmony and welfare of other
              passengers and the tour as a whole. The Company shall be under no
              further liability thereafter to any such person. No tour
              leaders/guides or other employees or agents of the Company are
              authorized to commit the Company to any liability whatsoever and
              the Company will not be bound by any statement or representation
              unless in writing and signed by a Management Executive of the
              Company. The Company reserves the right to take photographs and
              films of customers while on tour with the Company to be used for
              brochure advertising or publicity material without obtaining any
              further consent from customers. The Company shall not be liable to
              the customers for any goods purchases by the customers during the
              tour, whether or not the goods are of defective quality, not
              suitable for the customer's purpose, not in conformity with sample
              provided to the customers or rejected by the customer for any
              reason whatsoever, notwithstanding that the goods are purchased in
              shops which comprise part of the tour package (including
              itinerary), or which are specifically recommended by the travel
              services, their guides, servants, employees or independent
              contractors, nor the Company or the travel services be liable to
              the customers for the purchase monies of the aforesaid goods.
            </p>
            <p>
              The Company reserves the right to change, amend, insert or delete
              any of the terms and conditions, or policies contained in this
              document, as the case may be, without prior notice.
            </p>
          </Collapse.Panel>
          <Collapse.Panel
            header="12.  CONFIDENTIALITY OF CUSTOMER DATA"
            key="t12"
          >
            <p>
              The Company only collects the necessary customers' data to fulfill
              the purpose of completing every sales transaction and future
              in-house marketing. The Company will not disclose, share,
              transfer, sell or rent the data collected to third party.
            </p>
          </Collapse.Panel>
          <h3>USE OF THE WEBSITE</h3>
          <Collapse.Panel header="1. OWNERSHIP" key="w1">
            <p>
              Royal Wings Travel Singapore Pte Ltd (hereinafter referred to as
              the "Company") is the operator of this Website. This Website, and
              each of its modules, together with the arrangement and compilation
              of the content found on this Website, is the copyrighted property
              of the Company and/or its various third party providers and
              distributors and which is licensed to the Company. No content
              found in this Website may be printed, reproduced, stored in a
              retrieval system or transmitted in any form without prior
              permission from the Company.
            </p>
            <p>
              In addition, the trademarks, logos and service marks displayed on
              this Website (collectively, the&nbsp;<strong>"Trademarks"</strong>
              ) are registered and common law Trademarks of the Company, its
              affiliates, and various third parties. Nothing contained on this
              Website should be construed as granting, by implication, estoppel,
              or otherwise, any license or right to use any of the content on
              the Website or Trademarks without the prior written permission of
              the Company or such other party that may own the content on the
              Website or Trademarks.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="2. USE OF WEBSITE" key="w2">
            <p>
              The Company grants you a limited, non-exclusive, non-transferable,
              non-sub-licensable and revocable right to use this Website in
              accordance with the Terms and Conditions. You agree that you:
            </p>
            <ol type="a">
              <li>
                {" "}
                shall only use this Website to make legitimate reservations or
                purchases and shall not use this Website for any other purposes,
                including without limitation, to make any speculative, false or
                fraudulent reservation or any reservation in anticipation of
                demand.
              </li>
              <li>
                shall not employ misleading email addresses or falsify
                information in the header, footer, return path, or any part of
                any communication, including emails, transmitted through this
                Website.
              </li>
              <li>
                shall not, except as permitted under applicable law, copy,
                reproduce, republish, upload, post, transmit, distribute or
                otherwise communicate or caused to be displayed to the public
                any of the content provided in this Website, including the text,
                graphics, button icons, audio and video clips, digital
                downloads, data compilations and software, without the prior
                written permission of the Company and/or its third party
                providers and distributors.
              </li>
              <li>
                {" "}
                shall not use any "robot," "spider", "crawler" or other
                automated device, or a program, algorithm or methodology having
                similar processes or functionality, or any manual process, to
                monitor or copy any of the web pages, data or content found on
                this Website, in any case without the prior written permission
                of the Company.
              </li>
              <li>
                shall not, except as permitted under applicable law, alter,
                modify, translate, decompile, adapt, disassemble, reverse
                engineer or create derivative works from any content or
                component from this Website.
              </li>
              <li>
                shall not transmit or otherwise transfer any web pages, data or
                content found on this Website to any other computer, server, web
                site, or other medium for mass distribution or for use in any
                commercial enterprise. The use of such materials on any other
                web site or in any environment of networked computers is
                prohibited.
              </li>
              <li>
                shall not use any device, software or routine to interfere or
                attempt to interfere with the proper working of this Website.
              </li>
              <li>
                shall not take any action that imposes a burden or load on our
                infrastructure that the Company deems in its sole discretion to
                be unreasonable or disproportionate to the benefits the Company
                obtains from your use of the Website.
              </li>
              <li>
                shall not make any unauthorized use of this Website and/or the
                materials contained on this Website to violate applicable
                copyright, trademark or other intellectual property laws or
                other laws. You must retain all copyright and trademark notices,
                including any other proprietary notices, contained in the
                materials, and you must not alter, obscure or obliterate any of
                such notices.
              </li>
              <li>
                shall not post or transmit any information which (i) infringes
                the rights of others or violates their privacy or publicity
                rights, (ii) is protected by copyright, trademark or other
                proprietary right, unless with the express written permission of
                the owner of such right, (iii) contains a virus, bug, worm ,
                time bomb, trojan horse, trap door or other harmful item, (iv)
                is used to unlawfully collude against another person in
                restraint of trade or competition, or (v) creates any liability
                on the Company or causes the Company to lose (in whole or in
                part) the services of our internet service providers or other
                suppliers.
              </li>
            </ol>
            <p>
              You shall be solely liable for any damages resulting from any
              infringement of copyright, trademark, or other proprietary right,
              or any other harm or loss resulting from your use of this Website.
            </p>
            <p>
              The Company is not under any obligation to update any content on
              this Website. Although reasonable care has been taken in preparing
              the information displayed on this Website, the Company cannot
              guarantee the accuracy of all this information. It is your sole
              responsibility to contact the relevant employee(s) of the Company
              to verify. As such, the Company cannot accept any liability or
              loss arising from any error, omission and inaccuracy found in this
              Website.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="3. REGISTRATION" key="w3">
            <p>
              Use of this Website may require you to register and provide
              certain data. In consideration of the Company permitting you to
              use the Website, in registering and providing such data, you
              represent and warrant that: (a) the information about yourself is
              true, accurate, current, and complete (apart from optional items)
              as required by various registration forms on the Website (the
              "Membership Data") and (b) you will do your best to maintain and
              promptly update the Membership Data to keep it true, accurate,
              current and complete. If you provide any information that is
              untrue, inaccurate, not current or incomplete, or the Company has
              reasonable grounds to suspect that such information is untrue,
              inaccurate, not current or incomplete, the Company has the right
              to suspend or terminate your account and refuse any and all
              current or future access to and use of this Website. The Company
              shall not be liable for any losses or harm which you may suffer,
              where the loss or harm is attributable to untrue, inaccurate,
              non-current or incomplete information provided by you.
            </p>
            <p>
              Upon registering, you will receive a password and user ID. You are
              responsible for maintaining the confidentiality of the password
              and user ID, and are fully responsible for all activities that
              occur under your password or user ID. You may only maintain one
              user ID and password at any one time and may not use multiple user
              IDs or passwords. You are required to use your password for your
              own use only. You may not permit others to use your passwords. You
              agree to (a) immediately notify the Company upon discovery of any
              unauthorized use of your password or account or any other breach
              of security, and (b) ensure that you exit from your account at the
              end of each session. The Company cannot and will not be liable for
              any loss or damage arising from your failure to comply with this
              requirement.
            </p>
            <p>
              You represent that you are of sufficient legal age to use this
              Website and to create binding legal and financial obligations for
              any liability you may incur as a result of the use of this
              Website. You understand that you are legally and financially
              responsible for all uses of this Website by you and those using
              your login information.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="4. PRIVACY & MONITORING" key="w4">
            <p>
              You consent to the use and processing of your membership data and
              other personally identifiable user information by the Company
              and/or its third party providers and distributors in accordance
              with the terms of and for the purposes set forth in the Company's
              Privacy Policy. You acknowledge and agree that the technical
              processing of this Website and the transmission of transaction
              messages relating to the products and services on this Website may
              involve transmissions of your membership data and other data
              collected and maintained by the Company with regard to you, over
              various networks and to various countries. You will not hold the
              Company responsible for events arising from third parties gaining
              unauthorized access to such data. You agree that if you submit
              suggestions, ideas, comments or questions or post any other
              information on this Website, you grant the Company and its
              affiliates a worldwide, non-exclusive, royalty-free, perpetual,
              irrevocable, and fully sub-licensable right to use, reproduce,
              modify, adapt, publish, translate, create derivative works from,
              distribute and display such content in any form, media or
              technology. The Company takes no responsibility and assumes no
              liability for any content posted or submitted by you to this
              Website.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="5. ONLINE BOOKINGS & PURCHASES" key="w5">
            <p>
              <b>Contracts for products and services</b> <br />
              Unless otherwise expressly stated on the Website, the Company acts
              as agent for its third party providers and distributors for all
              products and services purchased by you on this Website. A contract
              for the sale and purchase of products and services may be made
              online at this Website by your acceptance of the products and
              services offered on the Website (the "Contract") by third party
              providers and/or distributors and/or where stated, the Company,
              and you hereby agree to waive any rights to challenge the validity
              or enforceability of Contracts entered into on this Website on the
              grounds that it was made in electronic form instead of by paper
              and/or signed or sealed. Please note that by indicating your
              acceptance to purchase any product or service offered on the
              Website, you are obligated to complete such transactions. You are
              solely responsible for checking that the booking made contains the
              correct information (including dates, names and timings). The
              Company and the third party providers and distributors shall be
              entitled to rely on the information contained in your booking and
              shall not be responsible for any error(s) made in your booking.
            </p>
            <p>
              <b>Third party travel service providers</b>
              <br />
              You may be bound by terms and conditions imposed by the travel
              service providers for whom the Company acts as an agent including
              conditions of carriage, refund and cancellation policies of
              airlines, cruise lines, car hire operators, hotels, etc. The
              Company is not liable for any claims against non-fulfillment or
              unsatisfactory fulfillment of products and services purchased on
              your behalf by the Company from these third party providers and
              distributors, such as, but not limited to, airlines, hotels, tour
              operators, car hire companies, insurance companies, and other
              entities. At times, airlines and other travel services providers
              may overbook passengers on their flights or re-schedule flight
              times. The Company is not responsible for any such incidents.
            </p>
            <p>
              <b>Fulfillment</b>
              <br />
              Any booking or enquiry made in this Website cannot be construed as
              having been received and processed immediately by the Company,
              unless a relevant employee of the Company has acknowledged to you
              by telephone. Even then, the acknowledged booking or enquiry
              requires a minimum processing time of varying periods. The Company
              will not be held liable for any claims for loss, damages or
              compensation against non-fulfillment should you purchase a product
              or service that cannot be fulfilled between the time of booking or
              enquiry, and the time of departure. In such an event, the Company
              shall refund to you all unutilised monies collected from you for
              that purchase, subjected to the terms and conditions of that
              product or service.
            </p>
            <p>
              <b>Passport, Visa and Health information</b>
              <br />
              Many countries require that foreign nationals entering hold a
              valid passport with at least 6 months of validity from the date of
              return. This information as well as other related information
              supplied by the Company or any of its affiliates and any link
              within the Website are supplied in good faith, and should be
              treated as a guideline only. Possessing valid and relevant
              passports, visas (including, but not limited to, transit visas)
              and meeting health requirements are the sole responsibilities of
              the travellers. The Company reserves the right to decline any
              booking or purchase for any reason and shall not be held liable
              for any resulting claim for loss, damages or compensation.
            </p>
            <p>
              <b>Over-lapping bookings</b>
              <br />
              You agree that multiple over-lapping bookings for the same
              traveller/s are not allowed and may result in damage and loss to
              the Company, which you will bear in full.
            </p>
            <p>
              <b>Specific terms and conditions for products and services</b>
              <br />
              Certain products and services have their own specific governing
              terms and conditions which shall apply in addition to the Terms
              and Conditions. It is important for you to obtain and read the
              terms and conditions applicable to these certain products and
              services as they may contain exclusions of limitations of
              liability and other terms and conditions, including restrictions
              on amendment or cancellation. Some of these terms and conditions
              for specific products and services shall include terms and
              conditions found on your screens and when using any part of this
              Website.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="6. PRICES" key="w6">
            <p>
              All prices and availability displayed for the products and
              services in this Website are subjected to changes without prior
              notice, and such prices and availability are not final until you
              have been instructed by an employee of the Company to make partial
              or full payment for the product(s) and service(s) concerned.
            </p>
          </Collapse.Panel>
          <Collapse.Panel
            header="7. CANCELLATION OF & CHANGES TO BOOKING & PURCHASE"
            key="w7"
          >
            <p>
              Please note that the terms and conditions for cancellation of and
              changes to bookings and purchases will vary for each product and
              service, and therefore you are advised to check with the relevant
              Company's employee(s) during office hours. You are not allowed to
              cancel or amend your booking or purchase via this Website. You
              must contact the relevant employee(s) of the Company via telephone
              or make a visit to the Company's office to effect any cancellation
              or amendment. Please note that cancellation or amendment may not
              be allowed in certain cases, and if allowed, may require a minimum
              processing time of varying periods, subject to the specific terms
              and conditions applicable to the type of product or service booked
              or purchased. Cancellation or amendment may be subjected to
              cancellation or amendment fees imposed by the Company and, but not
              limited to, the relevant travel service provider(s), which you
              agree to bear in full and immediately pay up.
            </p>
            <p>
              The Company reserves the right to decline any booking or purchase
              for any reason and shall not be held liable for any resulting
              claims for loss, damages or compensation. In such an event, the
              Company shall refund to you all unutilised monies collected from
              you for that purchase. You agree to bear the full cost of any
              booking, cancellation and administration fees for any product or
              service booked but not utilised for any reason. In some cases, the
              Company may charge cancellation and/or amendment fees in addition
              to those imposed by travel service providers. Any refund is
              subjected to the Company's policy in terms of the amount,
              processing time and the mode of refund. The Company reserves all
              rights to determine the appropriate refund procedure.
            </p>
            <p>
              For online hotel bookings, please note that a change to your
              booking such as a change of date, a change in hotel or a change in
              room type, constitutes a cancellation, not amendment. Please check
              with us on what constitutes a cancellation or amendment if in
              doubt.
            </p>
          </Collapse.Panel>
          <Collapse.Panel
            header="8. THIRD PARTY WEB SITES, CONTENT AND PRODUCTS AND SERVICES"
            key="w8"
          >
            <p>
              You acknowledge and agree that your correspondence and business
              dealings with any third parties, including any merchants or
              advertisers, found on or accessed through this Website, including
              payment for and delivery of any related goods and services, and
              all other terms, conditions, representations, and warranties
              related to such dealings, are solely as between you and such third
              parties. You understand and acknowledge that some of the services
              accessible through this Website are provided by third parties and
              may make use of the Company's name, logo and/or other identifying
              marks of the Company, and that in certain instances, the Company
              may receive referral fees and/or other compensation from third
              parties on a per use or other basis based on your purchase or use
              of third party goods or services found on or accessed through this
              Website. YOU ACKNOWLEDGE THAT IN SUCH DEALINGS WITH THIRD PARTIES,
              THE COMPANY IS NOT THE SELLER OR PROVIDER AND YOUR AGREEMENT OF
              PURCHASE OR FOR SERVICES WITH SUCH THIRD PARTY IS SOLELY BETWEEN
              YOU AND THE THIRD PARTY, AND NOT THE COMPANY. THE COMPANY ASSUMES
              NO RESPONSIBILITY WHATSOEVER FOR ANY CHARGES, LOSSES OR
              LIABILITIES YOU, OR ANY USER OF YOUR ACCOUNT, INCURS WHEN MAKING
              PURCHASES, DEALING WITH OR COMPLETING TRANSACTIONS WITH SUCH THIRD
              PARTIES. By accessing third party services through this Website,
              you hereby authorize such third party service providers to provide
              the Company with personal information regarding your use of and
              your activities with respect to the purchase and use of third
              party services and/or goods.
            </p>
            <p>
              Hypertext links or pointers to third party web sites and
              references to products and services offered by third parties are
              provided to you for convenience only and do not constitute an
              endorsement or approval by the Company of:
            </p>
            <ol type="a">
              <li> the organizations that operate such web sites;</li>
              <li>
                the information, content, privacy policies or other terms of use
                on such web sites or;
              </li>
              <li>such third party products and services.</li>
            </ol>
            <p>
              As the Company has no control or responsibility over, and does not
              investigate, monitor, or check for accuracy third party web sites
              or information and/or content maintained by other organizations,
              or for products and services offered by third parties, the Company
              does not assume any liability for your use of any of the
              foregoing. If you decide to exit this Website and access third
              party web sites, you acknowledge and agree that you do so AT YOUR
              OWN RISK and you should be aware that these Terms and Conditions
              no longer govern.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="9. TRAVEL RESOURCES" key="w9">
            <p>
              Travel resources including, but not limited to, currency
              converter, weather information, country information, passport and
              visa requirements and electric voltage, found in various parts of
              this Website are based on various publicly available sources and
              should be used as guidelines only. The above information are
              provided solely for your convenience and general references and
              are not verified as accurate. Travel resources are not updated
              every day. The Company is not under any obligation to update the
              travel resources on this Website. Although reasonable care has
              been taken in preparing the information displayed on this Website,
              the Company cannot guarantee the accuracy of all these
              information. It is your sole responsibility to verify with the
              appropriate parties. As such, the Company cannot accept any
              liability or loss arising from any error, omission and inaccuracy
              resulting from the use of the travel resources found in this
              Website or third party web sites that have hypertext links in or
              pointed from this Website.
            </p>
            <p>
              We do not authorize the use of this information for any purpose
              other than your personal use and you are expressly prohibited from
              the resale, redistribution, and use of this information for
              commercial purposes.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="10. AGE AND RESPONSIBILITY" key="w10">
            <p>
              You represent that you are of legal age to use the booking
              facility in accordance with these Terms and Conditions and to
              create binding legal obligations for any liability you may incur
              as a result of the use of the Website. You are financially
              responsible for all uses of this Website by yourself and those
              using your login information. You will supervise all usage of the
              Website under your name or account. You warrant that all
              information supplied by you and members of your household in using
              the Website are true and accurate.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="11. DISCLAIMER" key="w11">
            <p>
              The materials in this Website are provided "as-is" and without
              warranties of any kind either express or implied. To the fullest
              extent permissible and subject and pursuant to applicable law, the
              Company disclaims all warranties, express or implied, including,
              but not limited to, implied warranties of satisfactory quality,
              merchantability and fitness for a particular purpose. The Company
              does not warrant that the functions contained in the materials
              will be uninterrupted or error-free, that defects will be
              corrected or that this Website or this server that makes it
              available is free of any virus or other harmful elements. The
              Company does not warrant or make any representations regarding the
              correctness, accuracy, reliability, or otherwise of the materials
              in this Website or the results of their use.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="12. LIMITATION OF LIABILITY" key="w12">
            <p>
              You agree that the Company is a mere agent for the providers of
              services available through the Website, including but not limited
              to travel services. The relationship between the Company and you
              will be that of independent contractors, and neither party nor any
              of the company's respective officers, agents or employees will be
              held or construed to be partners, joint ventures, fiduciaries,
              employees or agents of the other. Except as required by statute or
              government regulation, the Company shall not be liable for any
              loss or damage caused by a user's reliance on information obtained
              through this Website or a linked site, or user's reliance on any
              product or service obtained from a linked site. Any and all claims
              regarding any failure or breach with respect to the services
              offered through the Website are limited to claims against any and
              all service providers. The Company hereby disclaims any liability,
              whether based in contract, tort, strict liability or otherwise,
              including without limitation, liability for any direct, punitive,
              special, consequential, incidental or indirect damages, in
              connection with (i) any use of this Website or content found
              herein, (ii) any failure or delay (including, but not limited to,
              the use of or inability to use any component of this Website for
              reservations or ticketing), or (iii) the performance or
              non-performance by the Company or any third party providers or
              distributors, including, but not limited to, non-performance
              resulting from bankruptcy, reorganization, insolvency, dissolution
              or liquidation provided by any carrier or other supplier through
              the Website, liability for any act, error, omission, injury, loss,
              accident, delay or irregularity which may be incurred through the
              fault, negligence or otherwise, of such carrier or supplier, lost
              profits, cost of procuring substitute service or lost opportunity
              arising out of or in connection with the use of the Website or a
              linked site, or with the delay or inability to use the Website or
              a linked site, even if the Company is made aware of the
              possibility of such damages, action must be taken to prevent or
              limit such damage and you hereby agree to exonerate and hold
              harmless the Company , its affiliates and any of their respective
              officers, directors, employees, or agents from any liability with
              respect to the same. This limitation on liability includes, but is
              not limited to, the transmission of any virus which may infect a
              user's equipment, failure of mechanical or electronic equipment or
              communication lines, telephone or other interconnect problems
              (e.g., you cannot access your internet service provider),
              unauthorized access, theft, operator errors, strikes or other
              labour problems or any force majeure.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="13. INDEMNIFICATION" key="w13">
            <p>
              You agree to defend and indemnify the Company, the Company's
              affiliates, and/or their respective suppliers and any of their
              officers, directors, employees and agents from and against any
              claims, causes of action, demands, recoveries, losses, damages,
              fines, penalties or other costs or expenses of any kind or nature,
              including but not limited to, reasonable legal and accounting
              fees, brought by third parties as a result of:
            </p>
            <ol type="a">
              <li>
                your breach of this Terms and Conditions or the documents
                referenced herein;
              </li>
              <li>
                your violation of any law or the rights of a third party or
              </li>
              <li>your use of this Website</li>
            </ol>
          </Collapse.Panel>
          <Collapse.Panel header="14. GOVERNING LAW & JURISDICTION" key="w14">
            <p>
              The Terms and Conditions, its performance and all disputes arising
              out of or in connection with the Terms and Conditions shall be
              governed by the laws of Singapore. You hereby consent and submit
              to the exclusive jurisdiction of the courts of the Republic of
              Singapore in all questions and controversies arising out of or in
              connection with the Terms and Conditions, its performance and your
              use of this Website.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="15. INJUNCTIVE RELIEF" key="w15">
            <p>
              You acknowledge that a violation or attempted violation of any of
              the Terms and Conditions will cause such damage to the Company as
              will be irreparable, the exact amount of which would be difficult
              to ascertain and for which there will be no adequate remedy at
              law. Accordingly, you agree that the Company shall be entitled as
              a matter of right to an injunction issued by any court of
              competent jurisdiction, restraining such violation or attempted
              violation of these terms and conditions by you, or your
              affiliates, partners, or agents, as well as recover from you any
              and all costs and expenses sustained or incurred by the Company in
              obtaining such an injunction, including, without limitation, all
              legal fees and expenses. You agree that no bond or other security
              shall be required of the Company in connection with such
              injunction.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="16. TERMINATION RELIEF" key="w16">
            <p>
              The Company may terminate the Terms and Conditions and/or the
              provision of any of the products and services at any time for any
              reason, including any improper use of this Website or your failure
              to comply with the Terms and Conditions. Such termination shall
              not affect any right to relief to which the Company and its third
              party providers or distributors may be entitled, at law or in
              equity. Upon termination of the Terms and Conditions, all rights
              granted to you will terminate and revert to the Company and its
              third party providers or distributors, as applicable.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="17. MODIFICATION TO WEBSITE" key="w17">
            <p>
              The Company may at any time modify any part of the Website,
              including the products and services and the Terms and Conditions
              at its sole discretion without prior notice, and your continued
              use of this Website will be subject to the Terms and Conditions in
              force at the time of your use. The Company also reserves the right
              to deny any person access and use of the Website at any time
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="18. ADDITIONAL TERMS" key="w18">
            <p>
              Additional terms and conditions may apply to reservations,
              purchases of products and services and other uses of portions of
              this Website, and you agree to abide by such other terms and
              conditions, including additional terms and conditions that apply
              in relation to specific products and services.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="19. SEVERABILITY" key="w19">
            <p>
              The Terms and Conditions shall be severable. In the event that any
              provision is determined to be unenforceable, illegal or invalid,
              such provision shall nonetheless be enforced to the fullest extent
              permitted by applicable law, and such determination shall not
              affect the enforceability, legality and validity of any other
              remaining provisions.
            </p>
          </Collapse.Panel>
          <Collapse.Panel header="20. ENTIRE AGREEMENT" key="w20">
            <p>
              The Terms and Conditions, together with any terms and conditions
              incorporated herein or referred to herein constitute the entire
              agreement between us relating to the subject matter hereof, and
              supersedes any prior understandings or agreements (whether oral or
              written) regarding the subject matter, and may not be amended or
              modified except in writing or by making such amendments or
              modifications available on this Website.
            </p>
          </Collapse.Panel>
        </Collapse>
      </div>
    </section>
  );
};
export default Terms;
