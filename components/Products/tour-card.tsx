import { Button, Col, Row } from "antd";
import Link from "next/link";
import React from "react";
import { useOriginalImage } from "../../helpers/common-function";
import { convertToUrl } from "../Hotels/functions";
import { FixedAspectRatio } from "../Utilities/fixed-aspect-ratio";

export const VacationTourCard = ({ tour }) => {
  const cheapestPrice = tour.options.reduce((acc, cur) => {
    // if (cur.departures.length) {
    const curTwn = parseFloat(cur.TWN);
    if (curTwn < acc || acc === 0) {
      acc = curTwn;
    }
    // }
    return acc;
  }, 0);
  return (
    <Link href={`/tour/${convertToUrl(tour.name)}/${tour.id}`}>
      <a>
        <div className="vacation-card">
          <Row>
            <Col md={12}>
              {/* <div
                style={{
                  backgroundImage: `url(${tour.photo_url})`,
                  backgroundPosition: "center",
                  height: 185,
                }}
                className="vacation-img"
              >
              </div> */}
              <FixedAspectRatio
                ratio="6:4"
                imageUrl={useOriginalImage(tour.photo_url)}
              />

              {/* <div className="vacation-img">
                <img src={useOriginalImage(tour.photo_url)} />
              </div> */}
            </Col>
            <Col md={12}>
              <div className="vacation-content">
                <h4>{tour.name}</h4>
                <h6>{tour?.attributes?.["short-desc"]}</h6>
              </div>
              {/* <h6>
                  {tour.categories
                    .map((category) => category.description)
                    .join(" + ")}
                </h6> */}
              <div className="vacation-footer">
                <div className="vacation-divider" />
                <div className="vacation-description">
                  <div className="fs-17">
                    <span className="black fs-13">From:</span>
                    <br />
                    <span className="black">
                      {cheapestPrice > 0 && <strong>S${cheapestPrice} </strong>}
                    </span>
                  </div>
                  <Button className="vacation-btn">Explore</Button>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </a>
    </Link>
  );
};

// export const VacationTourCard = ({ tour }) => {
//   // const cheapestPrice = tour.options.reduce((acc, cur) => {
//   //   if (cur.departures.length) {
//   //     const curTwn = parseFloat(cur.TWN);
//   //     if (curTwn < acc || acc === 0) {
//   //       acc = curTwn;
//   //     }
//   //   }
//   //   return acc;
//   // }, 0);
//   return (
//     <Link href="#">
//       <div className="vacation-card">
//         <Row>
//           <Col md={12}>
//             <div className="vacation-img">
//               <img src={tour.photo_url} />
//             </div>
//           </Col>
//           <Col md={12}>
//             <div className="vacation-content">
//               <h4>{tour.name}</h4>
//               <h6>{tour.short_desc}</h6>
//               <div className="vacation-divider" />
//               <div className="vacation-footer">
//                 <div className="fs-17">
//                   <span className="black">From:</span>
//                   <br />
//                   <span className="black">
//                     {/* {cheapestPrice > 0 && <strong>S${cheapestPrice} </strong>} */}
//                   </span>
//                 </div>
//                 <Button className="vacation-btn">Explore</Button>
//               </div>
//             </div>
//           </Col>
//         </Row>
//       </div>
//     </Link>
//   );
// };
