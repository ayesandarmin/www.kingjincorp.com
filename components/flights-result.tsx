import React, { Fragment } from "react";
import { TcResponse, Cart } from "travelcloud-antd/travelcloud";
import { Button, Icon, Row, Col, Collapse, Popover } from "antd";
import { NoResult } from "travelcloud-antd/components/no-result";
import { FlightSearch, FlightDetail } from "travelcloud-antd/types";
import Big from "big.js";
import { Element } from "react-scroll";
import {
  computeOd,
  computePrice,
  ignoreTimezoneExtractTime,
  ignoreTimezoneExtractDate,
  computeBaggage,
  getBestCabinFromSegments
} from "travelcloud-antd/components/flights-result";

const Od = ({ od, source }: { od: FlightSearch[0]['od1'], source: string }) => {
  const odInfo = computeOd(od)

  return <section className="flightinfo border">
    <Row>
      <Col lg={6}>
        <img
          src={`//cdn.net.in/static/airlines/${odInfo.departure_marketing_carrier.code}.gif`}
        />
      </Col>
      <Col lg={7}>
        {odInfo.departure_local_date}
      </Col>
      <Col lg={11}>
        {odInfo.departure_operating_carrier != null && odInfo.departure_marketing_carrier.code !== odInfo.departure_operating_carrier.code
          && <span className="flight-opr-full-name">
            Operated by:
                {odInfo.departure_operating_carrier.name.substring(0, 15)}
          </span>
        }
      </Col>
    </Row>
    <ul className="timing">
      <li className="long">
        <h6>Depart</h6>
        <h2>
          <strong>
            {odInfo.departure_airport.code}
          </strong>
          &nbsp;
            {odInfo.departure_local_time}
        </h2>
      </li>
      <li className="long2">
        <div className="stop-num">
          <h2>
            <span className="direct">
              {odInfo.stops.display_with_airport_codes}
              <br />
              {odInfo.total_time.display}
            </span>
          </h2>
        </div>
      </li>
      <li className="long">
        <h6>Arrive</h6>
        <h2>
          <strong>
            {odInfo.arrival_airport.code}
          </strong>
          &nbsp;
            {odInfo.arrival_local_time}
        </h2>
      </li>
    </ul>
    {source !== 'pkfare' &&
      <Collapse bordered={false} className="details">
        <Collapse.Panel header="Show details" key={"i"}>
          {od.segments.map((segment, key) => (
            <Row
              key={key}
              type="flex"
              justify="space-between"
              align="middle" gutter={24}
            >
              <Col xs={24} md={4} className="flightno">
                {segment.marketing_carrier.code}
                {segment.flight_number}
              </Col>
              <Col xs={24} md={20}>
                {segment.flights.map((flight, key) => (
                  <div key={`ds1` + key}>
                    <p>
                      Depart{" "}
                      <strong>
                        {flight.departure_airport.code}
                      </strong>{" "}
                      {" - " + flight.departure_airport.name} at{" "}
                      {ignoreTimezoneExtractTime(
                        flight.departure_datetime
                      )}
                      ,{" "}
                      {ignoreTimezoneExtractDate(
                        flight.departure_datetime
                      )}
                      <br />
                      Arrive{" "}
                      <strong>
                        {flight.arrival_airport.code}
                      </strong>{" "}
                      {" - " + flight.arrival_airport.name} at{" "}
                      {ignoreTimezoneExtractTime(
                        flight.arrival_datetime
                      )}
                      ,{" "}
                      {ignoreTimezoneExtractDate(
                        flight.arrival_datetime
                      )}
                    </p>
                  </div>
                ))}
              </Col>
            </Row>
          ))}
        </Collapse.Panel>
      </Collapse>}
  </section>
}
export function renderFares(flightIndex: number, cart: Cart, flight: FlightDetail, onFareClick?: (index: number, flight: FlightDetail, fareId: string) => any) {
  const indexServices = (acc2, fare, fareDetailIndex) => {
    if (acc2[fareDetailIndex] == null) acc2[fareDetailIndex] = {
      origin: fare.segments[0].origin,
      destination: fare.segments[fare.segments.length - 1].destination,
      servicesSet: {}
    }
    const brand = fare.brand
    if (brand == null) return acc2
    const services = brand.services
    if (services == null) return acc2
    acc2[fareDetailIndex].servicesSet = services.reduce((acc3, service) => {
      acc3[service.name] = true
      return acc3
    }, acc2[fareDetailIndex].servicesSet)
    return acc2
  }

  const fares = flight.pricings
  var fareServicesIndexed = fares.reduce((acc, fare) => {
    return fare.fares.reduce(indexServices, acc)
  }, new Array(fares[0].fares.length))

  fareServicesIndexed = fareServicesIndexed.map(x => {
    x.servicesSet = Object.keys(x.servicesSet)
    return x
  })

  return <div className="list">
    <div className="col-details">
      <div className="space">
      </div>
      {fareServicesIndexed.map((info, index) => {
        return <div key={index}>
          <div className="head">{info.origin.code} → {info.destination.code}</div>
          {info.servicesSet.map(serviceName => <div key={serviceName} className="body">{serviceName}</div>)}
        </div>
      })}
    </div>
    {fares.map((detail, i1) => {
      const computed = computePrice(cart, flight, detail.id)
      return <div key={i1} className="col-details">
        <div className="col-price">
          <div className="price">{computed.afterDiscount}</div>
          <div><Button type="primary" onClick={() => onFareClick(flightIndex, flight, detail.id)}>Select</Button></div>
        </div>
        {detail.fares.map((fare, i2) => {
          const servicesIndexed = fare.brand == null || fare.brand.services == null ? {} : fare.brand.services.reduce((acc, service) => {
            acc[service.name] = service
            return acc
          }, {})
          return <div key={i2}>
            <div className="head">{fare.brand != null && fare.brand.name != null ? fare.brand.name : getBestCabinFromSegments(fare.segments)}</div>
            {fareServicesIndexed[i2].servicesSet.map((serviceName, index) => {
              return <div key={index} className="icon">
                {servicesIndexed[serviceName] != null && servicesIndexed[serviceName].included === true ? <Icon style={{ color: '#7C7' }} type="check" /> : <Icon style={{ color: '#C77' }} type="close" />}
              </div>
            })}
          </div>
        })}
      </div>
    })}
  </div>
}

export const FlightsResult: React.StatelessComponent<{
  flights: FlightSearch;
  flightMap: { [key: string]: TcResponse<FlightDetail> };
  onFlightClick?: (index: number, flight: any) => any;
  onFareClick?: (index: number, flight: FlightDetail, fareId: string) => any;
  cart: Cart;
}> = ({ flights, flightMap, onFlightClick, onFareClick, cart }) => {
  return (
    <>
      {flights.map((flight, index) => {
        const computed = computePrice(cart, flight, flight.pricings[0].id)
        const baggage = computeBaggage(flight.pricings[0].fares[0])

        return (
          <Fragment key={index}>
            <div className={"ant-card flight-result"}>
              <div className="filter-result-section">
                <Row type="flex" justify="start">
                  <Col xs={24} sm={24} lg={18}>
                    <Od od={flight.od1} source={flight.pricings[0].source} />
                    {flight.od2 != null && <Od od={flight.od2} source={flight.pricings[0].source} />}
                  </Col>
                  <Col xs={24} sm={24} lg={6}>
                    <section className="travel-fair">
                      <h2 className="total-price">
                        {" "}
                        {computed.beforeDiscount ===
                          computed.afterDiscount ? (
                          computed.beforeDiscount
                        ) : (
                          <>
                            <span style={{ textDecoration: "line-through" }}>
                              {computed.beforeDiscount}
                            </span>
                              &nbsp;
                            <b>{computed.afterDiscount}</b>
                          </>
                        )}
                      </h2>
                      <div className="query-and-info">
                        <Popover overlayClassName={`fares`}
                          content={
                            <table className="fare-breakdown-table">
                              {Object.entries(flight.pricings[0].ptc_breakdown_map).map((val, n) => {

                                if (val[1].base_fare) return <Fragment key={n}>
                                  <tr>
                                    <td>{val[0].toUpperCase()} Base Fare</td>
                                    <td>x{val[1].quantity}</td>
                                    <td>${Big(val[1].base_fare).toFixed(2)}</td>
                                  </tr>
                                  <tr>
                                    <td>{val[0].toUpperCase()} Taxes and Charges</td>
                                    <td>x{val[1].quantity}</td>
                                    <td>${Big(val[1].price).minus(val[1].base_fare).toFixed(2)}</td>
                                  </tr>
                                </Fragment>

                                return <tr key={n}>
                                  <td>{val[0] == 'adt' ? 'Adult' : val[0] == 'cnn' ? 'Child' : 'Infant'}</td>
                                  <td> x {val[1].quantity}</td>
                                  <td>= ${Big(val[1].price).toFixed(2)}</td>
                                </tr>
                              })}
                            </table>}
                          trigger="click">
                          <p className="hasinfo">
                            <Icon type="question-circle" /> Fare Breakdown
                            </p>
                        </Popover>

                        {/*<p className="hasinfo">
                          <Icon type="info-circle" /> Fare Rules
                        </p>*/}
                        {baggage
                          && (
                            <p>
                              <Icon type="shopping" /> {baggage}
                            </p>
                          )}
                      </div>
                      {/*<small className="tax">(inclusive of $80.2 tax)</small>
           <p><img src="../static/images/icon-bag.png"/> 30kg</p>*/}
                      {flightMap[index] != null ? (
                        <Button className="select sel-deal open" type="primary">
                          {" "}
                          Select <Icon type="right" />
                        </Button>
                      ) : (
                        <Button
                          className="select sel-deal"
                          type="primary"
                          onClick={() => {
                            onFlightClick(index, flight);
                          }}
                        >
                          {" "}
                            Select <Icon type="right" />
                        </Button>
                      )}
                    </section>
                  </Col>
                </Row>
              </div>
              {flightMap[index] != null && (
                <Element name="options">
                  {flightMap[index].result == null ? (
                    <div className="options">
                      <NoResult
                        response={flightMap[index]}
                        type="fares"
                        loadingMessage="Verifying prices and availability..."
                      />{" "}
                    </div>
                  ) : (
                    <div className="options">
                      <Collapse
                        bordered={false}
                        defaultActiveKey={[`op${index}`]}
                      >
                        <Collapse.Panel header="Select Option" key={`op${index}`}>
                          {flightMap[index].result &&
                            renderFares(
                              index,
                              cart,
                              flightMap[index].result,
                              onFareClick
                            )}
                        </Collapse.Panel>
                      </Collapse>
                    </div>
                  )}
                </Element>
              )}
            </div>
          </Fragment>
        );
      })}
    </>
  );
};
