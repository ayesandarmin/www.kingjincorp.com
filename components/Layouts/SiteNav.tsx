import React, { useContext, useState } from "react";
import Link from "next/link";
import { Button, Menu, Drawer, Row, Col } from "antd";
import { LayoutContext } from "./WrappedLayout";
import { getContent } from "web-component-antd";
import config from "../../customize/config";
import { useRouter } from "next/router";
// import { QuotationFormComponent } from "../Utilities/contact";

const { SubMenu } = Menu;

export function SiteNav({ openCart }) {
  const router = useRouter();
  const { windowSize, layoutContent, setShowForm, setInitialValue } =
    useContext(LayoutContext);

  const header = getContent(layoutContent).tag("header").getValue();

  const [drawer, setDrawer] = useState(false);

  const scrollToTikTok = () => {
    var element = document.getElementById("tiktok");
    element.scrollIntoView({ behavior: "smooth" });
  };

  const menus = header.content;

  const selectedKey = (): string => {
    const pathName =
      router.pathname === "/" ? "home" : router.pathname.slice(1);
    return pathName.charAt(0).toUpperCase() + pathName.slice(1);
  };

  const selected = (link: string): string => {
    return router.asPath &&
      (link === router.asPath ||
        router.asPath.includes(link.length > 1 ? link : "home"))
      ? "ant-menu-item-active"
      : "";
  };

  return (
    <section id="main-header">
      <section id="top-bar">
        <div className="wrap">
          <div className="get-quote">
            <a
              className="ant-dropdown-link"
              onClick={() => {
                setShowForm(true);
                setInitialValue({ destination: undefined });
              }}
            >
              <span className="quote-eng">GET QUOTE </span>
              <span className="quote-chinese">(索取报价) </span>
              <span className="quote-arrow"> {`>>`}</span>
            </a>
          </div>
        </div>
      </section>
      <div className="wrap-xl">
        {windowSize.width > 768 && windowSize.width < 992 ? (
          <div id="top-header">
            <Row type="flex" justify="center" align="middle">
              <Col span={7}>
                <div className="logo center" style={{ textAlign: "right" }}>
                  <a href="/">
                    <img src="/img/king-travel-logo-circle.png" />
                  </a>
                </div>
              </Col>
              <Col span={3}>
                <div className="circle-divider center">
                  <img src="/img/circle-divider.png" />
                </div>
              </Col>
              <Col span={14}>
                <div
                  className="phone"
                  style={{ textAlign: "left", marginBottom: 20 }}
                >
                  <h5>Call Us</h5>
                  <a href={`tel:${config.phone}`} target="_blank">
                    (65) 6970 7044
                  </a>
                </div>
                <div className="email" style={{ marginBottom: 10 }}>
                  <h5>Email</h5>
                  <a href={`mailto:${config.email}`} target="_blank">
                    admin@kingjincorp.com
                  </a>
                </div>
              </Col>
            </Row>
          </div>
        ) : (
          <div id="top-header">
            <Row type="flex" justify="center" align="middle">
              <Col xs={24} md={24} lg={7}>
                <div className="phone">
                  <h5>Call Us</h5>
                  <a href={`tel:${config.phone}`} target="_blank">
                    (65) 6970 7044
                  </a>
                </div>
              </Col>
              <Col xs={24} md={24} lg={3}>
                <div className="circle-divider center">
                  <img src="/img/circle-divider.png" />
                </div>
              </Col>
              <Col xs={24} md={24} lg={4}>
                <div className="logo center">
                  <a href="/">
                    <img src="/img/king-travel-logo-circle.png" />
                  </a>
                </div>
              </Col>
              <Col xs={24} md={24} lg={3}>
                <div className="circle-divider center">
                  <img src="/img/circle-divider.png" />
                </div>
              </Col>
              <Col xs={24} md={24} lg={7}>
                <div className="email center">
                  <h5>Email</h5>
                  <a href={`mailto:${config.email}`} target="_blank">
                    admin@kingjincorp.com
                  </a>
                </div>
              </Col>
            </Row>
          </div>
        )}

        <div id="bottom-header">
          {windowSize.width < 798 ? (
            <Row type="flex" justify="end" align="middle">
              <div className="p-r-20">
                <Button
                  className="color-primary m-t-5"
                  shape="circle"
                  icon="menu"
                  size="large"
                  onClick={() => setDrawer(true)}
                />
                <div>
                  <Drawer
                    placement="left"
                    closable={true}
                    width={"100%"}
                    onClose={() => setDrawer(false)}
                    visible={drawer}
                  >
                    <Menu
                      onClick={() => setDrawer(false)}
                      selectedKeys={[selectedKey()]}
                      mode="inline"
                      className="m-t-60"
                    >
                      {menus.slice(0, 3).map((menu, index) =>
                        menu.subMenuItems ? (
                          <SubMenu
                            key={`menu-${index}`}
                            className="padding-left-reduce"
                            title={
                              <div>
                                <a
                                  className={`${
                                    selected(menu.link)
                                      ? "ant-menu-submenu-selected"
                                      : ""
                                  }`}
                                >
                                  {menu.name}
                                </a>
                              </div>
                            }
                          >
                            {menu.subMenuItems.map((subMenu) => (
                              <Menu.Item key={subMenu.name}>
                                <Link href={subMenu.link || "#"}>
                                  <div>
                                    <a
                                      className={`${
                                        selected(menu.link)
                                          ? "active-sub-menu"
                                          : ""
                                      }`}
                                    >
                                      {subMenu.name}
                                    </a>
                                  </div>
                                </Link>
                              </Menu.Item>
                            ))}
                          </SubMenu>
                        ) : (
                          <Menu.Item
                            key={menu.name}
                            className={`p-x-0 ${selected(menu.link)}`}
                          >
                            <div>
                              <Link href={menu.link || "#"}>
                                <div>
                                  <a>{menu.name}</a>
                                </div>
                              </Link>
                            </div>
                          </Menu.Item>
                        )
                      )}
                      <Menu.Item key={"tiktok-menu"} className="p-x-0">
                        <div>
                          <a onClick={scrollToTikTok}>TikTok</a>
                        </div>
                      </Menu.Item>
                      {menus.slice(3).map((menu, index) =>
                        menu.subMenuItems ? (
                          <SubMenu
                            key={`menu-${index}`}
                            className="padding-left-reduce"
                            title={
                              <div>
                                <a
                                  className={`${
                                    selected(menu.link)
                                      ? "ant-menu-submenu-selected"
                                      : ""
                                  }`}
                                >
                                  {menu.name}
                                </a>
                              </div>
                            }
                          >
                            {menu.subMenuItems.map((subMenu) => (
                              <Menu.Item key={subMenu.name}>
                                <Link href={subMenu.link || "#"}>
                                  <div>
                                    <a
                                      className={`${
                                        selected(menu.link)
                                          ? "active-sub-menu"
                                          : ""
                                      }`}
                                    >
                                      {subMenu.name}
                                    </a>
                                  </div>
                                </Link>
                              </Menu.Item>
                            ))}
                          </SubMenu>
                        ) : (
                          <Menu.Item key={menu.name} className="p-x-0">
                            <div>
                              <Link href={menu.link || "#"}>
                                <div>
                                  <a
                                    className={`${
                                      selected(menu.link)
                                        ? "active-sub-menu"
                                        : ""
                                    }`}
                                  >
                                    {menu.name}
                                  </a>
                                </div>
                              </Link>
                            </div>
                          </Menu.Item>
                        )
                      )}
                      <Menu.Item key={"cart"} className="p-x-0">
                        <div>
                          <div>
                            <a
                              onClick={() => {
                                openCart();
                              }}
                            >
                              <img
                                src="/img/cart.png"
                                style={{
                                  width: 20,
                                  height: 20,
                                }}
                              />
                              <span> My Cart</span>
                            </a>
                          </div>
                        </div>
                      </Menu.Item>
                    </Menu>
                  </Drawer>
                </div>
              </div>
            </Row>
          ) : (
            <Row type="flex" justify="center" align="middle">
              <Menu
                onClick={() => {}}
                selectedKeys={[selectedKey()]}
                mode="horizontal"
                className="m-t-5"
              >
                {menus.slice(0, 3).map((menu, index) =>
                  menu.subMenuItems ? (
                    <SubMenu
                      key={`menu-${index}`}
                      title={
                        <div>
                          <a
                            className={`${
                              selected(menu.link)
                                ? "ant-menu-submenu-selected"
                                : ""
                            }`}
                          >
                            {menu.name}
                          </a>
                        </div>
                      }
                    >
                      {menu.subMenuItems.map((subMenu) => (
                        <Menu.Item key={subMenu.name}>
                          <Link href={subMenu.link || "#"}>
                            <div>
                              <a
                                className={`${
                                  selected(menu.link) ? "active-sub-menu" : ""
                                }`}
                              >
                                {subMenu.name}
                              </a>
                            </div>
                          </Link>
                        </Menu.Item>
                      ))}
                    </SubMenu>
                  ) : (
                    <Menu.Item
                      key={menu.name}
                      className={`p-x-0 ${selected(menu.link)}`}
                    >
                      <div>
                        <Link href={menu.link || "#"}>
                          <div>
                            <a>{menu.name}</a>
                          </div>
                        </Link>
                      </div>
                    </Menu.Item>
                  )
                )}
                <Menu.Item key={`tiktok`} className="p-x-0">
                  <div>
                    <a onClick={scrollToTikTok}>TikTok</a>
                  </div>
                </Menu.Item>
                {menus.slice(3).map((menu, index) =>
                  menu.subMenuItems ? (
                    <SubMenu
                      key={`menu-${index}`}
                      title={
                        <div>
                          <a
                            className={`${
                              selected(menu.link)
                                ? "ant-menu-submenu-selected"
                                : ""
                            }`}
                          >
                            {menu.name}
                          </a>
                        </div>
                      }
                    >
                      {menu.subMenuItems.map((subMenu) => (
                        <Menu.Item key={subMenu.name}>
                          <Link href={subMenu.link || "#"}>
                            <div>
                              <a
                                className={`${
                                  selected(menu.link) ? "active-sub-menu" : ""
                                }`}
                              >
                                {subMenu.name}
                              </a>
                            </div>
                          </Link>
                        </Menu.Item>
                      ))}
                    </SubMenu>
                  ) : (
                    <Menu.Item key={menu.name} className="p-x-0">
                      <div>
                        <Link href={menu.link || "#"}>
                          <div>
                            <a
                              className={`${
                                selected(menu.link) ? "active-sub-menu" : ""
                              }`}
                            >
                              {menu.name}
                            </a>
                          </div>
                        </Link>
                      </div>
                    </Menu.Item>
                  )
                )}
                <Menu.Item key={"cart"} className="p-x-0">
                  <div>
                    <div>
                      <a
                        onClick={() => {
                          openCart();
                        }}
                      >
                        <img
                          src="/img/cart.png"
                          style={{ width: 20, height: 20, marginTop: "-5px" }}
                        />
                      </a>
                    </div>
                  </div>
                </Menu.Item>
              </Menu>
            </Row>
          )}
        </div>
      </div>
    </section>
  );
}
