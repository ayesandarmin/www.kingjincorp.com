import React, { useEffect, useLayoutEffect, useState } from 'react';
import Head from 'next/head'
import { Button, Col, Layout, Row, Modal} from 'antd';
import $ from "jquery";
import config from '../../customize/config';
import { ContactForm } from '../TravelCloud/ContactForm';

const { Content } = Layout;

const Maintenance = () => {
  const [windowSize, setWindowSize] = useState<any>({});
  const [modal, setModal] = useState(false);

  useEffect(() => {
    checkWindowSize();
    window.addEventListener("resize", checkWindowSize)
  }, [])

  const checkWindowSize = () => {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight
    })
  }
  // smoke animation using jquery
  useLayoutEffect(() => {
    (function () {
      var a = 0;
      for (; a < 25; a += 1) {
        setTimeout(function b() {
          var a = Math.random() * 1e3 + 5e3,
            c = $("<div />", {
              "class": "smoke",
              css: {
                left: Math.random() * 800,
                backgroundSize: "contain",
                width: Math.random() * 800,
                height: Math.random() * 600
              }
            });
          $(c).appendTo("#smoke-viewport");
          $.when(
            $(c).animate({}, {
              duration: a / 4,
              easing: "linear",
              queue: false,
              complete: function () {
                $(c).animate({}, {
                  duration: a / 3,
                  easing: "linear",
                  queue: false
                })
              }
            }),
            $(c).animate({
              bottom: $("#smoke-viewport").height()
            }, {
              duration: a,
              easing: "linear",
              queue: false
            })
          ).then(
            function () {
              $(c).remove();
              b()
            })
        }, Math.random() * 3e3)
      }
    })();
  }, [])

  return <>
    <Head>
      <title>Silk Way Travel Maintenance</title>
      <link rel="stylesheet" type="text/css" href="/fonts/style.css" />
    </Head>
    <Layout className="layout">
      <Content id="maintenance">
        {windowSize.width < 768 ?
          <Row>
            <Col>
              <div
                style={{ backgroundImage: `url(/img/arches.png)` }}

              // className="screen-height"
              >
                <div className="p-30">
                  <img className="logo-img m-b-40" src="/img/logo.png" />
                  <h1 className="font-molle color-accent-4 fs-64">We Are Open!</h1>
                  <Row gutter={24}>
                    <Col span={2}>
                      <div className="bg-primary m-t-9 w-25 h-3"></div>
                    </Col>
                    <Col span={22}>
                      <p className="font-bold color-primary">SILKWAY TRAVEL ASIA PTE LTD</p>
                      <p><strong>Phone:</strong> {config.phone}</p>
                      <p><strong>Email:</strong> {config.email}</p>
                      <p><strong>Operating Hrs:</strong> {config.operatingHrs}</p>
                      <Button className="m-t-20 silk-primary-btn" onClick={() => setModal(true)}>Send an enquiry <img width={20} height={20} className="m-l-5 m-b-2" src="/img/right-move.png" /></Button>
                      <Modal
                        centered
                        visible={modal}
                        onCancel={() => setModal(false)}
                        footer={false}
                      >
                        <ContactForm />
                      </Modal>
                    <h5 className="font-semi-bold m-t-35">@2021 Silkway Travel Asia Pte Ltd / STB's License TA <a className="color-primary" target="_blank" href={`https://trust.stb.gov.sg/site/content/tagaem/landing-page/travel-agent/travel-agent-details.html?licenceNo=03392`}><span className="color-primary underline">#03392</span></a></h5>
                    </Col>
                  </Row>
                </div>
                <div
                  style={{ backgroundImage: `url(/img/brewing-under-construction.jpg)`, height: '500px' }}
                  className="bg-cover bg-center relative"
                >
                  <div id="smoke-viewport"></div>
                  <div className="right-text-position-mobile text-color">
                    <h2 className="font-semi-bold">Something's Brewing... Watch This Space</h2>
                    <p className="">We are busy working on something awesome for the site.</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
          : <Row>
            <Col md={12}>
              <div
                style={{ backgroundImage: `url(/img/arches.png)` }}
                className="screen-height"
              >
                <div className="left-text-position">

                  <img className="m-b-80" src="/img/logo.png" />
                  <h1 className="font-molle color-accent-4 fs-64">We Are Open!</h1>
                  <Row gutter={24}>
                    <Col span={2}>
                      <div className="bg-accent-1 m-t-9 w-25 h-3"></div>
                    </Col>
                    <Col span={22}>
                      <p className="font-bold color-accent-1">SILKWAY TRAVEL ASIA PTE LTD</p>
                      <p><strong>Phone:</strong> {config.phone}</p>
                      <p><strong>Email:</strong> {config.email}</p>
                      <p><strong>Operating Hrs:</strong> {config.operatingHrs}</p>
                      <Button className="m-t-40 silk-primary-btn" onClick={() => setModal(true)}>Send an enquiry <img width={20} height={20} className="m-l-5 m-b-2" src="/img/right-move.png" /></Button>
                      <Modal
                        centered
                        visible={modal}
                        onCancel={() => setModal(false)}
                        footer={false}
                      >
                        <ContactForm />
                      </Modal>
                    </Col>
                  </Row>
                </div>
                <h5 className="left-text-position-bottom font-semi-bold">@2021 Silkway Travel Asia Pte Ltd / STB's License TA <a className="color-primary" target="_blank" href={`https://trust.stb.gov.sg/site/content/tagaem/landing-page/travel-agent/travel-agent-details.html?licenceNo=03392`}><span className="color-primary underline">#03392</span></a></h5>
              </div>
            </Col>
            <Col md={12}>
              <div
                style={{ backgroundImage: `url(/img/brewing-under-construction.jpg)` }}
                className="screen-height bg-cover bg-center"
              >
                <div id="smoke-viewport"></div>
                <div className="right-text-position text-color">
                  <h1 className="font-semi-bold">Something's Brewing... Watch This Space</h1>
                  <p className="">We are busy working on something awesome for the site.</p>
                </div>
              </div>
            </Col>
          </Row>
        }
      </Content>
    </Layout>
  </>
}

export default Maintenance;
