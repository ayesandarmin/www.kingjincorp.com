import { Col, Modal, Row } from "antd";
import React, { useContext, useState } from "react";
import { LayoutContext } from "./WrappedLayout";

export function SiteFooter() {
  const [visible, setVisible] = useState(false);
  const { setShowForm, setInitialValue } = useContext(LayoutContext);

  const handleVisibleChange = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div id="site-footer">
        <div className="wrap">
          <div className="p-x-30">
            <Row type="flex" justify="space-between" gutter={30}>
              <Col xs={24} sm={24} md={12} lg={12} xl={6}>
                <div>
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8137407277654!2d103.84176421450334!3d1.2857672621367586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da19c165137671%3A0xda94efdee23e5c3e!2sKing%20Travel%20(%40King%20Jin%20Corporation%20Pte%20Ltd)!5e0!3m2!1sen!2snl!4v1655657998063!5m2!1sen!2snl"
                    width="100%"
                    height="187"
                    style={{ border: "none", marginTop: 5 }}
                  ></iframe>
                </div>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={6}>
                <div className="footer-address">
                  <h5>Address</h5>
                  <p>
                    101 Upper Cross Street, People’s Park Center
                    <br /> Level B1-02
                    <br />
                    Singapore 058357
                  </p>
                  <h5>Operating Hours</h5>
                  <p>
                    Monday - Sunday: 1100 - 2030hrs
                    <br />
                    Public Holidays: Closed
                  </p>
                </div>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={6}>
                <div className="useful-links">
                  <h5>Useful Links</h5>
                  <div>
                    <a href="/korea-travel">Korea Travel Deals</a>
                    <br />
                    <a href="/japan-travel">Japan Travel Deals</a>
                    <br />
                    <a
                      onClick={() => {
                        setShowForm(true);
                        setInitialValue({ destination: undefined });
                      }}
                    >
                      Get Quote For a Customised Trip
                    </a>
                    <br />
                    <a href="/about">About Us</a>
                    <br />
                    <a href="/documents/terms">Booking T&Cs</a>
                    <br />
                    <a href="/documents/privacy-policy">Privacy Policy</a>
                    <br />
                    <a href="/contact">Contact Us</a>
                    <br />
                  </div>
                </div>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={6}>
                <div className="connect-with-us">
                  <h5>Connect With Us</h5>
                  <div style={{ paddingTop: 5 }}>
                    <a
                      href="https://www.facebook.com/KingTravelSingapore/"
                      target="_blank"
                    >
                      <img
                        src="/img/facebook.png"
                        alt="facebook"
                        style={{ marginRight: 8 }}
                      />
                    </a>
                    <a onClick={handleVisibleChange}>
                      <img src="/img/wechat.png" alt="wechat" />
                    </a>
                  </div>
                </div>
                <div className="footer-payment-options">
                  <h5 style={{ paddingTop: 20 }}>Payment Supported</h5>
                  <img
                    src="/img/payments.png"
                    alt="payments"
                    style={{ paddingTop: 5 }}
                  />
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>

      <div className="footer-bottom">
        <div className="wrap">
          <p>
            © Copyright 2022. King Travel (@King Jin Corporation Pte Ltd). All
            Rights Reserved. STB License:{" "}
            <a
              href="https://trust.stb.gov.sg/site/content/tagaem/landing-page/travel-agent/travel-agent-details.html?licenceNo=03352"
              target="_blank"
            >
              <span>TA03352</span>
            </a>
          </p>
        </div>
      </div>
      <Modal
        title={
          <h6
            style={{
              fontWeight: "bold",
              fontSize: "16px",
              fontFamily: "Inter-Bold",
              marginBottom: "0px",
            }}
          >
            Kingjin Wechat Qr Code
          </h6>
        }
        visible={visible}
        onCancel={handleVisibleChange}
        footer={null}
      >
        <div>
          <img src="/img/king-travel-wechat-qr.jpg" width="100%" />
        </div>
      </Modal>
    </>
  );
}
