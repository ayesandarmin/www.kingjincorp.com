const SingleBanner = ({ content }) => {
  return (
    <div id="single-banner">
      <div style={{
        backgroundImage: `url("${content.photo.regular}")`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        height: 558
      }}>
        <div className="single-banner-title">
          <h1>{content.title}</h1>
        </div>
      </div>
    </div>
  )
}
export default SingleBanner