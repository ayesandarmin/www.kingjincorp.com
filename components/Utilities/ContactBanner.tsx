const ContactBanner = ({ content }) => {
    return (
      <div id="contact-banner">
        <div style={{
          backgroundImage: `url("${content.photo.regular}")`,
          backgroundPosition: 'center',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          height: 700,
        //   background: 'transparent'
        }}>
          <div className="contact-banner-content">
            <div className="contact-banner-title" dangerouslySetInnerHTML={{ __html: content.title }} />
            <div className="contact-banner-description">{content.description}</div>
          </div>
          <img src="/img/map-point.svg" className="map-point" />
          <img src="/img/map-line.svg"  className="map-line" />
        </div>
      </div>
    )
  }
  export default ContactBanner