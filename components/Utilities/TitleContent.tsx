import { Col, Row } from 'antd';
import React from 'react'

const TitleContent = ({
  title,
  description,
  className,
  position = "center",
}: {
  title: string;
  description: string;
  className?: string;
  position?: "center" | "space-around" | "space-between" | "end" | "start";
}) => {
  return (
    <article className={`title-content ${className || ""}`}>
      <Row type="flex" justify={position}>
        <Col>
          <h3>{title || "Title"}</h3>
        </Col>
        <Col>
          <p>{description || "Description"}</p>
        </Col>
      </Row>
    </article>
  );
};

export default TitleContent;