import { Carousel, Icon } from "antd";
import { CarouselArrow } from "./carousel-arrow";

const InspirationGallery = ({ content }) => {

  const settings = {
    autoplay: false,
    infinite: false,
    slidesToShow: 1,
    speed: 500,
    rows: 2,
    slidesPerRow: 4,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          rows: 2,
          slidesToShow: 1,
          slidesPerRow: 2,
          infinite: false,
        },
      },
      {
        breakpoint: 576,
        settings: {
          rows: 1,
          slidesToShow: 1,
          slidesPerRow: 1,
          infinite: false,
          arrows: true,
          dots: false,
          prevArrow: (
            <CarouselArrow
              iconElement={
                <div className="carousel-arrow">
                  <Icon
                    style={{ fontSize: "40px" }}
                    type="left-circle"
                    theme="filled"
                  />
                </div>
              }
            />
          ),
          nextArrow: (
            <CarouselArrow
              iconElement={
                <div className="carousel-arrow">
                  <Icon
                    style={{ fontSize: "40px" }}
                    type="right-circle"
                    theme="filled"
                  />
                </div>
              }
            />
          ),
        },
      },
    ],
  };

  return (
    <div style={{ position: "relative" }}>
      <Carousel {...settings}>
        {content.map((img, index) => (
          <div key={`all-${img.name}-img-${index}`}>
            <div
              className="inspiration-img"
              style={{
                backgroundImage: `url(${img.photo?.small})`,
                width: "100%",
                backgroundRepeat: "no-repeat",
                height: 260,
              }}
            >
              {img.title ? (
                <div
                className="inspiration-img-title"
                >
                  <h6>{img.title}</h6>
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        ))}
      </Carousel>
    </div>
  );
};
export default InspirationGallery;
