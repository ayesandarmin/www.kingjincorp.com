import React from "react";
import { Form, Menu, Select } from "antd";

export const CategoryFilters = ({
  categories = [],
  selected = null,
  onSelect,
  windowSize,
  showAll = false,
}) => {
  return windowSize.width < 992 ? (
    <Form.Item className="category-filters">
      <Select
        dropdownClassName="category-filters"
        value={selected}
        onChange={(value) => onSelect(value)}
      >
        {categories.map((category: any, index) => (
          <Select.Option key={`cat-index=${index}`} value={category.name}>
            {category.description}
          </Select.Option>
        ))}
        {showAll && (
          <Select.Option key="all" value="all">
            ALL TOURS
          </Select.Option>
        )}
      </Select>
    </Form.Item>
  ) : (
    <Menu
      mode="horizontal"
      className="category-filters"
      selectedKeys={[selected]}
      onSelect={(e) => onSelect(e.key)}
    >
      {categories.map((category: any) => (
        <Menu.Item key={category.name}>{category.description}</Menu.Item>
      ))}
      {showAll && <Menu.Item key="all">ALL TOURS</Menu.Item>}
    </Menu>
  );
};
