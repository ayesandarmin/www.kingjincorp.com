import React from "react";
import config from "../../customize/config";
import {
  Form,
  Input,
  Row,
  Col,
  Button,
  Modal,
  Checkbox,
  Select,
  Icon,
} from "antd";
import { WrappedFormUtils } from "antd/lib/form/Form";
import { TravelCloudClient } from "travelcloud-antd";

export const GeneralForm = Form.create<{
  form: WrappedFormUtils;
  wrappedComponentRef;
  rowGutter;
}>()(
  class extends React.Component<{ form: WrappedFormUtils; rowGutter }> {
    // state = {
    //   phone_code: 65,
    //   phone_number: null,
    // };

    // onChange = (e, field) => {
    //   const { value } = e.target;
    //   const reg = /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/;
    //   if ((!isNaN(value) && reg.test(value)) || value === "" || value === "-") {
    //     if (field == "phone_code") {
    //       if (value.length <= 3) {
    //         this.props.form.setFieldsValue({
    //           phone: `+${value} ${this.state.phone_number}`,
    //         });
    //         this.setState({ phone_code: value });
    //       }
    //     } else if (field == "phone_number") {
    //       this.props.form.setFieldsValue({
    //         phone: `+${this.state.phone_code} ${value}`,
    //       });
    //       this.setState({ phone_number: value });
    //     }
    //   }
    // };

    render() {
      const { form, rowGutter } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Form>
          <Row type="flex" gutter={rowGutter}>
            <Col xs={24} md={24}>
              <label>
                <strong>Name</strong>
              </label>
              <Form.Item className="border-right-adjustment">
                {getFieldDecorator("name", {
                  rules: [
                    {
                      required: true,
                      message: "Please enter your first name.",
                      whitespace: true,
                    },
                  ],
                })(<Input placeholder="Your Name" />)}
              </Form.Item>
            </Col>
            {getFieldDecorator("subject", {
              initialValue: "Contact Form",
            })(<Input hidden />)}
            <Col xs={24} md={24}>
              <label>
                <strong>Email</strong>
              </label>
              <Form.Item className="border-right-adjustment">
                {getFieldDecorator("email", {
                  rules: [
                    { type: "email", message: "Email is not valid." },
                    { required: true, message: "Please enter your email." },
                  ],
                })(<Input placeholder="youremail@mailserver.com" />)}
              </Form.Item>
            </Col>
            <Col xs={24} md={24}>
              <label>
                <strong>Contact No.</strong>
              </label>
              <Form.Item className="border-right-adjustment">
                {getFieldDecorator("phone", {
                  rules: [
                    {
                      required: true,
                      pattern: new RegExp("^[0-9 ]*$"),
                      message: "Please enter only numbers",
                    },
                  ],
                })(<Input placeholder="phone" />)}
              </Form.Item>
              {/* <Form.Item className="border-right-adjustment">
                {getFieldDecorator("phone", {
                  rules: [
                    {
                      required: true,
                      message: "Please enter your contact number.",
                    },
                  ],
                })(<Input type="hidden" />)}
                <Input.Group compact>
                  <Input
                    placeholder="Phone"
                    onChange={(x) => this.onChange(x, "phone_number")}
                    value={this.state.phone_number}
                  />
                </Input.Group>
              </Form.Item> */}
            </Col>
          </Row>
          <Row type="flex" gutter={rowGutter}>
            <Col xs={24}>
              <label>
                <strong>Message</strong>
              </label>
              <Form.Item>
                {getFieldDecorator("message", {
                  rules: [
                    {
                      required: true,
                      message: "Please enter your message.",
                      whitespace: true,
                    },
                  ],
                })(
                  <Input.TextArea
                    autoSize={{ minRows: 6, maxRows: 8 }}
                    placeholder="Your Message to us"
                  />
                )}
              </Form.Item>
            </Col>
            <Col xs={24} md={24} lg={24}>
              <div style={{ marginTop: -15 }}>
                <Form.Item className="agree-checkout">
                  {getFieldDecorator("agree-terms", {
                    rules: [
                      {
                        required: true,
                        message: "",
                      },
                    ],
                  })(
                    <Checkbox value="agree">
                      <strong>
                        By submitting the form, I agree to King Travel’s{" "}
                        <a
                          href="/documents/privacy-policy"
                          target="_blank"
                          className="check-policy"
                        >
                          Privacy Policy
                        </a>
                      </strong>
                    </Checkbox>
                  )}
                </Form.Item>
              </div>
            </Col>
          </Row>
        </Form>
      );
    }
  }
);

export class ContactFormComponent extends React.Component<any, any> {
  formRef;
  client: TravelCloudClient = new TravelCloudClient(config);
  state = {
    agreeTerms: false,
    formState: "",
  };

  agreeTerms = (e) => {
    this.setState({ agreeTerms: e.target.checked });
  };

  onSubmit = () => {
    this.formRef.props.form.validateFieldsAndScroll(async (error, values) => {
      if (!error) {
        this.setState({ formState: "submitting" });
        const result = await this.client.submitContactForm(
          {
            referral_site: config.domain,
            subject: this.props.subject,
            customer_name: values.name,
            customer_email: values.email,
          },
          values
        );
        if (result.result == null) {
          this.setState({ formState: "error" });
        } else {
          this.setState({ formState: "submitted" });
          if (this.props.closeModal) {
            setTimeout(function () {
              this.props.closeModal;
            }, 4000);
          }
        }
      } else {
        //console.log("error", error, values)
      }
    });
  };

  render() {
    let heading: any = this.props.subject || "";
    let contactForm = (
      <GeneralForm
        wrappedComponentRef={(formRef) => {
          this.formRef = formRef;
        }}
        rowGutter={this.props.rowGutter || 0}
      />
    );

    let formState: any = {
      heading: heading,
      body: (
        <div>
          {contactForm}
          <Button
            className="kingtravel-contact-btn"
            size="large"
            type="primary"
            //disabled={this.state.agreeTerms!==true || (this.state.formState !== "" && this.state.formState !== "error")}
            disabled={
              this.state.formState !== "" && this.state.formState !== "error"
            }
            onClick={this.onSubmit}
          >
            Submit Message
          </Button>
        </div>
      ),
    };

    if (this.state.formState === "error") {
      formState = {
        heading: "Error",
        body: (
          <p>
            There was a problem submitting the form. Please try again later.
          </p>
        ),
      };
    }

    if (this.state.formState === "submitted") {
      formState = {
        heading: "Thank you",
        body: (
          <p>
            We have received your enquiry. Our travel consultant will get in
            touch with you (via email or phone) within 3 working days.
          </p>
        ),
      };
    }

    return this.props.useModal ? (
      <Modal
        title={formState.heading}
        footer={null}
        visible={this.props.visible}
        onCancel={this.props.closeModal}
      >
        {formState.body}
      </Modal>
    ) : (
      <div>
        <h4 className="heading">{formState.heading}</h4>
        {formState.body}
      </div>
    );
  }
}

export const QuotationForm = Form.create<{
  form: WrappedFormUtils;
  wrappedComponentRef;
  rowGutter;
  initialValue;
}>()(
  class extends React.Component<{
    form: WrappedFormUtils;
    rowGutter;
    initialValue;
  }> {
    // state = {
    //   phone_code: 65,
    //   phone_number: null,
    // };

    // onChange = (e, field) => {
    //   const { value } = e.target;
    //   const reg = /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/;
    //   if ((!isNaN(value) && reg.test(value)) || value === "" || value === "-") {
    //     if (field == "phone_code") {
    //       if (value.length <= 3) {
    //         this.props.form.setFieldsValue({
    //           phone: `+${value} ${this.state.phone_number}`,
    //         });
    //         this.setState({ phone_code: value });
    //       }
    //     } else if (field == "phone_number") {
    //       this.props.form.setFieldsValue({
    //         phone: `+${this.state.phone_code} ${value}`,
    //       });
    //       this.setState({ phone_number: value });
    //     }
    //   }
    // };

    render() {
      const { form, rowGutter, initialValue } = this.props;
      const { getFieldDecorator } = form;

      return (
        <Form>
          <Row>
            <Col className="center">
              <h3 className="quotation-title">Request For A Quote</h3>
              <p className="quotation-description">
                Please fill up the form below and one of our tour
                representatives will get back to you soon!
              </p>
            </Col>
          </Row>
          <Row type="flex" gutter={rowGutter} className="quotation-form">
            <Col xs={24} md={24}>
              <label>
                <strong>Name</strong>
              </label>
              <Form.Item className="border-right-adjustment">
                {getFieldDecorator("name", {
                  rules: [
                    {
                      required: true,
                      message: "Please enter your first name.",
                      whitespace: true,
                    },
                  ],
                })(<Input placeholder="Your Name" />)}
              </Form.Item>
            </Col>
            {getFieldDecorator("subject", {
              initialValue: "Quotation Form",
            })(<Input hidden />)}
            <Col xs={24} md={24}>
              <label>
                <strong>Email</strong>
              </label>
              <Form.Item className="border-right-adjustment">
                {getFieldDecorator("email", {
                  rules: [
                    { type: "email", message: "Email is not valid." },
                    { required: true, message: "Please enter your email." },
                  ],
                })(<Input placeholder="youremail@mailserver.com" />)}
              </Form.Item>
            </Col>
            <Col xs={24} md={24} lg={24}>
              <label>Destination</label>
              <Form.Item>
                {getFieldDecorator("Destination", {
                  initialValue: initialValue?.destination || undefined,
                  rules: [
                    {
                      required: true,
                      message: "Please select destination",
                    },
                  ],
                })(
                  <Select
                    placeholder="Select Destination"
                    suffixIcon={<Icon type="caret-down" />}
                  >
                    <Select.Option value="korea-tour">Korea Tour</Select.Option>
                    <Select.Option value="japan-tour">Japan Tour</Select.Option>
                    <Select.Option value="other-destinations">
                      Other Destinations
                    </Select.Option>
                    <Select.Option value="flight-tickets">
                      Flight Tickets
                    </Select.Option>
                    <Select.Option value="accommodations">
                      Accommodations
                    </Select.Option>
                    <Select.Option value="others">Others</Select.Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col xs={24} md={24}>
              <label>
                <strong>Contact Number</strong>
              </label>
              <Form.Item className="border-right-adjustment">
                {getFieldDecorator("phone", {
                  rules: [
                    {
                      required: true,
                      pattern: new RegExp("^[0-9 ]*$"),
                      message: "Please enter only numbers",
                    },
                  ],
                })(<Input placeholder="phone" />)}
              </Form.Item>
            </Col>
            <Col xs={24}>
              <label>
                <strong>How can we help you?</strong>
              </label>
              <Form.Item>
                {getFieldDecorator("message", {
                  rules: [
                    {
                      required: true,
                      message:
                        "Please provide the details for your desires such as travel period, no. of travelers, preferred activities, types of accommodations etc",
                      whitespace: true,
                    },
                  ],
                })(
                  <Input.TextArea
                    style={{ paddingTop: 10 }}
                    autoSize={{ minRows: 6, maxRows: 8 }}
                    placeholder="Please provide the details for your desires such as travel period, no. of travelers, preferred activities, types of accommodations etc"
                  />
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      );
    }
  }
);

export class QuotationFormComponent extends React.Component<any, any> {
  formRef;
  client: TravelCloudClient = new TravelCloudClient(config);
  state = {
    agreeTerms: false,
    formState: "",
  };

  agreeTerms = (e) => {
    this.setState({ agreeTerms: e.target.checked });
  };

  onSubmit = () => {
    this.formRef.props.form.validateFieldsAndScroll(async (error, values) => {
      if (!error) {
        this.setState({ formState: "submitting" });
        const result = await this.client.emailAdmins(
          {
            referral_site: config.domain,
            subject: this.props.subject,
            customer_name: values.name,
            customer_email: values.email,
            admin_emails: ["admin@kingjincorp.com"]
          },
          values
        );
        if (result.result == null) {
          this.setState({ formState: "error" });
        } else {
          this.setState({ formState: "submitted" });
          if (this.props.closeModal) {
            setTimeout(() => {
              this.props.closeModal();
            }, 4000);
          }
        }
      } else {
        //console.log("error", error, values)
      }
    });
  };

  render() {
    let heading: any = this.props.subject || "";
    let quotationForm = (
      <QuotationForm
        wrappedComponentRef={(formRef) => {
          this.formRef = formRef;
        }}
        rowGutter={this.props.rowGutter || 0}
        initialValue={this.props.initialValue}
      />
    );

    let formState: any = {
      heading: heading,
      body: (
        <div>
          {quotationForm}
          <Button
            className="kingtravel-quotation-btn"
            size="large"
            type="primary"
            //disabled={this.state.agreeTerms!==true || (this.state.formState !== "" && this.state.formState !== "error")}
            disabled={
              this.state.formState !== "" && this.state.formState !== "error"
            }
            onClick={this.onSubmit}
          >
            Submit Message
          </Button>
        </div>
      ),
    };

    if (this.state.formState === "error") {
      formState = {
        heading: "Error",
        body: (
          <p>
            There was a problem submitting the form. Please try again later.
          </p>
        ),
      };
    }

    if (this.state.formState === "submitted") {
      formState = {
        heading: "Thank you",
        body: (
          <p>
            We have received your enquiry. Our travel consultant will get in
            touch with you (via email or phone) within 3 working days.
          </p>
        ),
      };
    }

    return this.props.useModal ? (
      <Modal
        title={formState.heading}
        footer={null}
        visible={this.props.visible}
        onCancel={this.props.closeModal}
        width={900}
      >
        {formState.body}
      </Modal>
    ) : (
      <div>
        <h4 className="heading">{formState.heading}</h4>
        {formState.body}
      </div>
    );
  }
}
