import React, { Fragment } from 'react';
import { Card, Tooltip, Icon, Row, Col, Statistic, Tag } from 'antd';
import { RateClassInfo, prepareCancellationPolicy } from 'travelcloud-antd/components/hooks/hotelbeds';
import { FaBed, FaRegCalendarAlt, FaCloudMoon } from 'react-icons/fa';
import moment from 'moment';
import { Facility } from './hotelbeds-results';

const HotelDetailCard = ({ room, rates, hotel, checkIn, checkOut, facilities, controller, nights }) => {
  const formatDate = (dateStr: string) => moment(dateStr).format("DD MMM YYYY");
  return (
    <Card className="info-box"
      bordered={false}
      title={
        <>
          {`${room.name} (${rates[0].boardName})`}
          <sup>
            <Tooltip
              placement="rightTop"
              title={RateClassInfo[rates[0]["rateClass"]]}
            >
              <Icon type="info-circle" />
            </Tooltip>
          </sup>
        </>
      }
      extra={rates.map(
        (rate, index) =>
          prepareCancellationPolicy(rate, hotel.currency) == null && (
            <Tag color="cyan" key={`rate_${index}`}> Free Cancellation</Tag>
          )
      )}
    >
      {rates.map((rate: any, index: number) => (
        <Row
          type="flex"
          align="top"
          justify="space-between"
          className="center-info"
          key={`d-info${index}`}
        >
          <Col xs={24} sm={24} md={8} lg={8}>
            <h6>
              <span>
                <FaBed />
                    OCCUPANT
                  </span>
            </h6>
            <Row type="flex" justify="center">
              <Col xs={4} sm={5} md={5} lg={6} className="room">
                <Statistic
                  title=""
                  value={rate.rooms}
                  suffix={rate.rooms > 1 ? " rooms" : " room"}
                />
              </Col>
              <Col xs={4} sm={5} md={5} lg={6}>
                <Statistic
                  title=""
                  value={rate.adults}
                  suffix={rate.adults > 1 ? " adults" : " adult"}
                />
              </Col>
              <Col xs={4} sm={5} md={5} lg={6}>
                <Statistic
                  title=""
                  value={rate.children}
                  suffix={rate.children > 1 ? " children" : " child"}
                />
              </Col>
            </Row>
          </Col>
          <Col xs={24} sm={24} md={8} lg={8} className="dates">
            <h6>
              <span>
                <FaRegCalendarAlt />
                    FROM
                  </span>
            </h6>

            <p>
              <b>{formatDate(checkIn)}</b> to <b>{formatDate(checkOut)}</b>
            </p>
          </Col>
          <Col xs={24} sm={24} md={8} lg={8} className="price">
            <h6>
              <span>
                <FaCloudMoon />
                {nights} {nights > 1 ? "NIGHTS" : "NIGHT"} FOR
              </span>
            </h6>
            {rate._view.hasDiscount && (
              <s>
                {hotel.currency}
                {rate._view.beforeDiscount.toString()}
              </s>
            )}
            <b>
              {hotel.currency}
              {rate._view.afterDiscount.toString()}
            </b>
          </Col>
        </Row>
      ))}
      {facilities != null &&
        facilities.map((group, index) => (
          <div className="details" key={`facility_${index}`}>
            <h4>{group.description.content}</h4>
            <ul key={`dfacility-index`}>
              {group.facilities.map((facility, index) => (
                <>
                  <Facility
                    key={`d${index}`}
                    facility={facility}
                    checkIn={checkIn}
                    checkout={checkOut}
                  />
                </>
              ))}
            </ul>
          </div>
        ))}
      {rates.map((rate: any, index: number) => (
        <Fragment key={`rate_offer_${index}`}>
          {rate.offers != null && rate.offers.length > 0 ? (
            <>
              <Tag color="magenta">Discount</Tag>
              <ul>
                {rate.offers.map((p, index) => (
                  <li key={`d-off` + index}>
                    {p.name} {hotel.currency} {p.amount}
                  </li>
                ))}
              </ul>
            </>
          ) : null}

          {rate.rateClass == "NRF" ? (
            <Tag color="red">Non-refundable</Tag>
          ) : (
              prepareCancellationPolicy(rate, hotel.currency) !== null && (
                <Tag color="orange">Cancellation Policy</Tag>
              )
            )}
          {prepareCancellationPolicy(rate, hotel.currency) !== null && (
            <>
              <ul>
                {prepareCancellationPolicy(rate, hotel.currency).map(
                  (policy, n) => (
                    <li key={`d-policy${n}`}>{policy}</li>
                  )
                )}
              </ul>
            </>
          )}
          {rate.promotions != null && rate.promotions.length > 0 ? (
            <>
              <ul>
                {rate.promotions.map((p, idx) => (
                  <li key={`d-promo` + idx}>{p.name}</li>
                ))}
              </ul>
            </>
          ) : null}

          {parseInt(rate.children) > 0 && (
            <p>Children Ages: {rate.childrenAges}</p>
          )}
          {rate.rateCommentsId != null && (
            <React.Fragment>
              <b>Remarks</b>
              {controller
                .getRateCommentsById(rate.rateCommentsId, checkIn)
                .map((mesg: string, index: number) => (
                  <pre key={`dremark-${index}`}>{mesg}</pre>
                ))}
            </React.Fragment>
          )}
        </Fragment>
      ))}
    </Card>
  )
}

export default HotelDetailCard;
