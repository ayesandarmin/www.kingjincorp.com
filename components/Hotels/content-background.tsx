import React from 'react';


const ContentBackground = ({ children }: any) => {
  return (
    <div style={{ background: '#fff' }}>
      {children}
    </div>
  )
}

export default ContentBackground;