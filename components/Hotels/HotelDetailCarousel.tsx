import React from 'react';
import { Carousel } from 'antd';
import CarouselArrow from 'travelcloud-antd/components/carousel-arrow';

const HotelDetailCarousel = ({ images }) => {
  const photoslides = {
    arrows: false,
    prevArrow: (
      <CarouselArrow
        classname="slick-arrow slick-prev"
        theme="filled"
        iconType="left-circle"
      />
    ),
    nextArrow: (
      <CarouselArrow
        classname="slick-arrow slick-next"
        theme="filled"
        iconType="right-circle"
      />
    ),
    dots: true,
    autoplay: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 1380,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 779,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <Carousel className="hotel-photos" {...photoslides}>
      {images.map((photo, key) => (
        <div key={`dimage-${key}`}>
          <div style={{ position: "relative" }}>
            <img src={photo.url} height={300} />
            {photo.description != null && (
              <div
                style={{
                  position: "absolute",
                  bottom: 0,
                  left: 0,
                  right: 0,
                  padding: 10,
                  textAlign: "center",
                }}
              >
                {photo.description.content}
              </div>
            )}
          </div>
        </div>
      ))}
    </Carousel>
  )
}

export default HotelDetailCarousel;