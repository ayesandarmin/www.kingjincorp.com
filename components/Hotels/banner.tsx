import { CSSProperties } from "react"

export function Banner({
  backgroundImageUrl = "" as string,
  style = {} as CSSProperties,
  title = "" as string,
  subTitle = "" as string
}) {
  if (backgroundImageUrl) style.backgroundImage = `url(${backgroundImageUrl})`
  return (
    <div className="banner" style={style}>
      <h4 className="font-primary">{subTitle}</h4>
      <h2 className="font-primary">{title}</h2>
    </div>
  )
}