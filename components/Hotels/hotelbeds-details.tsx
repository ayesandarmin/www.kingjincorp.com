
import React from 'react';
import HotelDetailCarousel from './HotelDetailCarousel'
import HotelDetailCard from './HotelDetailCard'
import HotelbedsDetail from 'travelcloud-antd/components/hotelbeds-details'
import { Row, Col } from 'antd'
import { FaMapMarkerAlt, FaStar, FaStarHalf, FaBuilding } from 'react-icons/fa'
import { getHotelResultUtilities } from 'travelcloud-antd/components/hooks/hotelbeds'

const CustomizeHotelbedsDetail = props => {
  const { findStars, countNights } = getHotelResultUtilities
  const nights = countNights(props.checkIn, props.checkOut);
  return (
    <HotelbedsDetail
      {...props}
    >
      {
        (params) => {
          const { hotel, room, rates, facilities, images, imageUrlPrefix, checkIn, checkOut, controller } = params
          return (
            <>
              <h1>
                {hotel.name}{" "}
                {[...Array(findStars(hotel.categoryName))].map((_i, index) => (
                  <FaStar key={`detail-star${hotel.code + index}`} />
                ))}
                {hotel.categoryName.includes("HALF") && <FaStarHalf />}
              </h1>
              <h5 className="address">
                <Row type="flex" align="middle">
                  <Col xs={2} sm={2} lg={1} className="location">
                    <FaMapMarkerAlt />
                  </Col>
                  <Col xs={22} sm={22} lg={23}>
                    {hotel.details.address.content}, {hotel.details.city.content}{" "}
                    {hotel.details.postalCode}
                  </Col>
                </Row>
              </h5>
              {!!images && images.length > 0 ? (
                <HotelDetailCarousel images={images} />
              ) : (!!hotel.details && !!hotel.details.images && hotel.details.images[0]) ? (
                <div className="single-pic">
                  <img src={imageUrlPrefix + hotel.details.images[0].path} />
                </div>
              ) : <div className="placeholder-image">
                    <FaBuilding />
                  </div>
              }
              <HotelDetailCard
                room={room}
                hotel={hotel}
                rates={rates}
                checkIn={checkIn}
                checkOut={checkOut}
                nights={nights}
                facilities={facilities}
                controller={controller}
              />
            </>
          )
        }
      }
    </HotelbedsDetail>
  )
}

export default CustomizeHotelbedsDetail;
