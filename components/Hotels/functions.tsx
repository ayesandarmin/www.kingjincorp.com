import Link from "next/link"
import config from "../../customize/config"

export function useOriginalImage(url:string) {
  if (!url) return null
  return url.slice(0, -4) + "_o.jpg"
}

export function useOptimisedImage(url:string) {
  if (!url) return null
  return url.slice(0, -4) + "_z.jpg"
}

export function convertToUrl(url:string) {
  if (!url) return null
  const alpanumericHyphened = url
    .toLowerCase()                    // convert to lowercase
    .replace(/[^a-zA-Z0-9]/g,' ')     // replace non alpanumerics with spaces
    .trim()                           // remove trailing spaces
    .replace(/( )\1{1,}/g,' ')        // reduce consecutive spaces
    .replace(/ /g,'-')                // replace spaces with hyphens
  return (alpanumericHyphened || "-") // use hyphen in case of ""
}

export function splitString(fullString) {
  let values:any = {
    mainText: fullString.trim(),
    subText: ""
  }
  const subText = values.mainText.match(/\[(.*?)\]/g)
  if (subText) {
    values.mainText = values.mainText.replace(subText[0], '').trim()
    values.subText = subText[0].replace('[', '').replace(']', '').trim()
  }
  return [ values.mainText, values.subText ]
}

export function LinkElement({
  url = "" as string, 
  children = "" as any
}) {
  if (url.indexOf('http') > -1) {
    return <a href={url} target="_blank">{children}</a>
  } else {
    return <Link href={url}><a>{children}</a></Link>
  }
}

export function priceDisplay({ 
  value = 0 as any,
  locale = config.locale as string,
  currency = "USD" as string,
  showZeroCents = true as boolean
}) {
  if (!value) return

  const floatValue = parseFloat(value)
  const neg = floatValue < 0 ? "-" : ""
  var formatter = new Intl.NumberFormat(locale, {
    style: 'currency',
    currency: currency.replace(/\s/g, '')
  })

  const priceString = neg + formatter.format(floatValue)

  if (showZeroCents) {
    return priceString
  } else {
    return priceString.replace(".00", "")
  }
}