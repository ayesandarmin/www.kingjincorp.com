import React, {  useState, useEffect, useRef } from 'react'
import { Row, Col, Card, Radio, Form, Input } from 'antd'
import { groupBy } from 'travelcloud-antd/travelcloud'
import { HotelbedsSelectedRateInfo } from 'travelcloud-antd/components/hotelbeds-results'
import { useHotelbedsPassengerHook } from 'travelcloud-antd/components/hooks/hotelbeds-passenger-form';
const { Item } = Form

// fixme: provide types
const PassengerTypeSelector = ({ onChange, value }) => {
  return (
    <Radio.Group onChange={onChange}
      value={value}
      style={{ marginBottom: '20px' }}>
      <Radio value={true}>Enter the main occupant only</Radio>
      <Radio value={false}>Enter the details for all guests</Radio>
    </Radio.Group>
  )
}

// fixme: any return type ?
const PassengerForm = (
  {
    type,
    onChange,
    roomIndex,
    paxIndex,
    value
  }: {
    type?: string,
    onChange: (value: any) => void,
    roomIndex?: number,
    paxIndex?: number,
    value: string[]
  }) => {

  const [name, setName] = useState(value[0])
  const [surname, setSurname] = useState(value[1])

  const firstRender = useRef(true)
  useEffect(() => {

    // skip on first render
    if (firstRender.current) {
      firstRender.current = false;
      return
    }

    onChange({
      name: [name, surname],
      paxIndex,
      roomIndex
    })

  }, [name, surname])

  return (
    <Row>
      <Col xs={24} sm={24} md={24} lg={10}>
        <Item>
          <Input onChange={e => setName(e.target.value)}
            value={name}
            placeholder={type == 'AD' ? 'Name - Adult' : 'Name - Child'} />
        </Item>
      </Col>
      <Col xs={24} sm={24} md={24} lg={10}>
        <Item>
          <Input onChange={e => setSurname(e.target.value)}
            value={surname}
            placeholder={type == 'AD' ? 'Surname - Adult' : 'Name - Child'} />
        </Item>
      </Col>
    </Row>
  )
}


const HotelbedsPassenger = (
  {
    onUpdate,
    isFormValidated,
    item,
  }: {
    title?: string,
    children?: any,
    onUpdate: (any) => void,
    isFormValidated: (any) => void,
    item: HotelbedsSelectedRateInfo,
    rates?: any
  }): any => {

  const {
    isLeadPassengerOnly,
    rooms,
    onChange,
  } = useHotelbedsPassengerHook({ item });

  return (
    <Card bordered={false} title={isLeadPassengerOnly ? 'Details Of Main Occupant' : 'Details Of All Occupants'}>
      <PassengerTypeSelector
        onChange={(e) => { onChange({ isLeadPassengerOnly: e.target.value }, onUpdate, isFormValidated) }}
        value={isLeadPassengerOnly}
      />
      {isLeadPassengerOnly ? (
        <Row>
          <Col span={24}>
            <Form layout="inline">
              {rooms.map((room, idx) => (
                groupBy(room.paxes, x => x.roomId).map((group, gIdx) => (
                  <React.Fragment key={gIdx}>
                    <PassengerForm key={gIdx}
                      paxIndex={gIdx}
                      roomIndex={idx}
                      value={group[0].name}
                      type={group[0].type}
                      onChange={(value) => onChange(value, onUpdate, isFormValidated)} />
                  </React.Fragment>
                ))
              ))}
            </Form>
          </Col>
        </Row>
      ) : (
          <Row>
            <Col span={24}>
              <Form layout="inline">
                {rooms.map((room, idx) => (
                  groupBy(room.paxes, x => x.roomId).map((group, gIdx) => (
                    <React.Fragment key={gIdx}>
                      {group.map((pax, index) => (
                        <PassengerForm key={index}
                          paxIndex={index}
                          roomIndex={idx}
                          value={pax.name}
                          type={pax.type}
                          onChange={(value) => onChange(value, onUpdate, isFormValidated)} />
                      ))}
                    </React.Fragment>
                  ))
                ))}
              </Form>
            </Col>
          </Row>
        )}
    </Card>
  )
}

export default HotelbedsPassenger
