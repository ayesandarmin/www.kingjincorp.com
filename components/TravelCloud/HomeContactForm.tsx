import React, { Fragment } from "react";
import { Form, Input, Row, Col, Button } from "antd";
import { WrappedFormUtils } from "antd/lib/form/Form";
import FloatLabel from "../Utilities/FloatLabel";
import { TravelCloudClient } from "travelcloud-antd";
import config from "../../customize/config";

export const HomeContactForm = Form.create()(
  class extends React.Component<{ form: WrappedFormUtils; customer?}> {
    state = {
      emailSubject: "" as string
    };

    private client = new TravelCloudClient(config);

    getParameterByName(name, url) {
      if (!url) url = window.location.pathname;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return "";
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    componentDidMount() {
      const url = window.location.href;
      const sub = this.getParameterByName("sub", url);
      this.setState({
        emailSubject: sub
      });
    }

    onSubmit = () => {
      this.props.form.validateFieldsAndScroll(async (error, values) => {

        if (!error) {
          this.setState({
            formState: 'submitting'
          })

          const result = await this.client.submitContactForm({
            referral_site: `${config.domain}`,
            subject: 'Contact form',
            customer_name: values.name,
            customer_email: values.email
          }, values)

          if (result.result == null) {
            this.setState({
              formState: 'error'
            })
          } else {
            this.setState({
              formState: 'submitted',
              showForm: false
            })
          }
        } else {
          //console.log('error', error, values);
        }
      })
    }


    render() {
      const { form } = this.props;
      const { getFieldDecorator } = form;
      const name = form.getFieldValue("name");
      const email = form.getFieldValue("email");
      const company = form.getFieldValue("company");
      const message = form.getFieldValue("message");
      return (
        <Fragment>
          <Row
            className="text-left"
            gutter={20}
            type="flex"
            justify="space-between"
          >
            <Col md={24} sm={24} xs={24}>
              <Form.Item>
                <FloatLabel name="name" value={name}>
                  {getFieldDecorator("name", {
                    initialValue:
                      this.props.customer != null
                        ? this.props.customer.name
                        : "",
                    rules: [
                      {
                        required: true,
                        message: "Please enter your name",
                        whitespace: true
                      }
                    ]
                  })(<Input placeholder="Your Name" autoComplete="off" />)}
                </FloatLabel>
              </Form.Item>
            </Col>
            <Col md={24} sm={24} xs={24}>
              <Form.Item>
                <FloatLabel name="email" value={email}>
                  {getFieldDecorator("email", {
                    initialValue:
                      this.props.customer != null
                        ? this.props.customer.email
                        : "",
                    rules: [
                      {
                        type: "email",
                        message: "E-mail is not valid"
                      },
                      {
                        required: true,
                        message: "Please enter your E-mail"
                      }
                    ]
                  })(<Input placeholder="Your Email" autoComplete="off" />)}
                </FloatLabel>
              </Form.Item>
            </Col>
            <Col md={24} sm={24} xs={24}>
              <Form.Item>
                <FloatLabel name="company" value={company}>
                  {getFieldDecorator("company", {
                    initialValue:
                      this.props.customer != null
                        ? this.props.customer.company
                        : "",
                    rules: [
                      {
                        required: true,
                        message: "Please enter your company",
                        whitespace: true
                      }
                    ]
                  })(<Input placeholder="Your Company (Optional)" autoComplete="off" />)}
                </FloatLabel>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item>
                <FloatLabel name="message" value={message}>
                  {getFieldDecorator("message", {
                    rules: [
                      {
                        required: true,
                        message: "Please enter your message",
                        whitespace: true
                      }
                    ]
                  })(
                    <Input.TextArea
                      placeholder="Message"
                      autoSize={{ minRows: 4, maxRows: 6 }}
                      autoComplete="off"
                    />
                  )}
                </FloatLabel>
              </Form.Item>
            </Col>
            <Col span={24}>
              <Button className="home-contact-btn" onClick={this.onSubmit}>Submit Form</Button>
            </Col>
          </Row>

        </Fragment>
      );
    }
  }
);
