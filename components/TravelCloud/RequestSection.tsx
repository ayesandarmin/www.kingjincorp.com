import { Button, Col, DatePicker, Form, Input, Row } from "antd";
import React, { useContext } from "react";
import { getContent } from "web-component-antd";
import config from "../../customize/config";
import { splitNameAndUrl } from "../../helpers/common-function";
import { LayoutContext } from "../Layouts/WrappedLayout";


const RequestSection = ({ contactAddress, section }) => {
  const { layoutContent } = useContext(LayoutContext);
  const requestForm = getContent(layoutContent).tag("request-form").getValue();
  const sectionForm = requestForm.photos.find(
    (photo) => photo.title == section
  );

  return (
    <div id="request-section">
      <Row gutter={80}>
        <Col sm={24} md={10}>
          <div className="address-content">
            <h3>{contactAddress.title}</h3>
            <div className="responsive-contact-address">
              {contactAddress.photos.map((address, index) => {
                const [desc, link] = splitNameAndUrl(address.desc);
                return (
                  <div className="address" key={`address-${index}`}>
                    <Row type="flex" justify="start" gutter={20}>
                      <Col>
                        <div className="m-t-5">
                          <img src={address.photo.regular} />
                        </div>
                      </Col>
                      <Col>
                        <div className="address-desc">
                          <span>{address.title}</span>
                          <a href={link} target="_blank">
                            <div
                              className="address-link"
                              dangerouslySetInnerHTML={{ __html: desc }}
                            ></div>
                          </a>
                        </div>
                      </Col>
                    </Row>
                  </div>
                );
              })}
            </div>
          </div>
        </Col>
        <Col sm={24} md={14}>
          <WrappedRequestForm>
            {{ sectionForm }}
          </WrappedRequestForm>
        </Col>
      </Row>
    </div>
  );
};

const RequestForm = ({ form, children }) => {
  const { getFieldDecorator } = form;
  const { sectionForm } = children;
  const { client } = useContext(LayoutContext);

  const onSubmit = () => {
    form.validateFieldsAndScroll(async (error, values) => {
      if (!error) {
        const result = await client.submitContactForm(
          {
            referral_site: `${config.domain}`,
            subject: sectionForm.title,
            customer_name: values.name,
            customer_email: values.email,
          },
          values
        );
        console.log({result})
      } else {
        //console.log('error', error, values);
      }
    });
  };

  return (
    <Form>
      <div className="title">{sectionForm.desc}</div>
      <div>
        <Row
          className="text-left"
          gutter={20}
          type="flex"
          justify="space-between"
        >
          <Col md={12} sm={12} xs={12} style={{ paddingRight: 0 }}>
            <Form.Item>
              {getFieldDecorator("name", {
                initialValue: "John Doe",
                rules: [
                  {
                    required: true,
                    message: "Please enter your name",
                    whitespace: true,
                  },
                ],
              })(<Input autoComplete="off" placeholder="Your Name *" />)}
            </Form.Item>
          </Col>
          <Col md={12} sm={12} xs={12} style={{ paddingLeft: 0 }}>
            <Form.Item>
              {getFieldDecorator("email", {
                initialValue: "johndoe@gmail.com",
                rules: [
                  {
                    type: "email",
                    message: "E-mail is not valid",
                  },
                  {
                    required: true,
                    message: "Please enter your E-mail",
                  },
                ],
              })(<Input autoComplete="off" placeholder="Email *" />)}
            </Form.Item>
          </Col>
          <Col md={24} sm={24} xs={24}>
            {/* <Form.Item className="getntouch">
              {getFieldDecorator("subject", {
                rules: [
                  {
                    required: true,
                    message: "Please select Enquiry Type",
                    whitespace: true,
                  },
                ],
              })(
                <Select
                  placeholder="Where would you like to go? *"
                  size="large"
                  dropdownClassName="section-form"
                >
                  <Option key="Corporate Travel">Corporate Travel</Option>
                  <Option key="Digital Travel Solutions">
                    Digital Travel Solutions
                  </Option>
                  <Option key="M.I.C.E Services">M.I.C.E Services</Option>
                  <Option key="Others">Others</Option>
                </Select>
              )}
            </Form.Item> */}
            <Form.Item>
              {getFieldDecorator("subject", {
                rules: [
                  {
                    required: true,
                    message: "Please enter your desire",
                    whitespace: true,
                  },
                ],
              })(
                <Input
                  placeholder="Where would you like to go? *"
                  autoComplete="off"
                  style={{ display: "inline-block" }}
                />
              )}
            </Form.Item>
          </Col>
          <Col md={12} sm={12} xs={12} style={{ paddingRight: 0 }}>
            <Form.Item>
              {getFieldDecorator("start_date", {
                rules: [
                  {
                    type: "object",
                    required: true,
                    message: "Please enter start date",
                    whitespace: true,
                  },
                ],
              })(<DatePicker placeholder="Start travel date *" />)}
            </Form.Item>
          </Col>
          <Col md={12} sm={12} xs={12} style={{ paddingLeft: 0 }}>
            <Form.Item>
              {getFieldDecorator("end_date", {
                rules: [
                  {
                    type: "object",
                    required: true,
                    message: "Please enter end date",
                    whitespace: true,
                  },
                ],
              })(<DatePicker placeholder="End travel date *" />)}
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={24}>
            <Form.Item>
              {getFieldDecorator("message", {
                rules: [
                  {
                    // required: true,
                    message: "Please enter your feedback",
                    whitespace: true,
                  },
                ],
              })(
                <Input.TextArea
                  placeholder="Remarks (if any)"
                  autoSize={{ minRows: 4, maxRows: 6 }}
                  autoComplete="off"
                />
              )}
            </Form.Item>
          </Col>
          <Col span={24}>
            <Button className="kingtravel-contact-btn" onClick={onSubmit}>
              Submit Form
            </Button>
          </Col>
        </Row>
      </div>
    </Form>
  );
};

const WrappedRequestForm = Form.create({})(RequestForm);

export default RequestSection;
