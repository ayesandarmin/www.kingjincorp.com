import React, { Fragment } from "react";
import { Form, Input, Row, Col, Button, Icon } from "antd";
import { WrappedFormUtils } from "antd/lib/form/Form";
import FloatLabel from "../Utilities/FloatLabel";
import { TravelCloudClient } from "travelcloud-antd";
import config from "../../customize/config";
import { FixedAspectRatio } from "../Utilities/fixed-aspect-ratio";

const SendIcon = props => <Icon component={SendIconSVG} {...props} />;

const SendIconSVG = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 26.998 26.995">
    <path id="Icon_ionic-ios-send" data-name="Icon ionic-ios-send" d="M30.649,4.563,4.838,15.813a.592.592,0,0,0,.021,1.076l6.982,3.945a1.126,1.126,0,0,0,1.287-.127L26.895,8.838c.091-.077.309-.225.394-.141s-.049.3-.127.394L15.251,22.507a1.122,1.122,0,0,0-.113,1.343L19.7,31.17a.594.594,0,0,0,1.069-.014L31.444,5.344A.592.592,0,0,0,30.649,4.563Z" transform="translate(-4.503 -4.503)" fill="#fff"/>
  </svg>
);

type Props = {
  customer?: any; 
  contactInformation?: any;
  form: WrappedFormUtils;
};

export const ContactForm = Form.create<Props>()(
  class extends React.Component<Props> {
    state = {
      emailSubject: "" as string
    };

    private client = new TravelCloudClient(config);

    getParameterByName(name, url) {
      if (!url) url = window.location.pathname;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return "";
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    componentDidMount() {
      const url = window.location.href;
      const sub = this.getParameterByName("sub", url);
      this.setState({
        emailSubject: sub
      });
    }

    onSubmit = () => {
      this.props.form.validateFieldsAndScroll(async (error, values) => {
        if (!error) {
          this.setState({
            formState: 'submitting'
          })

          const result = await this.client.submitContactForm({
            referral_site: `${config.domain}`,
            subject: 'Contact form',
            customer_name: values.name,
            customer_email: values.email
          }, values)

          if (result.result == null) {
            this.setState({
              formState: 'error'
            })
          } else {
            this.setState({
              formState: 'submitted',
              showForm: false
            })
          }
        } else {
          //console.log('error', error, values);
        }
      })
    }


    render() {
      const { form } = this.props;
      const { getFieldDecorator } = form;
      const name = form.getFieldValue("name");
      const email = form.getFieldValue("email");
      // const phone = form.getFieldValue("phone");
      const message = form.getFieldValue("message");
      return (
        <Fragment>
          <div className="contact-form">
            <Row gutter={10} type="flex">
              <Col span={15}>
                <h1>{this.props.contactInformation.title}</h1>
                <Row
                  className="text-left"
                  gutter={20}
                  type="flex"
                  justify="space-between"
                >
                  <Col md={12} sm={12} xs={12}>
                    <Form.Item>
                      <FloatLabel name="name" value={name}>
                        {getFieldDecorator("name", {
                          initialValue:
                            this.props.customer != null
                              ? this.props.customer.name
                              : "",
                          rules: [
                            {
                              required: true,
                              message: "Please enter your name",
                              whitespace: true
                            }
                          ]
                        })(<Input autoComplete="off" placeholder="Name *" />)}
                      </FloatLabel>
                    </Form.Item>
                  </Col>
                  <Col md={12} sm={12} xs={12}>
                    <Form.Item>
                      <FloatLabel name="email" value={email}>
                        {getFieldDecorator("email", {
                          initialValue:
                            this.props.customer != null
                              ? this.props.customer.email
                              : "",
                          rules: [
                            {
                              type: "email",
                              message: "E-mail is not valid"
                            },
                            {
                              required: true,
                              message: "Please enter your E-mail"
                            }
                          ]
                        })(<Input autoComplete="off" placeholder="Email *" />)}
                      </FloatLabel>
                    </Form.Item>
                  </Col>
                </Row>
                <Row className="text-left">
                  <Col xs={24} sm={24} md={24} lg={24}>
                    <Form.Item>
                      <FloatLabel name="message" value={message}>
                        {getFieldDecorator("message", {
                          rules: [
                            {
                              required: true,
                              message: "Please enter your message",
                              whitespace: true
                            }
                          ]
                        })(
                          <Input.TextArea
                            placeholder="Your Message"
                            autoSize={{ minRows: 4, maxRows: 6 }}
                            autoComplete="off"
                          />
                        )}
                      </FloatLabel>
                    </Form.Item>
                  </Col>
                </Row>
                <span dangerouslySetInnerHTML={{ __html: this.props.contactInformation.content }} />
                <Button className="kingtravel-contact-btn" onClick={this.onSubmit}>Submit<SendIcon /></Button>
              </Col>
              <Col span={9}>
                <FixedAspectRatio
                  ratio="373:500"
                  imageUrl={this.props.contactInformation.photo.regular}
                />
              </Col>
            </Row>
          </div>
        </Fragment>
      );
    }
  }
);
