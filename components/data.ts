export const tours = [
  {
    id: 1,
    name: "7D6N Europe Holiday Package",
    short_desc: "Madrid + Barcelona + Valencia + Granada + Malaga + Seville + Lisbon",
    photo_url: "/img/tour-img1.png",
    price: "2688.00"
  },
  {
    id: 2,
    name: "7D6N Europe Holiday Package",
    short_desc: "Madrid + Barcelona + Valencia + Granada + Malaga + Seville + Lisbon",
    photo_url: "/img/tour-img2.png",
    price: "2000.00"
  },
  {
    id: 3,
    name: "7D6N Europe Holiday Package",
    short_desc: "Madrid + Barcelona + Valencia + Granada + Malaga + Seville + Lisbon",
    photo_url: "/img/tour-img3.png",
    price: "1000.00"
  },
  {
    id: 4,
    name: "7D6N Europe Holiday Package",
    short_desc: "Madrid + Barcelona + Valencia + Granada + Malaga + Seville + Lisbon",
    photo_url: "/img/tour-img4.png",
    price: "1500.00"
  },
  {
    id: 5,
    name: "7D6N Europe Holiday Package",
    short_desc: "Madrid + Barcelona + Valencia + Granada + Malaga + Seville + Lisbon",
    photo_url: "/img/tour-img4.png",
    price: "800.00"
  },
  {
    id: 6,
    name: "7D6N Europe Holiday Package",
    short_desc: "Madrid + Barcelona + Valencia + Granada + Malaga + Seville + Lisbon",
    photo_url: "/img/tour-img4.png",
    price: "1800.00"
  },
]
export const regions = {
  europe: {
    x: 532, y: 98
  },
  southEastAsia: {
    x: 869, y: 311
  },
  southAmerica: {
    x: 258, y: 386
  },
  southAfrica: {
    x: 599, y: 435
  },
  australia: {
    x: 990, y: 472
  },
  northAsia: {
    x: 793 , y: 54
  },
  northIndia: {
    x: 789, y: 213
  },
  bhutan: {
    x: 843, y: 240
  },
  maldives: {
    x: 762, y: 318
  }
}

export const MAP = {
  name: 'World-Map',
  areas: [
    { name: 'south-east-asia', shape: 'circle', coords: [regions.southEastAsia.x, regions.southEastAsia.y, 15], href: '#south-east-asia' },
    { name: 'south-america', shape: 'circle', coords: [regions.southAmerica.x, regions.southAmerica.y, 15], href: '#south-america' },
    { name: 'europe', shape: 'circle', coords: [regions.europe.x, regions.europe.y, 15], href: '#europe' },
    { name: 'south-africa', shape: 'circle', coords: [regions.southAfrica.x, regions.southAfrica.y, 15], href: '#south-africa' },
    { name: 'australia', shape: 'circle', coords: [regions.australia.x, regions.australia.y, 15], href: '#australia' },
    { name: 'north-asia', shape: 'circle', coords: [regions.northAsia.x, regions.northAsia.y, 15], href: '#north-asia' },
    { name: 'north-india', shape: 'circle', coords: [regions.northIndia.x, regions.northIndia.y, 15], href: '#north-india' },
    { name: 'bhutan', shape: 'circle', coords: [regions.bhutan.x, regions.bhutan.y, 15], href: '#bhutan' },
    { name: 'maldives', shape: 'circle', coords: [regions.maldives.x, regions.maldives.y, 15], href: '#maldives' },
  ]
}