import { Col, Row, Spin } from "antd";
import { useEffect, useState } from "react";
import { TravelCloudClient } from "travelcloud-antd";
import { getContent } from "web-component-antd";
import config from "../customize/config";

const TikTok = ({ content }) => {
  const result = getContent(content).tag("tiktok-videos").getValue();
  const [videos, setVideos] = useState([]);
  const [load, setLoad] = useState(false);

  const fetchVideo = async (videoUrl) => {
    const response = await fetch(
      `https://www.tiktok.com/oembed?url=${videoUrl}`
    );
    const content = await response.json();
    return content;
  };

  const fetchAllVideos = async () => {
    const promises = result.photos.map((item) => {
      return fetchVideo(item.desc);
    });
    Promise.all(promises).then((values) => {
      setVideos(values);
      setLoad(false);
    });
  };

  useEffect(() => {
    setLoad(true);
    fetchAllVideos();
  }, []);

  useEffect(() => {
    if (!load) {
      const scriptTag = document.createElement("script");
      scriptTag.src = "https://www.tiktok.com/embed.js";
      document.body.appendChild(scriptTag);
    }
  }, [load]);

  return (
    <div id="tiktok-page">
      <div className="wrap">
        {load && (
          <div className="loading-spinner">
            <Spin size={"large"} />
            <div style={{ paddingTop: 10 }}>Loading the videos...</div>
          </div>
        )}
        <Row>
          {videos.map((video, index) => (
            <Col key={index} sm={24} md={12} lg={12} xl={8}>
              <div
                style={{ width: "100%" }}
                className="tiktok-video"
                dangerouslySetInnerHTML={{
                  __html: video.html,
                }}
              />
            </Col>
          ))}
        </Row>
      </div>
    </div>
  );
};
TikTok.getInitialProps = async () => {
  const client = new TravelCloudClient(config);
  const content = await client.getDocument("tiktok", "www.kingtravelasia.com");
  return { content };
};
export default TikTok;
