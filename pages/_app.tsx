import React from "react";
import App from "next/app";
import { loadGetInitialProps } from "next-server/dist/lib/utils";
import WrappedLayout from "../components/Layouts/WrappedLayout";
import * as NProgress from "nprogress/nprogress";
import Router from "next/router";
import "../customize/styles.less";
import config from "../customize/config";
import Rollbar from "rollbar";
import "../customize/styles.less";
import { setupUserback, TravelCloudClient } from "travelcloud-antd";

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

class MyApp extends App<any> {
  state = {
    hasError: false as boolean,
    //available: false as boolean,
  };

  static async getInitialProps({ Component, ctx }) {
    const pageProps = await loadGetInitialProps(Component, ctx);
    const client = new TravelCloudClient(config);
    const priceRules = await client.priceRules({ is_public: "1" });
    const content = await client.getDocument(
      "layout",
      "www.kingtravelasia.com"
    );

    return { pageProps, content, priceRules };
  }

  componentDidMount() {
    /*
    this.setState({
      available: Router?.pathname == "/"
    })
    */
    setupUserback();
  }

  componentDidCatch(error) {
    // Display fallback UI
    this.setState({ hasError: true });
    // https://github.com/zeit/next.js/issues/5070
    const rollbar = new Rollbar({
      enabled: location != null && location.hostname !== "localhost",
      accessToken: config.rollbarAccessToken,
      captureUncaught: false,
      captureUnhandledRejections: false,
      payload: {
        tcUser: config.tcUser,
      },
    });
    rollbar.error(error);
  }

  render() {
    const { Component, pageProps, content, priceRules } = this.props;
    return (
      <WrappedLayout
        layoutContent={content}
        priceRulesApp={priceRules}
        hasError={this.state.hasError}
        pageProps={pageProps}
      >
        {Component}
      </WrappedLayout>
    );
  }
}

export default MyApp;
