import React, { createContext } from "react";
import { TravelCloudClient } from "travelcloud-antd";
import { getContent } from "web-component-antd";
import AllAboutTrips from "../components/Home/AllAboutTrips";
import Blog from "../components/Home/Blog";
import FeaturedTours from "../components/Home/FeaturedTours";
import GoogleReviews from "../components/Home/GoogleReviews";
import HomeBanner from "../components/Home/HomeBanner";
import LatestMoments from "../components/Home/LatestMoments";
import TikTokBanner from "../components/Home/TikTokBanner";
import config from "../customize/config";

export const CMSContext = createContext([]);

const Index = ({ content, allTours }) => {
  const reviewTitle = getContent(content).tag("reviews").getValue();
  return (
    <CMSContext.Provider value={content}>
      <HomeBanner />
      <LatestMoments />
      <FeaturedTours tours={allTours}/>
      <AllAboutTrips />
      <Blog />
      <GoogleReviews reviewTitle={reviewTitle}/>
      <TikTokBanner />
    </CMSContext.Provider>
  );
};

Index.getInitialProps = async () => {
  const client = new TravelCloudClient(config);
  const content = await client.getDocument("index", "www.kingtravelasia.com");
  const allTours = (await client.tours({})).result;
  const categories = (await client.categories({})).result;
  return { content, allTours, categories };
};

export default Index;
