import { Carousel, Col, Row } from "antd";
import { TravelCloudClient } from "travelcloud-antd";
import { getContent } from "web-component-antd";
import GoogleReviews from "../components/Home/GoogleReviews";
import TikTokBanner from "../components/Home/TikTokBanner";
import config from "../customize/config";

const About = ({ content, reviewContent }) => {
  const banner = getContent(content).tag("banner-section").getValue();
  const reviewTitle = getContent(reviewContent).tag("reviews").getValue();
  const profile = getContent(content).tag("company-profile").getValue();
  const mission = getContent(content).tag("mission-vision").getValue();
  const sellingPoints = getContent(content).tag("selling-points").getValue();

  const settings = {
    autoplay: true,
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <div id="about-us">
      <div className="about-wrapper">
        <div className="about-banner">
          <div
            className="banner-img"
            style={{
              backgroundImage: `url(${banner.photo.regular})`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              height: "483px",
            }}
          >
            <div className="about-title">
              <h2>{banner.title}</h2>
              <h5>{banner.description}</h5>
            </div>
          </div>
        </div>
        <div className="wrap profile">
          <Row gutter={80} type="flex" justify="space-around" align="middle">
            <Col xs={24} sm={24} md={24} lg={12}>
              <div>
                <Carousel {...settings}>
                  {profile.photos.map((photo, index) => (
                    <div key={`profile-slide-${index}`}>
                      <div
                        style={{
                          backgroundImage: `url(${photo.photo.regular})`,
                          backgroundPosition: "center",
                          backgroundSize: "cover",
                          backgroundRepeat: "no-repeat",
                          height: 378,
                        }}
                      />
                    </div>
                  ))}
                </Carousel>
              </div>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12}>
              <div style={{ marginBottom: 15, marginTop: 10 }}>
                <h3>{profile.title}</h3>
                <h5>{profile.description}</h5>
                <p className="profile-text">{profile.code}</p>
                <div
                  className="profile-content"
                  dangerouslySetInnerHTML={{ __html: profile.content }}
                />
              </div>
            </Col>
          </Row>
        </div>
      </div>
      <div className="mission-vision">
        <div className="wrap center mission-vision-wrapper">
          <h3>{mission.title}</h3>
          <h5>{mission.description}</h5>
          <Row gutter={60} type="flex" justify="space-around" align="middle">
            {mission.photos.map((photo, index) => (
              <Col
                xs={24}
                sm={24}
                md={12}
                lg={12}
                key={`mission-photo-${index}`}
              >
                <div className="mission-content-wrapper">
                  <div className="mission-content">
                    <div className="mission-img">
                      <img src={photo.photo.regular} />
                    </div>
                    <div className="mission-text">
                      <h6>{photo.title}</h6>
                      <p>{photo.desc}</p>
                    </div>
                  </div>
                </div>
              </Col>
            ))}
          </Row>
        </div>
      </div>
      <div className="wrap center selling-points">
        <h3>{sellingPoints.title}</h3>
        <h5>{sellingPoints.description}</h5>
        <p>{sellingPoints.code}</p>
        <div className="selling-points-wrapper">
          <Row gutter={60} type="flex" justify="space-around">
            {sellingPoints.photos.map((photo, index) => (
              <Col xs={24} sm={12} md={12} lg={6} key={`selling-points-${index}`}>
                <div className="selling-points-content">
                  <div className="selling-points-img">
                    <img src={photo.photo.regular} />
                  </div>
                  <div className="selling-points-title">{photo.title}</div>
                  <div className="selling-points-desc">{photo.desc}</div>
                </div>
              </Col>
            ))}
          </Row>
        </div>
      </div>
      <GoogleReviews reviewTitle={reviewTitle} />
      <TikTokBanner />
    </div>
  );
};
About.getInitialProps = async () => {
  const client = new TravelCloudClient(config);
  const content = await client.getDocument(
    "about-us",
    "www.kingtravelasia.com"
  );
  const reviewContent = await client.getDocument(
    "index",
    "www.kingtravelasia.com"
  );
  return { content, reviewContent };
};
export default About;
