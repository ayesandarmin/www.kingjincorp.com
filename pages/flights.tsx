import React, { Fragment, useContext, useEffect, useState } from "react";
import { Col, Row, Carousel, Button, Icon, Drawer } from "antd";
import { TravelCloudClient } from "travelcloud-antd";
import SingleBanner from "../components/Utilities/SingleBanner";
import config from "../customize/config";
import { getContent } from "web-component-antd";
import { CarouselArrow } from "../components/Utilities/carousel-arrow";
import { LeftCircleFilled, RightCircleFilled } from "@ant-design/icons";
import {
  FilterResults,
  FilterResultWrapper,
  FlightsParamsAdvancedForm,
  SortButtons,
} from "../components/flights-params-form2";
import {
  useAirFares,
  useFlightFilterResults,
  useFlightFilters,
  useFlights,
  useFlightSorter,
  useUrlDispatch,
} from "travelcloud-antd/components/flights/component-logic";
import Router from "next/router";
import * as Scroll from "react-scroll";
import { LayoutContext } from "../components/Layouts/WrappedLayout";
import { NoResult } from "travelcloud-antd/components/no-result";
import { FlightsResult } from "../components/flights-result";
import FareRulesModal from "../components/fare-rules";
import Head from "next/head";

const settings = {
  dots: false,
  infinite: true,
  speed: 500,
  arrows: true,
  slidesToShow: 6,
  prevArrow: (
    <CarouselArrow
      iconElement={
        <span className="carousel-arrow">
          <LeftCircleFilled />
        </span>
      }
    />
  ),
  nextArrow: (
    <CarouselArrow
      iconElement={
        <span className="carousel-arrow">
          <RightCircleFilled />
        </span>
      }
    />
  ),
  rows: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1130,
      settings: {
        slidesToShow: 5,
      },
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 446,
      settings: {
        arrows: false,
        slidesToShow: 2,
      },
    },
  ],
};

const Flights = ({ content, query }) => {
  const { cart, windowSize } = useContext(LayoutContext);
  const flightsBanner = getContent(content).tag("flight-ticketing").getValue();
  const flightsAttributes = JSON.parse(flightsBanner.attributes);
  const flightsPhotos = flightsBanner.photos;
  const bookingSection = getContent(content).tag("booking-section").getValue();

  const client = new TravelCloudClient(config);

  var Events = Scroll.Events;
  useEffect(() => {
    Events.scrollEvent.register("begin", function () {
      console.log("begin", arguments);
    });
    Events.scrollEvent.register("end", function () {
      console.log("end", arguments);
    });
  });

  useEffect(() => {
    Events.scrollEvent.remove("begin");
    Events.scrollEvent.remove("end");
  }, []);

  const onLoadStart = (flightsParam) =>
    Router.push({
      pathname: Router.pathname,
      query: Object.assign(Router.query, flightsParam),
    });

  const data = useFlights({
    defaultType: "return",
    query: { ...query, "od1.origin_city.code": "SIN" },
    client,
    sources: [query.source == null ? config.flightDataSource : query.source],
    onLoadStart,
    autoSearch: false,
  });

  const {
    validateAndSearch,
    flightType,
    flightMap,
    handleFlightMap,
    handleParamsChange,
    flightsParam,
    flights,
  } = data;

  // flight result logic
  const [filterDrawer, setFilterDrawer] = useState(false);
  const [filters, handleFilters] = useFlightFilters(query);
  useUrlDispatch(filters);
  const filteredFlights = useFlightFilterResults(flights, filters, cart);

  const [sortedFilter, sort, setSort] = useFlightSorter(
    filteredFlights,
    "default"
  );

  function customPriceDisplay(defaultPriceDisplay) {
    return (
      "S$" +
      Math.ceil(Number(defaultPriceDisplay.replace("$", ""))).toLocaleString()
    );
  }

  const [limit] = useState(50);

  const { fareRulesModalData } = useAirFares();

  function formatToHours(value) {
    return `0${value}:00`.slice(-5).replace("24:00", "23:59");
  }

  const $showFilter = flights.result && flights.result.length > 0 && (
    <FilterResultWrapper
      flights={flights}
      customPriceDisplay={customPriceDisplay}
      query={query}
      handleFilters={handleFilters}
      cart={cart}
      resultElement={({ filters, handleFilters, airlines, flightStops }) => (
        <FilterResults
          flightStops={flightStops}
          handleFilters={handleFilters}
          filters={filters}
          formatToHours={formatToHours}
          flightType={flightType}
          airlines={airlines}
        />
      )}
    />
  );

  return (
    <section id="search-page" className="flights-content">
      <section id="flights" style={{ position: "relative" }}>
        <Head>
          <title>{config.defaultTitle} | Flights</title>
        </Head>
        <SingleBanner content={flightsBanner} />
        <div className={`flights-search wrap`}>
          <FlightsParamsAdvancedForm
            client={client}
            flightParamStore={data}
            onChange={(value) => handleParamsChange(value)}
            onSearch={(_value) => validateAndSearch()}
            value={flightsParam}
            defaultAirportCodes={["SIN", "BKK", "KUL", "HKG"]}
            defaultCityCodes={["NYC"]}
            loading={flights.loading === true}
          />
        </div>
        <div className="wrap">
          <Row gutter={50}>
            {flights.result == null || flights.result.length === 0 ? (
              <NoResult
                response={flights}
                type="flights"
                loadingMessage="Searching for the lowest fares..."
              />
            ) : (
              <Fragment>
                {windowSize.width < 979 ? (
                  <Fragment>
                    <Button
                      id="filter-mobile"
                      onClick={() => setFilterDrawer(true)}
                    >
                      <Icon type="filter" /> FILTER FLIGHTS
                    </Button>
                    <Drawer
                      placement="top"
                      closable={true}
                      onClose={() => setFilterDrawer(false)}
                      visible={filterDrawer}
                      title="FILTER FLIGHTS"
                    >
                      {filterDrawer && (
                        <div style={{ padding: "0 20px 60px" }}>
                          {$showFilter}
                        </div>
                      )}
                    </Drawer>
                  </Fragment>
                ) : (
                  <Col xl={6} lg={6} md={24} sm={24}>
                    <article id="filter-desktop">{$showFilter}</article>
                  </Col>
                )}

                <Col xl={18} lg={18} md={24} sm={24}>
                  {sortedFilter == null || sortedFilter.length === 0 ? (
                    <div style={{ textAlign: "center", paddingTop: "50px" }}>
                      Sorry, there aren’t any flight that match your filters.
                    </div>
                  ) : (
                    <div
                      style={{
                        padding: windowSize.width < 991 ? "0" : "40px 0",
                      }}
                    >
                      <SortButtons
                        sort={sort}
                        setSort={setSort}
                        windowSize={windowSize}
                      />
                      <div
                        style={{ marginTop: windowSize.width < 992 ? 20 : "" }}
                      >
                        <FlightsResult
                          cart={cart}
                          flights={sortedFilter.slice(0, limit)}
                          flightMap={flightMap}
                          onFlightClick={(index, flight) => {
                            if (
                              query.source == null ||
                              query.source == "webconnect"
                            ) {
                              cart
                                .reset()
                                .addFlight(flight, flight["pricings"][0]["id"]);
                              Router.push("/checkout");
                            } else {
                              handleFlightMap(index, flight, [query.source]);
                            }
                          }}
                          onFareClick={(_index, flight, fareId) => {
                            cart.reset().addFlight(flight, fareId);
                            Router.push("/checkout");
                          }}
                        />
                      </div>
                    </div>
                  )}
                </Col>
              </Fragment>
            )}
          </Row>
          <FareRulesModal {...fareRulesModalData} />
        </div>
        <div className="wrap">
          <div className="flights-content m-y-60">
            <Row type="flex" justify="start" gutter={50}>
              <Col xs={24} sm={10}>
                <div
                  className="flights-title"
                  dangerouslySetInnerHTML={{
                    __html: flightsBanner.description,
                  }}
                />
                {flightsAttributes.map(([_key, value], index) => {
                  return (
                    <div className="m-y-60" key={`flight-attribute-${index}`}>
                      <img src={value} />
                    </div>
                  );
                })}
              </Col>
              <Col xs={24} sm={14}>
                <div
                  className="flights-desc"
                  dangerouslySetInnerHTML={{ __html: flightsBanner.content }}
                />
              </Col>
            </Row>
          </div>
        </div>

        <div className="wrap flights-carousel-wrap">
          <div className="silkway-divider"></div>
          <Carousel {...settings}>
            {flightsPhotos.map((fphoto, index) => (
              <div
                key={`flights-carousel-item-${index}`}
                className="flights-carousel-item"
              >
                <img src={fphoto.photo.regular} />
              </div>
            ))}
          </Carousel>
          <div className="silkway-divider"></div>
        </div>

        <div className="wrap booking-section">
          <h4>{bookingSection.title}</h4>
          <Row type="flex" justify="start" gutter={30}>
            {bookingSection.photos.map((bphoto, index) => (
              <Col xs={24} sm={12} lg={6} key={`book-info-${index}`}>
                <div className="book-info">
                  <p className="book-info-img">
                    <img src={bphoto.photo.regular} />
                  </p>
                  <span className="book-info-title">{bphoto.title}</span>
                  <span
                    dangerouslySetInnerHTML={{ __html: bphoto.desc }}
                  ></span>
                </div>
              </Col>
            ))}
          </Row>
        </div>
      </section>
    </section>
  );
};
Flights.getInitialProps = async ({ query }) => {
  const client = new TravelCloudClient(config);
  const content = await client.getDocument(
    "flights",
    "www.silkwaytravelasia.com"
  );
  return { content, query };
};
export default Flights;
