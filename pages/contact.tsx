import { TravelCloudClient } from "travelcloud-antd";
import { getContent } from "web-component-antd";
import config from "../customize/config";
import { ContactFormComponent } from "../components/Utilities/contact";
import TikTokBanner from "../components/Home/TikTokBanner";

const ContactUs = ({ content }) => {
  const banner = getContent(content).tag("banner-section").getValue();
  return (
    <div id="contact-us">
      <div className="contact-wrapper">
        <div className="contact-us-banner">
          <div
            className="banner-img"
            style={{
              backgroundImage: `url(${banner.photo.regular})`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              height: "483px",
            }}
          >
            <div className="contact-us-title">
              <h2>{banner.title}</h2>
              <h5>{banner.description}</h5>
            </div>
          </div>
        </div>
        <div className="wrap-sm">
          <div className="contact-form-section">
            <div className="get-in-touch-title">Get in Touch With Us</div>
            <ContactFormComponent rowGutter={25} />
          </div>
        </div>
      </div>
      <TikTokBanner />
    </div>
  );
};
ContactUs.getInitialProps = async () => {
  const client = new TravelCloudClient(config);
  const content = await client.getDocument("contact", "www.kingtravelasia.com");
  return { content };
};
export default ContactUs;
