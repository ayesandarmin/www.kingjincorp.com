import { Carousel, Col, Icon, Row } from "antd";
import Link from "next/link";
import React, { useContext } from "react";
import { TravelCloudClient } from "travelcloud-antd";
import { LayoutContext } from "../components/Layouts/WrappedLayout";
import RequestSection from "../components/TravelCloud/RequestSection";
import { CarouselArrow } from "../components/Utilities/carousel-arrow";
import SingleBanner from "../components/Utilities/SingleBanner";
import config from "../customize/config";
import { splitNameAndUrl } from "../helpers/common-function";
import { getContent } from "web-component-antd";

const Mice = ({ content }) => {
  const { layoutContent } = useContext(LayoutContext);
  const miceArrangement = getContent(content)
    .tag("mice-arrangements")
    .getValue();
  const miceService = getContent(content).tag("mice-services").getValue();
  const miceSeamless = getContent(content).tag("mice-seamless").getValue();
  const contactAddress = getContent(layoutContent)
    .tag("contact-address")
    .getValue();

  const settings = {
    speed: 500,
    dots: true,
    arrows: false,
    autoplay: true,
    infinite: true,
    slidesToScroll: 1,
    slidesToShow: 1,
  };

  const miceSettings = {
    speed: 500,
    dots: false,
    arrows: true,
    autoplay: true,
    infinite: true,
    slidesToScroll: 1,
    slidesToShow: 3,
    prevArrow: (
      <CarouselArrow
        iconElement={
          <span className="carousel-arrow">
            {" "}
            <Icon type="left" />
          </span>
        }
      />
    ),
    nextArrow: (
      <CarouselArrow
        iconElement={
          <span className="carousel-arrow">
            {" "}
            <Icon type="right" />
          </span>
        }
      />
    ),
    responsive: [
      {
        breakpoint: 768,
        settings: {
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 576,
        settings: {
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
        },
      },
    ],
  };

  return (
    <div id="mice">
      <SingleBanner content={miceArrangement} />
      <div className="wrap">
        <div className="mice-content">
          <h3>{miceArrangement.description}</h3>
          <div dangerouslySetInnerHTML={{ __html: miceArrangement.content }} />
        </div>
        <div className="silkway-divider" />
        <div className="mice-service m-y-50">
          <Row gutter={40}>
            <Col md={12}>
              <Carousel {...settings}>
                {miceService.photos.map((photo, index) => (
                  <div key={`slider-${index}`}>
                    <div
                      style={{
                        margin: "auto",
                        backgroundImage: `url(${photo.photo.regular})`,
                        backgroundPosition: "center",
                        backgroundSize: "cover",
                        height: 350,
                      }}
                    />
                  </div>
                ))}
              </Carousel>
            </Col>
            <Col md={12}>
              <div className="mice-service-content">
                <div className="mice-title p-l-25">{miceService.title}</div>
                <div
                  className="mice-service-desc"
                  dangerouslySetInnerHTML={{ __html: miceService.content }}
                />
              </div>
            </Col>
          </Row>
        </div>
      </div>
      <div
        className="mice-seamless"
        style={{
          backgroundImage: `url(${miceSeamless.photo.regular})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          height: 464,
        }}
      >
        <div className="mice-seamless-content wrap p-y-50">
          <div className="mice-title">{miceSeamless.title}</div>
          <Row gutter={30}>
            <Carousel {...miceSettings}>
              {miceSeamless.photos.map((photo, index) => {
                const [desc, link] = splitNameAndUrl(photo.desc);

                return (
                  <Link href="#" key={`mice-seamless-${index}`}>
                    <Col>
                      <div className="p-t-20">
                        <div
                          style={{
                            backgroundImage: `url(${photo.photo.regular})`,
                            backgroundPosition: "center",
                            backgroundSize: "cover",
                            height: 216,
                          }}
                        />
                        <div className="mice-seamless-desc">
                          <a href={link}>{desc}</a>
                        </div>
                      </div>
                    </Col>
                  </Link>
                );
              })}
            </Carousel>
          </Row>
        </div>
      </div>
      <div className="wrap p-y-60">
        <RequestSection contactAddress={contactAddress} section="mice" />
      </div>
    </div>
  );
};
Mice.getInitialProps = async () => {
  const client = new TravelCloudClient(config);
  const content = await client.getDocument("mice", "www.silkwaytravelasia.com");
  return { content };
};
export default Mice;
