import React from 'react'
import Big from 'big.js'
import config from '../customize/config'
import { TravelCloudClient, redirectToPayment, formatCurrency } from 'travelcloud-antd'
import { Row, Col, Card, Button, Avatar, Modal } from 'antd'
import { Order } from '../components/Payment/Order'
import { RightOutlined, CloseOutlined } from "@ant-design/icons"

const payment_logo = {
  'aba_payway_abapay': "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/travelcloud-antd%2Fimages%2Fpayment-aba_payway_abapay.png?alt=media&token=627f9e2b-a2e1-4d27-a3b1-9ce791e235d7",
  'aba_payway_cards': "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/travelcloud-antd%2Fimages%2Fpayment-aba_payway_cards.png?alt=media&token=a545cbdc-a412-4f81-92ab-31f01774dfb5",
  'first_data': "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/travelcloud-antd%2Fimages%2Fpayment-first_data.png?alt=media&token=2d203ade-49af-4f61-80fe-e896e3a7440f",
  'pay_2c2p': "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/travelcloud-antd%2Fimages%2Fpayment-pay_2c2p.png?alt=media&token=53c8d628-9901-47cc-9387-85a72ce11876",
  'paydollar': "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/travelcloud-antd%2Fimages%2Fpayment-paydollar.png?alt=media&token=99ef174e-ea2b-4881-a672-2e87b6d494b7",
  'paynow': "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/travelcloud-antd%2Fimages%2Fpayment-paynow.png?alt=media&token=45776b89-3377-4d72-a614-4990adea26ef",
  'paypal': "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/travelcloud-antd%2Fimages%2Fpayment-paypal.png?alt=media&token=9bff976c-7680-4ca3-a6dd-15adee45354e",
  'reddot': "https://firebasestorage.googleapis.com/v0/b/headless-cms-292305.appspot.com/o/travelcloud-antd%2Fimages%2Fpayment-reddot.png?alt=media&token=73188982-81ea-42b8-bb6c-72c86b21172b",
}

export default class extends React.PureComponent<any, any> {
  client = new TravelCloudClient(config)

  state: any = {
    loading: true,
    noRef: false,
    order: null
  }

  async componentDidMount() {
    window.scrollTo(0, 0)
    const ref = (new URL(location.href)).searchParams.get('ref')
    if (ref == null) {
      this.setState({
        loading: false,
        noRef: true
      })
    } else {
      const order = await this.client.order({ ref })
      this.setState({
        loading: false,
        order
      })
    }
  }

  async payWithAccountCredit() {
    const ref = (new URL(location.href)).searchParams.get('ref')
    if (ref == null) {
      this.setState({
        loading: false,
        noRef: true
      })
      return
    }
    this.setState({
      loading: true
    })
    await this.props.cart.payOrderWithCustomerCredit(ref)
    const order = await this.client.order({ref})
    this.setState({
      loading: false,
      order
    })
  }

  render() {
    // const cart = this.props.cart
    // const baseDeposit = !!this.state.order && this.state.order.result.deposit_required
    // const basePrice = !!this.state.order && this.state.order.result.payment_required

    if (this.state.loading) {
      return (
        <div className="wrap pad-y">
          <Card loading={true} style={{border: 0}} />
        </div>
      )
    }

    if (this.state.noRef || (this.state.order != null && this.state.order.result == null)) {
      return (
        <div className="wrap pad-y">
          <h1>Unable to load order</h1>
        </div>
      )
    }

    const customer = this.props.customer?.result || null
    const bigAccountBalance = customer == null 
    ? Big(0) 
    : this.props.customer.result.payments
        .reduce((acc, payment) => acc.plus(payment.amount), Big(0))
        .times(-1)

    const orderDeposit = Big(this.state.order.result.deposit_required)
    const orderPayment = Big(this.state.order.result.payment_required)
    
    return (
      <section className="grey pad-y">
        <div id="payment">
          <Row type="flex" justify="space-between" gutter={{ lg: 48 }}>
            <Col
              id="checkout"
              xs={{ span: 24, order: 2 }} 
              sm={{ span: 24, order: 2 }} 
              md={24}
              lg={{ span: 14, order: 1 }}
            >
              <section id="order-details">
                <Order order={this.state.order.result} />
              </section>
            </Col>

            {this.state.order.result.order_status === 'Canceled' &&
              <Col 
                xs={{ span: 24, order: 1 }} 
                sm={{ span: 24, order: 1 }} 
                md={{ span: 24, order: 1 }} 
                lg={10}
              >
                <h1>Your order has expired</h1>
              </Col>
            }

            {this.state.order.result.order_status === 'Invoice' &&
              <Col 
                xs={{ span: 24, order: 1 }} 
                sm={{ span: 24, order: 1 }} 
                md={{ span: 24, order: 1 }} 
                lg={10}
              >
                <h1>Your payment has been received</h1>
                <p>Thank you for your order! Our team is processing your order.</p>
                <p>Please check your email for a copy of the invoice.</p>
              </Col>
            }
            
            {this.state.order.result.order_status === "Quotation" &&
              <Col xs={24} sm={24} md={24} lg={10} style={{ marginBottom: 40 }}>
                <h1>Select payment method</h1>
                {customer !== null &&
                  <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", padding: "20px 0", borderBottom: "1px solid #e8e8e8" }}>
                    <div>
                      <strong>Account Credit</strong>
                      <div>Balance ${bigAccountBalance.toFixed(2)}</div>
                    </div>
                    <Button
                      size="large"
                      type="primary"
                      style={{ flexShrink: 0 }}
                      onClick={() => this.payWithAccountCredit()}
                    >
                      {orderDeposit.lt(orderPayment)
                        ? "Pay Deposit of " + formatCurrency(orderDeposit)
                        : "Pay " + formatCurrency(orderPayment)
                      }
                      <RightOutlined />
                    </Button>
                  </div>
                }
                {config.payNowUEN &&
                <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", padding: "20px 0", borderBottom: "1px solid #e8e8e8" }}>
                  <img src={payment_logo.paynow} style={{ width: 180 }} />
                  <Button
                    size="large"
                    type="primary"
                    onClick={() => this.setState({ visible: true })}
                    style={{ flexShrink: 0 }}
                  >
                    {orderDeposit.lt(orderPayment)
                      ? "Pay Deposit of " + formatCurrency(orderDeposit)
                      : "Pay " + formatCurrency(orderPayment)
                    }
                    <RightOutlined />
                  </Button>
                  <Modal 
                    visible={this.state.visible}
                    onCancel={() => this.setState({ visible: false })}
                    closable={false}
                    title={
                      <Row>
                        <Col span={20}>
                          PAYNOW
                        </Col>
                        <Col span={4} style={{ textAlign: "right" }}>
                          <a onClick={() => this.setState({ visible: false })}><CloseOutlined /></a>
                        </Col>
                      </Row>
                    }
                    footer={false}
                  >
                    <img src="/images/qr-code.png" width={300} /><br /><br />
                    <p><strong>PayNow</strong> to our <span style={{ textDecoration: "underline" }}>UEN Number</span>: <strong className="color-primary">{config.payNowUEN}</strong></p>
                    <p>Please include your order number <strong className="color-primary">{this.state.order.result.ref || "XXXXX"}</strong> in the reference note.</p>
                    <p>We will contact you asap once the transaction has been verified. If payment is not received, your order will be auto-cancelled within 20 mins from the time of booking.</p>
                    <p>If you need further assistance: kindly contact us at <a href={`tel:${config.phone.replace(/[^0-9]/g, '')}`}>{config.phone}</a></p>
                  </Modal>
                </div>
                }
                {this.state.order.result._payment_groups.map((paymentGroup, paymentGroupIndex) =>
                  paymentGroup.options.map((option, optionIndex) => {
                    const deposit = Big(option.deposit_required)
                    const fullfee = Big(option.payment_required)
                    let fee = Big(option.payment_required).minus(paymentGroup.payment_required)
                    let amt = option.payment_required
                    let grpAmt = paymentGroup.payment_required
                    let payBtnText = "Pay"
                    if (deposit.lt(fullfee)) {
                      fee = Big(option.deposit_required).minus(paymentGroup.deposit_required)
                      amt = option.deposit_required
                      grpAmt = paymentGroup.deposit_required
                      payBtnText = "Pay Deposit of"
                    }
                    return (
                      <div key={paymentGroupIndex + "/" + optionIndex} style={{ display: "flex", justifyContent: "space-between", alignItems: "center", padding: "20px 0", borderBottom: "1px solid #e8e8e8" }}>
                        <div>
                          <img src={"/img/payment-logo.jpeg"} style={{ width: 180, paddingBottom: 10 }} />
                          {Big(amt).gt(grpAmt) && 
                            <div style={{ fontStyle: "italic" }}>
                              (Inclusive of {formatCurrency(fee)} processing fee)
                            </div>
                          }
                          {Big(amt).lt(grpAmt) && 
                            <div style={{ fontStyle: "italic" }}>
                              (Inclusive of {formatCurrency(fee)} discount)
                            </div>
                          }
                        </div>
                        <Button
                          size="large"
                          type="primary"
                          style={{ flexShrink: 0 }}
                          onClick={() =>
                            redirectToPayment({
                              orderRef: this.state.order.result.ref,
                              tcUser: config.tcUser,
                              paymentProcessor: option.source,
                              successPage: "payment?ref=" + this.state.order.result.ref,
                              failurePage: "payment?ref=" + this.state.order.result.ref,
                              // successPage: "payment-successful",
                              // failurePage: "payment-failed",
                              amount: amt
                            })
                          }
                        >
                          {payBtnText} {formatCurrency(amt)} <RightOutlined />
                        </Button>
                      </div>
                    )
                  })
                )}
              </Col>
            }
            
            {customer != null && bigAccountBalance != 0 && 
              <>
                <Col sm={12} md={12} lg={12}>
                  <Card.Meta
                    avatar={<Avatar size="large" icon="user" style={{ backgroundColor: "#af0201" }} />}
                    title={customer.name
                      ? customer.name
                      : customer.email
                    }
                    description={<span>Account balance ${bigAccountBalance.toFixed(2)}</span>} 
                  />
                </Col>
                <Col sm={12} md={12} lg={12}>
                  <Button disabled={bigAccountBalance.lt(this.state.order.result.payment_required)} type="primary" onClick={() => this.payWithAccountCredit()}>Account Credit <RightOutlined /></Button>
                </Col>
              </>
            }
          </Row>
        </div>
      </section>
    )
  }
}