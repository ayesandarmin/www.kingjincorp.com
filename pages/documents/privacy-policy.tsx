import React from "react";
import { TravelCloudClient } from "travelcloud-antd";
import { getContent } from "web-component-antd";
import TikTokBanner from "../../components/Home/TikTokBanner";
import config from "../../customize/config";

const PrivacyPolicy = ({ content }) => {
  const policy = getContent(content).tag("privacy-policy").getValue();
  return (
    <>
      <div
        id="privacy-policy"
        className="wrap"
        dangerouslySetInnerHTML={{ __html: policy.content }}
      ></div>
      <TikTokBanner />
    </>
  );
};

PrivacyPolicy.getInitialProps = async () => {
  const client = new TravelCloudClient(config);
  const content = await client.getDocument(
    "document",
    "www.kingtravelasia.com"
  );
  return { content };
};

export default PrivacyPolicy;
