import React from "react";
import config from "../customize/config";

export default class Page extends React.PureComponent<any> {
  render() {
    return (
      <section className="main-background">
        <div id="payment" className="wrap pad-y">
          <h2 className="font-color-1">
            <strong>Payment Unsuccessful</strong>
          </h2>
          <p>
            If you are having difficulties making payment online, please contact
            us @{" "}
            <a href={`tel:${config.phone.replace(/[^0-9]/g, "")}`}>
              {config.phone}
            </a>{" "}
            for assistance.
          </p>
        </div>
      </section>
    );
  }
}
