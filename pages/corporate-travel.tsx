import { Carousel, Col, Icon, Row } from "antd"
import Link from "next/link"
import React, { useContext } from "react"
import { TravelCloudClient } from "travelcloud-antd"
import { LayoutContext } from "../components/Layouts/WrappedLayout"
import RequestSection from "../components/TravelCloud/RequestSection"
import { CarouselArrow } from "../components/Utilities/carousel-arrow"
import SingleBanner from "../components/Utilities/SingleBanner"
import config from "../customize/config"
import { splitNameAndUrl } from "../helpers/common-function"
import { getContent } from "web-component-antd";

const CorporateTravel = ({ corporate }) => {
  const { layoutContent } = useContext(LayoutContext)
  const corporateTravel = getContent(corporate).tag('corporate-travel').getValue()
  const corporateSeamless = getContent(corporate).tag('seamless-arrangement').getValue()
  const contactAddress = getContent(layoutContent).tag('contact-address').getValue()

  const corporateServices = getContent(layoutContent).tag('corporate-services').getValue()

  const settings = {
    speed: 500,
    dots: false,
    arrows: true,
    autoplay: true,
    infinite: true,
    slidesToScroll: 1,
    slidesToShow: 3,
    prevArrow: <CarouselArrow iconElement={< span className="carousel-arrow" > <Icon type="left" /></span>} />,
    nextArrow: <CarouselArrow iconElement={< span className="carousel-arrow" > <Icon type="right" /></span >} />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 576,
        settings: {
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
        }
      },
    ]
  }

  return (
    <div id="corporate-travel">
      <SingleBanner content={corporateTravel} />
      <div className="wrap">
        <div className="corporate-content">
          <h3>{corporateTravel.description}</h3>
          <div dangerouslySetInnerHTML={{ __html: corporateTravel.content }} />
        </div>
        <div className="silkway-divider" />
        <div className="corporate-services m-y-50">
          <div className="corporate-title p-b-10">
            {corporateServices.title}
          </div>
          <div>
            <Row gutter={34}>
              {corporateServices.photos.map((service, index) => (
                <Col md={8} key={`service-${index}`}>
                  <div className="service-item">
                    <Row>
                      <Col span={24}>
                        <div className="service-img">
                          <img src={service.photo.regular} />
                        </div>
                      </Col>
                      <Col span={24}>
                        <div className="service-desc">{service.desc}</div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              ))}
            </Row>
          </div>
        </div>
      </div>
      <div
        className="corporate-seamless"
        style={{
          backgroundImage: `url(${corporateSeamless.photo.regular})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          height: 464,
        }}
      >
        <div className="corporate-seamless-content wrap p-y-50">
          <div className="corporate-title">{corporateSeamless.title}</div>
          <Row gutter={30}>
            <Carousel {...settings}>
              {corporateSeamless.photos.map((photo, index) => {
                const [desc, link] = splitNameAndUrl(photo.desc);

                return (
                  <Link href="#" key={`corporate-seamless-${index}`}>
                    <Col>
                      <div className="p-t-20">
                        <div
                          style={{
                            backgroundImage: `url(${photo.photo.regular})`,
                            backgroundPosition: "center",
                            backgroundSize: "cover",
                            height: 216,
                          }}
                        />
                        <div className="corporate-seamless-desc">
                          <a href={link}>{desc}</a>
                        </div>
                      </div>
                    </Col>
                  </Link>
                );
              })}
            </Carousel>
          </Row>
        </div>
      </div>
      <div className="wrap p-y-60">
        <RequestSection
          contactAddress={contactAddress}
          section="corporate"
        />
      </div>
    </div>
  );
}
CorporateTravel.getInitialProps = async () => {
  const client = new TravelCloudClient(config)
  const corporate = await client.getDocument('corporate', 'www.silkwaytravelasia.com')
  return { corporate }
}
export default CorporateTravel