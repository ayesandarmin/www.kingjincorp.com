import React, { Fragment, useEffect, useState } from "react";
import config from "../../customize/config";
import { TravelCloudClient } from "travelcloud-antd";
import { getContent } from "web-component-antd";
import SingleBanner from "../../components/Utilities/SingleBanner";
import { LeftCircleFilled, RightCircleFilled } from "@ant-design/icons";
import scrollToElement from "scroll-to-element";

import { Button, Carousel, Col, Modal, Result, Row } from "antd";
import Router, { useRouter } from "next/router";
import { CarouselArrow } from "../../components/Utilities/carousel-arrow";
// import { tours } from "../../components/data";
import { VacationTourCard } from "../../components/Products/tour-card";
import { ContactForm } from "../../components/TravelCloud/ContactForm";
import Link from "next/link";

const Page = ({ content, country, destination, notFound, tours }) => {
  const [modals, setModals] = useState({});
  const router = useRouter();

  const countries = getContent(content)
    .tag("countries")
    .sortLTH("sequence")
    .all();
  const result = countries;

  const banner = getContent(content).tag("banner").getValue();

  const infos = {
    calendar: "BEST TIME TO GO",
    currency: "CURRENCY",
    capital: "CAPITAL CITY",
    timezone: "TIMEZONE",
    climate: "CLIMATE INFO",
    visa: "ENTRY VISA",
  };

  const replaceSpace = (code) => {
    return code.replace(" ", "-");
  };

  const handleCountryRoute = (countryCode) => {
    const code = replaceSpace(countryCode);
    router.push(`/region/${destination}?country=${code}`, undefined, {
      shallow: true,
    });
    scrollToElement(`#${code}`, {
      offset: 0,
      duration: 1000,
    });
  };

  useEffect(() => {
    scrollToElement(`#${country}`, {
      offset: 0,
      duration: 1000,
    });
  }, [country]);

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    arrows: true,
    slidesToShow: 2,
    prevArrow: (
      <CarouselArrow
        iconElement={
          <span className="carousel-arrow">
            <LeftCircleFilled />
          </span>
        }
      />
    ),
    nextArrow: (
      <CarouselArrow
        iconElement={
          <span className="carousel-arrow">
            <RightCircleFilled />
          </span>
        }
      />
    ),
    rows: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1130,
        settings: {
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 446,
        settings: {
          arrows: false,
          slidesToShow: 1,
        },
      },
    ],
  };

  const backToHome = (
    <Link href="/">
      <div className="flex-button">
        <Button type="primary" size="large">
          Back Home
        </Button>
      </div>
    </Link>
  );

  return notFound ? (
    <Result
      status={500}
      title="Error"
      subTitle="Sorry, Your region url is wrong"
      extra={backToHome}
    />
  ) : (
    <div id="region">
      <SingleBanner content={banner} />
      <section className="wrap region-content p-x-70 p-t-58">
        <h2>{banner.description}</h2>
        <div dangerouslySetInnerHTML={{ __html: banner.content }} />
        <div className="region-tags">
          <Row gutter={13} type="flex">
            {result.map(([countryCode, country], index) => (
              <Col key={`destination-tag-${index}`}>
                <a onClick={() => handleCountryRoute(countryCode)}>
                  {country.title}
                </a>
              </Col>
            ))}
          </Row>
        </div>
        <div className="region-divider m-t-58" />
      </section>
      {result.map(([code, country], index) => {
        const attributes = JSON.parse(country.attributes);
        const filteredTours = tours.filter((tour) => {
          return tour.categories.some(
            (category) => country.tags.indexOf(category.name) >= 0
          );
        });
        return (
          <Fragment key={`country-content-${index}`}>
            <section
              id={replaceSpace(code)}
              className="wrap p-x-70 p-t-58 region-country"
            >
              <div className="m-b-70">
                <Row gutter={33}>
                  <Col md={12}>
                    <div
                      dangerouslySetInnerHTML={{ __html: country.content }}
                    ></div>
                  </Col>
                  <Col md={12}>
                    <div
                      dangerouslySetInnerHTML={{ __html: country.className }}
                    />
                    <article>{country.description}</article>
                  </Col>
                </Row>
                <div className="region-divider m-t-35" />
                <Row type="flex" align="middle" justify="center">
                  <Col lg={18}>
                    <Row gutter={70}>
                      {attributes.map(([key, value], indx) => {
                        const image = `/img/region-${key}.png`;
                        return (
                          <Col
                            md={8}
                            key={`country-attributes-${index}-${indx}`}
                          >
                            <div className="p-t-40">
                              <img width={30} height={30} src={image} />
                              <div className="country-attributes-content">
                                <p>{infos[key]}</p>
                                <div
                                  dangerouslySetInnerHTML={{ __html: value }}
                                ></div>
                              </div>
                            </div>
                          </Col>
                        );
                      })}
                    </Row>
                  </Col>
                  <Col lg={6}>
                    <button
                      className="customise-btn"
                      onClick={() => setModals({ [code]: true })}
                    >
                      <img
                        className="m-r-10"
                        src="/img/customise-contact.png"
                      />
                      Customise for me
                    </button>
                    <Modal
                      centered
                      visible={modals[code]}
                      onCancel={() => setModals({ [code]: false })}
                      footer={false}
                    >
                      <ContactForm />
                    </Modal>
                  </Col>
                </Row>
              </div>
            </section>
            {filteredTours.length > 0 && (
              <section className="region-tours">
                <Row>
                  <div
                    style={{
                      backgroundImage: `url(/img/bg-recommend-tour.png)`,
                      backgroundRepeat: "no-repeat",
                      backgroundSize: "cover",
                      backgroundPosition: "center",
                      minHeight: 399,
                      // marginTop: 70,
                      paddingTop: 64,
                      paddingBottom: 64,
                    }}
                  >
                    <div className="wrap">
                      <h2>Recommended for you</h2>
                      {filteredTours.length > 1 ? (
                        <Carousel {...settings}>
                          {filteredTours.map((tour, index) => (
                            <VacationTourCard
                              key={`vaction-tour-card-${index}`}
                              tour={tour}
                            />
                          ))}
                        </Carousel>
                      ) : (
                        <Row>
                          <Col lg={12}>
                            {filteredTours.map((tour, index) => (
                              <VacationTourCard
                                key={`vaction-tour-card-${index}`}
                                tour={tour}
                              />
                            ))}
                          </Col>
                        </Row>
                      )}
                    </div>
                    {/* <Row type="flex" justify="center">
                    <button
                      className="view-all-btn"
                      onClick={() => router.push("/tours")}
                    >
                      View ALL PACKAGES
                    </button>
                  </Row> */}
                  </div>
                </Row>
              </section>
            )}
          </Fragment>
        );
      })}
    </div>
  );
};

Page.getInitialProps = async (context) => {
  const client = new TravelCloudClient(config);

  const { destination, country } = context.query;
  const { res } = context;

  try {
    const content = await client.getDocument(
      `region-${destination}`,
      "www.silkwaytravelasia.com"
    );
    const tours = (await client.tours({})).result;

    return { tours, content, country, destination };
  } catch (error) {
    if (res) {
      res.writeHead(307, { Location: "/" });
      res.end();
    } else {
      Router.replace("/");
    }
  }
};

export default Page;
