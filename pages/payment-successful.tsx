import React from "react";
import { Icon } from "antd";

export default class Page extends React.PureComponent<any> {
  render() {
    return (
      <section className="main-background">
        <div id="payment" className="wrap pad-y">
          <h2 className="font-color-1">
            <strong>Payment Successful</strong>
          </h2>
          <h3>
            <Icon type="check-circle" /> Thank you for your order!
          </h3>
          <p>
            A copy of the invoice has been sent to your email. Please quote your
            invoice reference when contacting us.
          </p>
        </div>
      </section>
    );
  }
}
