import { Col, Row } from "antd";
import { useContext, useState } from "react";
import { TravelCloudClient } from "travelcloud-antd";
import { getContent } from "web-component-antd";
import { FeaturedTourCard } from "../components/Card/FeaturedTourCard";
import TikTokBanner from "../components/Home/TikTokBanner";
import { LayoutContext } from "../components/Layouts/WrappedLayout";
import { CategoryFilters } from "../components/Utilities/CategoriesFilter";
import { QuotationFormComponent } from "../components/Utilities/contact";
import InspirationGallery from "../components/Utilities/InspirationGallery";
import config from "../customize/config";

const JapanTravel = ({ content, tours }) => {
  const { windowSize } = useContext(LayoutContext);
  const banner = getContent(content).tag("japan-banner").getValue();
  const planJapan = getContent(content).tag("plan-japan").getValue();
  const JpToursContent = getContent(content).tag("jp-tours-content").getValue();
  const japanInspiration = getContent(content)
    .tag("japan-inspiration")
    .getValue();
  const places = getContent(content).tag("inspiration-places").getValue();
  const activities = getContent(content)
    .tag("inspiration-activities")
    .getValue();
  const food = getContent(content).tag("inspiration-food").getValue();
  const festivals = getContent(content).tag("inspiration-festivals").getValue();
  const japanTours = tours.filter((tour) =>
    tour.categories.find((cat) => cat.name === "ctry-japan")
  );
  const [showForm, setShowForm] = useState(false);

  const allInspirations = {
    photos: [
      ...places.photos,
      ...activities.photos,
      ...food.photos,
      ...festivals.photos,
    ],
  };

  const result = [
    {
      name: "all",
      photos: allInspirations.photos,
    },
    {
      name: "places",
      photos: places.photos,
    },
    {
      name: "activities",
      photos: activities.photos,
    },
    {
      name: "food",
      photos: food.photos,
    },
    {
      name: "festivals",
      photos: festivals.photos,
    },
  ];

  const categories = [
    {
      description: "Show All",
      name: "all",
    },
    {
      description: "Places",
      name: "places",
    },
    {
      description: "Activities",
      name: "activities",
    },
    {
      description: "Food",
      name: "food",
    },
    {
      description: "Festivals",
      name: "festivals",
    },
  ];

  const [category, setCategory] = useState("all");

  const filteredResults = result.find((res) => res.name === category);

  return (
    <div id="country-travel">
      <div className="wrap-xl">
        <div className="country-banner">
          <div
            className="banner-img"
            style={{
              backgroundImage: `url(${banner.photo.regular})`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              height: "483px",
            }}
          >
            <div className="country-title">
              <h2>{banner.title}</h2>
              <h5>{banner.description}</h5>
            </div>
          </div>
        </div>
      </div>
      <div className="wrap plan-country">
        <Row gutter={80} type="flex" justify="space-around" align="middle">
          <Col sm={24} md={12}>
            <div>
              <iframe
                height="378"
                style={{ border: "none", width: "100%", maxWidth: 670 }}
                src="https://www.youtube.com/embed/Rl9gvBFAV_A"
                title="YouTube video player"
              ></iframe>
            </div>
          </Col>
          <Col sm={24} md={12}>
            <div style={{ marginBottom: 15, marginTop: 7 }}>
              <h3>{planJapan.title}</h3>
              <h5>{planJapan.description}</h5>
              <p>{planJapan.code}</p>
              <div className="get-a-quote">
                <a onClick={() => setShowForm(true)}>
                  Get A Quote within 24hrs
                </a>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <div className="ready-to-go-tours">
        <div className="wrap center p-y-60">
          <h3>{JpToursContent.title}</h3>
          <h5>{JpToursContent.description}</h5>
          <div className="country-tours">
            <Row>
              {japanTours.map((tour, index) => {
                return (
                  <Col sm={12} md={8} key={`tour-card-${index}`}>
                    <FeaturedTourCard tour={tour} />
                  </Col>
                );
              })}
            </Row>
          </div>
        </div>
      </div>
      <div className="wrap country-inspiration center">
        <h3>{japanInspiration.title}</h3>
        <h5>{japanInspiration.description}</h5>
        <div className="country-inspiration-tabs">
          {categories.length > 1 && (
            <CategoryFilters
              categories={categories}
              selected={category}
              onSelect={(value) => setCategory(value)}
              windowSize={windowSize}
            />
          )}
          <div>
              <InspirationGallery content={filteredResults.photos} />
          </div>
        </div>
      </div>
      <TikTokBanner />
      <QuotationFormComponent
        remark={true}
        useModal={true}
        visible={showForm}
        initialValue={{ destination: "japan-tour" }}
        closeModal={() => setShowForm(false)}
      />
    </div>
  );
};
JapanTravel.getInitialProps = async () => {
  const client = new TravelCloudClient(config);
  const content = await client.getDocument(
    "japan-travel",
    "www.kingtravelasia.com"
  );
  const tours = (await client.tours({})).result;
  return { content, tours };
};
export default JapanTravel;
